# Aula 3 - Programas e processos

- [Vídeo desta aula](https://youtu.be/0mJkvHWKwr0)
- [Playlist completa](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)

## 3.1 - O que 'não são' processos

As definições mais comuns, inclusive as minhas quando a ideia é não entrar no assunto, sempre passam a impressão de que os processos são *programas em execução*, ou ainda, *instâncias de programas em execução*. Elas não estão erradas, mas são bastante incompletas e imprecisas, principalmente se você quiser realmente entender como o sistema operacional permite e gerencia a execução de inúmeras operações simultâneas em um ambiente que possui recursos limitados.

O ponto mais importante é que **processos não são programas** e **instância** é uma palavra vaga demais para ser utilizada numa conceituação. Portanto, o nosso objetivo nesta aula é dar uma visão mais completa do que acontece no sistema operacional quando um programa é invocado para execução, de como os processos nascem, dos seus ciclos de vida e, principalmente, das ferramentas que temos para gerenciá-los.

## 3.2 - Executáveis são um formato de arquivo

Deixando a conversa sobre programas compilados e interpretados para mais adiante, o que nos interessa agora é saber que os programas são arquivos contendo dados, instruções em código de máquina, e diversas outras informações sobre as coisas de que ele precisa para ser executado. Neste aspecto, o arquivo de um programa não difere muito do arquivo de uma imagem PNG ou de um áudio OGG, no sentido de que todo arquivo possui uma forma padronizada para dizer ao sistema operacional como os seus dados deverão ser tratados, e é a isso que nós chamamos de **formatos de arquivo**.

No caso de arquivos executáveis, seus formatos podem variar de acordo com a plataforma de destino:

* **GNU/Linux:** ELF
* **FreeBSD:** a.out (até a versão 3.0, quando passou a ser ELF)
* **macOS:** Mach-O
* **Windows:** PE

> Algumas implementações do FreeBSD e do NetBSD oferecem suporte ao formato Mach-O.

Geralmente, o módulo de um sistema operacional responsável por interpretar os formatos de executáveis e carregar o programa memória é o ***loader*** (*carregador*). 

### Arquivos binários

Por mais estranho que pareça, a definição mais comum de "arquivo binário" é: um arquivo digital cujo conteúdo não é escrito para ser lido como texto. Mas, como é sempre preferível evitar as negações, nós vamos utilizar outro conceito:

> Binários são arquivos digitais em que os dados são escritos em sequências de bytes que não precisam, necessariamente, representar ou corresponder a caracteres de texto.

O arquivo de uma imagem PNG, por exemplo, é um arquivo binário, assim como o arquivo de um documento ODT, do LibreOffice Writer.

A imagem abaixo mostram o conteúdo do binário de um PNG. Note que, mesmo sendo uma sequência de bytes, nós ainda podemos reconhecer algumas palavras, mas quase nada faz sentido como texto:

![](https://blauaraujo.com/wp-content/uploads/2021/01/binario-01.png)

Os arquivos de programas também são binários, só que eles possuem um atributo a mais que permite a sua execução como programas.

Observe o conteúdo do binário executável do programa `ola-mundo`, escrito em C:

![](https://blauaraujo.com/wp-content/uploads/2021/01/binario-02.png)

Esses binários são gerados a partir do código fonte principal (que é um arquivo de texto), de códigos secundários provenientes de outros módulos e bibliotecas, e dos cabeçalhos e metadados correspondentes às especificações do formato final do arquivo.

Na imagem, observe dois detalhes muito interessantes:

* Na primeira linha da visualização em ASCII (coluna à direita), é possível ler a sigla **ELF**, que é o formato de arquivo dos executáveis para o GNU/Linux.

* Quase no final da mesma coluna, aparece o nome de um arquivo: `ld-linux-x86-64.so.2`. Em sistemas GNU/Linux, essa é a biblioteca responsável por carregar os programas (*loader*) e por fazer a ligação deles com as **bibliotecas dinâmicas** que eles utilizam.

> Essas imagens são 'prints' do programa 'Midnight Commander', que é um navegador, editor e visualizador de arquivos para a interface da linha de comandos (CLI).

### Bibliotecas

Um programa pode ou não conter todo o código necessário para cumprir suas finalidades, do mesmo modo que nem todo arquivo de programa é um executável. Na verdade, existem coleções de 
códigos e dados que podem ser utilizadas na criação de outros programas, evitando a *"reinvenção da roda"*, como dizem, e tornando o desenvolvimento mais modular e de fácil manutenção. Essas coleções de código são chamadas de **bibliotecas**.

> Uma biblioteca **é um programa**, e algumas podem até ser executadas como programas independentes.

No momento da compilação, as bibliotecas podem ser incorporadas ao arquivo executável que será gerado, e nós chamamos isso de **construção estática**. O mais comum, porém, é que os arquivos dos programas contenham apenas as informações sobre as bibliotecas que precisam ser carregadas junto com eles antes da execução, o que é feito através de um processo chamado de **ligação dinâmica**.

Sendo assim, um dos papéis do *loader* do sistema operacional é procurar as bibliotecas dinâmicas de que o programa precisa (dependências) e carregá-las na memória. Quando as bibliotecas já estão carregadas, é a vez de carregar o código do programa e de passar para ele o controle da execução. 

### Inspecionando as dependências de um executável

Existe um utilitário muito interessante na coleção de ferramentas  do Projeto GNU para o trabalho com arquivos binários (*binutils*) chamado `objdump`. Com ele, nós podemos obter diversas informações sobre o arquivo de um executável e até "desmontar" seu código em linguagem de máquina!

Com a opção `-f`, nós temos as informações do cabeçalho geral do arquivo executável. Vamos experimentar com o `bash`:

```
:~$ objdump -f /bin/bash

/bin/bash:     formato de ficheiro elf64-x86-64
arquitectura: i386:x86-64, bandeiras 0x00000150:
HAS_SYMS, DYNAMIC, D_PAGED
endereço inicial 0x0000000000030680
```

> Observe que, logo na primeira linha da saída, nós temos a informação sobre o formato do arquivo: `elf64-x86-64`.

No cabeçalho específico do objeto que gerou o arquivo binário do programa, chamado de *cabeçalho privado*, nós podemos encontrar as informações sobre as bibliotecas de que ele depende. Para isso, nós utilizamos a opção `-p`. Mas, como a saída pode ser muito longa, nós vamos filtrar com o `grep` apenas as linhas que contenham a palavra `NEEDED`:

```
:~$ objdump -p /bin/bash | grep NEEDED
  NEEDED               libtinfo.so.6
  NEEDED               libdl.so.2
  NEEDED               libc.so.6
```

E aqui está a lista das bibliotecas dinâmicas de que o Bash depende para ser executado!

## 3.3 - Programas interpretados

Diferente dos programas compilados, cujos arquivos executáveis contêm dados, instruções e metadados no formato binário, os programas interpretados são arquivos no formato de texto que serão lidos e interpretados por um programa que se chama, claro, **interpretador**.

O detalhe mais interessante sobre isso é que, quando um programa é interpretado, **quem está realmente sendo executado é o interpretador**, não o código em texto (a que geralmente damos o nome de **script**)! Então, se eu escrevo um script em Bash e mando executá-lo, uma instância do Bash será iniciada utilizando o código no meu script **como parâmetro**. O mesmo aconteceria com scripts em Python, PHP, Lua e qualquer outra linguagem interpretada.

## 3.4 - Contexto de execução

Felizmente, o mecanismo de execução de um programa não é só transferir o conteúdo de seu binário para a memória e entregar para ele controle da execução. Muitos sistemas operacionais funcionavam dessa forma, mas não é o caso de sistemas modernos, como os `unix-like`.

Do que vimos até aqui, você já deve ter percebido que o conteúdo do arquivo de um programa e o que é carregado na memória para ser executado não são a mesma coisa. Além disso, sistemas `unix-like` são **multiusuários** e **multitarefas**, ou seja, eles são capazes de executar várias instâncias simultâneas de programas que são invocados por vários usuários ao mesmo tempo. Isso implica, obrigatoriamente, na necessidade de algum tipo de controle -- tanto para especificar a quem pertence uma certa tarefa, quanto para gerenciar os recursos que cada programa terá à sua disposição (fora um monte de outras coisas que não caberiam nesta aula).

Quem faz esse controle no sistema operacional GNU é o *kernel*, e o conjunto de todos os fatores envolvidos na execução de um programa é o que nós chamamos de **contexto de execução**.

## 3.5 - O que é um processo

De novo: tudo são arquivos! Para cumprir sua função de gerenciar a execução de tarefas, o *kernel* conta com uma interface para toda a sua estrutura de dados na forma de um *sistema de arquivos virtual* (*procfs*) montado em `/proc`. Ali, nós encontramos vários arquivos organizados em pastas numeradas que representam, em detalhes, o contexto de cada um dos os programas em execução, seja no espaço do usuário ou do sistema, e essa (finalmente) é a definição de **processo**:

> **Um processo é a estrutura de dados que representa o contexto de execução de um programa.**

Então, processos não são programas carregados na memória nem instâncias de programas em execução, eles são estruturas de dados através das quais o kernel mapeia tudo que diz respeito ao programa que **será** carregado na memória para ser executado. 

### Sistemas de arquivos virtuais

Sistemas de arquivos virtuais são camadas de software do **kernel**, eles não existem "fisicamente" nos discos. Consequentemente, `/proc` é apenas o ponto de montagem de um *pseudo sistema de arquivos* (*procfs*), por isso ele ocupa zero bytes em disco, mesmo que um de seus arquivos, `/proc/kcore`, apareça numa listagem do `ls` com 128 terabytes!

```
:~$ ls -lh /proc/kcore 
-r-------- 1 root root 128T jan  1 15:50 /proc/kcore
```

> Esse valor pode variar com a arquitetura do computador, mas representa a capacidade de endereços físicos que o kernel é capaz de mapear em um sistema com barramento de 64 bits.


### Explorando os arquivos em '/proc'

Além dos processos do usuário, existem vários outros arquivos interessantes em `/proc` que valem uma visita, mesmo não sendo o assunto deste curso. Aqui, nós sugerimos o uso do utilitário `cat` para ver o conteúdo de arquivos com menos linhas e do paginador `less` para os que podem ser mais longos (para sair do `less`, tecle `q`):

#### Driver do tty

```
:~$ cat /proc/consoles
```

#### Versão do kernel

```
:~$ cat /proc/version
```

#### Módulos do kernel carregados

```
:~$ less /proc/modules
```

#### Dispositivos mapeados pelo kernel

```
:~$ less /proc/devices
```


#### Informações sobre a CPU

```
:~$ less /proc/cpuinfo
```

#### Informações sobre a memória

```
:~$ less /proc/meminfo
```

#### Tempo de atividade e tempo gasto em processos

```
:~$ cat /proc/uptime
```

#### Outros arquivos

O manual apresenta uma lista bem mais completa dos arquivos em `/proc`. Não deixe de consultar pelo terminal:

```
:~$ man proc
```

> Repare que muitos utilitários, como o `free`, `uname`, `top`, entre outros, servem-se da interface de dados em `/proc` para fornecerem suas saídas.

## 3.6 - Os diretórios '/proc/[pid]'

Toda vez que um processo é iniciado, ele recebe um número de identificação, o **PID** (*process ID*), e um pseudo diretório com o mesmo número, contendo vários pseudo arquivos diretórios, é criado em `/proc`. Todo esse conjunto de arquivos contém as informações que o kernel mapeia sobre cada processo em separado.

Um arquivo (*pseudo*, claro), em especial, é muito interessante de ser investigado neste momento: o arquivo `/proc/[pid]/status`, que contém dezenas de informações detalhadas sobre o processo.

O utilitário `ps` utiliza os dados em `/proc` para obter as informações que exibe, mas nós podemos ter uma visão mais direta sobre um processo lendo, entre outros, o arquivo `/proc/[pid]/status`, que oferece a maioria das informações em `/proc/[pid]/stat` e `/proc/[pid]/statm`, só que num formato mais fácil de ler.

No exemplo abaixo, nós veremos as informações do processo com **PID 1**. Até um tempo atrás, o PID 1 não era reservado para um processo específico, ou seja, simplesmente acontecia do primeiro processo a ser invocado pelo kernel ser o `init` e ele recebia o PID 1. Mais recentemente, porém, alguns componentes do kernel também começaram a aparecer como processos, o que leva os sistemas `unix-like` modernos a *forçarem* a atribuição do PID 1 para o `init` e, com isso, manter a compatibilidade com os sistemas mais antigos.

> No Debian GNU/Linux, o 'init' padrão é o **SystemD**, mas existem outros 'inits' em uso em outras distribuições e sistemas 'unix-like'.

Vejamos o conteúdo do arquivo `status` do PID 1:

```
:~$ cat /proc/1/status 
Name:	systemd
Umask:	0000
State:	S (sleeping)
Tgid:	1
Ngid:	0
Pid:	1
PPid:	0
TracerPid:	0
Uid:	0	0	0	0
Gid:	0	0	0	0
FDSize:	256
Groups:	 
NStgid:	1
NSpid:	1
NSpgid:	1
NSsid:	1
VmPeak:	  230528 kB
VmSize:	  165104 kB
VmLck:	       0 kB
VmPin:	       0 kB
VmHWM:	   10800 kB
VmRSS:	   10800 kB
RssAnon:	    2904 kB
RssFile:	    7896 kB
RssShmem:	       0 kB
VmData:	   19020 kB
VmStk:	    1032 kB
VmExe:	     816 kB
VmLib:	    8032 kB
VmPTE:	      80 kB
VmSwap:	       0 kB
HugetlbPages:	       0 kB
CoreDumping:	0
THP_enabled:	1
Threads:	1
SigQ:	0/63429
SigPnd:	0000000000000000
ShdPnd:	0000000000000000
SigBlk:	7be3c0fe28014a03
SigIgn:	0000000000001000
SigCgt:	00000001800004ec
CapInh:	0000000000000000
CapPrm:	000001ffffffffff
CapEff:	000001ffffffffff
CapBnd:	000001ffffffffff
CapAmb:	0000000000000000
NoNewPrivs:	0
Seccomp:	0
Seccomp_filters:	0
Speculation_Store_Bypass:	vulnerable
Cpus_allowed:	f
Cpus_allowed_list:	0-3
Mems_allowed:     00000000,00000000,00000000,00000000,
00000000,00000000,00000000,00000000,00000000,00000000,
00000000,00000000,00000000,00000000,00000000,00000000,
00000000,00000000,00000000,00000000,00000000,00000000,
00000000,00000000,00000000,00000000,00000000,00000000,
00000000,00000000,00000000,00000001
Mems_allowed_list:	0
voluntary_ctxt_switches:	117243
nonvoluntary_ctxt_switches:	19751
```

São muitos campos, e a maioria deles *são grego* para nós. Mas nós podemos obter informações importantes com alguns deles.

#### Name

```
Name:	systemd
```

É o nome do arquivo executável do processo. Para visualizar a linha de comando que deu origem ao processo, nós podemos ler o conteúdo de `/proc/[pid]/cmdline`.

#### State

```
State:	S (sleeping)  
```

É o estado do processo (mais sobre isso adiante). 

#### Pid

```
Pid:	1
```

Número de identificação do processo (PID).

#### PPid

```
PPid:	0  
```

Número de identificação do processo "pai" (PPID), ou seja, o processo a partir do qual o processo atual foi gerado. Repare que, no caso do PID 1, o PPID é `0`. Isso acontece por dois motivos: primeiro, porque `1` é o menor número na faixa de PIDs (não existe um PID `0`) e, segundo, os PIDs dizem respeito apenas aos processos no **espaço do usuário** (como visto na segunda aula).

#### Uid e Gid

```
Uid:	0	0	0	0
Gid:	0	0	0	0  
```

Números de identificação do usuário (UID) e do grupo (GID) a quem pertence o processo. São 4 campos, correspondendo ao usuário ou ao grupo:

* **Real:** a quem pertence o processo;
* **Efetivo:** o UID/GID em efeito na execução do comando;
* **Salvo:** o UID/GID salvo para realizar tarefas não privilegiadas;
* **Sistema de arquivos:** o usuário ou grupo do sistema de arquivos.

No exemplo, todos esses valores são `0`, que são o UID e GID do usuário **root**.

Como você pode ver, o arquivo `/proc/[pid]/status` pode ser até mais fácil de ler do que a saída do utilitário `ps`: 

```
:~$ ps -F 1
UID          PID    PPID  C    SZ   RSS PSR STIME TTY      STAT   TIME CMD
root           1       0  0 41276 10800   1 jan01 ?        Ss     0:44 /lib/systemd/systemd --system --deserialize 96
```

Aqui, nós utilizamos a opção `-F` (equivale à opção `u`, no estilo BSD) para termos uma saída com informações mais detalhadas sobre o processo.

## 3.7 - Inspecionando o processo do shell corrente

Como vimos na aula passada, quando um terminal gráfico ou um console tty é iniciado, com ele é iniciado um shell -- no caso do sistema operacional GNU, esse shell é o Bash. Então, vamos aproveitar o que aprendemos até aqui para entender outros conceitos ligados ao processos.

Antes porém, temos um problema: como saber o PID do shell em execução no terminal?

Felizmente, o shell tem uma variável especial que nos fornece exatamente essa informação, a variável `$`. Para acessar o seu valor, nós precedemos o seu nome com outro `$`. Assim:

```
:~$ echo $$
303225
```

Então, como `303225` é o PID do shell no meu terminal, eu poderia fazer:

```
:~$ ps -F 303225
UID          PID    PPID  C    SZ   RSS PSR STIME TTY      STAT   TIME CMD
blau      303225  299426  0  2166  5680   2 11:27 pts/1    Ss     0:00 bash
```

Ou então, poderia ler o conteúdo de `/proc/303225/status`. Mas existe um jeito mais prático nessa situação: acessar o valor de `$` diretamente na linha de comando do `ps`:

```
:~$ ps -F $$
UID          PID    PPID  C    SZ   RSS PSR STIME TTY      STAT   TIME CMD
blau      303225  299426  0  2166  5680   2 11:27 pts/1    Ss     0:00 bash
```

No caso da leitura do arquivo `/proc/[pid]/status`...

```
:~$ cat /proc/$$/status
```

Como a informação que queremos no momento já está na saída do `ps`, não há necessidade de lermos o arquivo `status`. Observe a coluna identificada como `PPID`. Este é o número do processo "pai" do shell no terminal. Como você deve se lembrar, na segunda aula nós dissemos que *"o shell é iniciado como um subprocesso do terminal"*. Agora, sendo mais exatos, nós podemos dizer que o shell é iniciado como um processo filho do processo do terminal.

Para comprovar, vejamos quem é o executável no processo `299426`:

```
:~$ ps -F 299426
UID          PID    PPID  C    SZ   RSS PSR STIME TTY      STAT   TIME CMD
blau      299426  299425  0 103955 58320  2 08:50 ?        Sl     0:12 xfce4-terminal
```

Na última coluna, nós temos: `xfce4-terminal`.

Mas isso também pode ser feito de uma forma mais prática, porque o shell também tem uma variável que armazena o número do processo pai: a variável PPID. Observe nos exemplos:

```
:~$ echo $PPID
299426
:~$ ps $PPID
    PID TTY      STAT   TIME COMMAND
 299426 ?        Sl     0:12 xfce4-terminal
```

## 3.8 - Todo processo tem um pai

Os processos geralmente são criados quando um processo em execução de uma operação **fork** ou  **execute** -- ou seja, todo processo é criado a partir de outro processo.

### Operação 'fork'

Quando um processo é *"forkado"*, a configuração do espaço da memória e os descritores de arquivos são copiados (clonados) para o novo processo e a ele será atribuído um novo PID.

### Operação 'execute'

Quando um processo é *"executado"*, a imagem do processo atual é substituída pela imagem do novo processo.

### Demonstração

É possível compreender a diferença entre **fork** e **execute** observando duas formas de execução de programas no shell. Por exemplo, abra um terminal e execute:

```
:~$ ls
```

Quando você tecla `Enter`, o processo do comando será *"forkado"* do processo do shell (que será o processo pai). Quando o `ls` termina o seu trabalho, seu processo é encerrado e o controle da execução volta para o shell, que fica esperando pelo nosso próximo comando.

Mas, experimente fazer o mesmo com o comando interno do Bash `exec`:

```
:~$ exec ls
```

Não, o seu terminal não está com defeito. O comando interno `exec` realiza uma operação do tipo **execute**, o que faz com que o novo processo (o processo do `ls`) substitua o processo do shell. Então, quando o `ls` termina o seu trabalho, o terminal, que era o pai do processo do shell, também é encerrado.

## 3.9 - A genealogia dos processos

Se todo processo é filho de algum outro processo, todos os processos remontam, por genealogia, ao **PID 1**.

Mas, se todo processo tem um pai, de onde vem o PID 1?

### A origem de tudo

Para começar, lembre-se de que todos os processos são gerenciados pelo kernel. No sistema GNU/Linux, em certa etapa da [sequência de boot](https://www.youtube.com/watch?v=neLmvi4OJDo), o kernel é carregado na memória e recebe um sistema de arquivos virtual (também na memória) chamado **initramfs**. O **initramfs**, por sua vez, possui todos os programas e scripts necessários para que o kernel execute seus primeiros comandos. Alguns desses comandos estão no script `init`, que inicia (com uma operação **execute**) o primeiro processo do espaço de usuário do sistema: o **PID 1**.

> Terminar o PID 1 é a mesma coisa que terminar o sistema. 

### Processos órfãos

Embora todo processo tenha que ser iniciado por outro, é comum acontecer de um processo pai ser terminado enquanto o processo filho ainda está em execução, o que os deixa, como o nome diz, órfãos.

Isso pode acontecer acidentalmente (quando o processo pai é fechado por algum erro) ou propositalmente, como quando deixamos um processo que não requeira interação manual sendo executado ao fundo (como os *daemons*, por exemplo).

Como não pode haver processos sem um processo pai, uma das funções do PID 1 é "adotar" os processos órfãos, ou seja, o **PPID** desses processos passa a ser `1`.

### Processos zumbis

Quando o código de saída de processo filho (uma chamada `exit()`), por algum motivo, não consegue ser processado pelo processo pai, o processo filho entra no estado de "terminado", mas continua aparecendo na tabela dos processos ativos -- são os processos "zumbis".

Nós ainda falaremos sobre os sinais e o utilitário `kill`, mas, só para não deixarmos os zumbis à solta por aí, nós podemos tentar eliminar um processo zumbi avisando ao processo pai que o processo filho terminou:

```  
kill -s SIGCHLD <PPID>  
```

Onde:

* **kill** - ferramenta para enviar sinais para processos;
* **-s** - opção para especificar o sinal;
* **SIGCHLD** - sinal para avisar ao processo pai que o processo filho terminou;
* **PPID** - PID do processo pai do zumbi.

Antes de matarmos um zumbi, nós temos que localizá-lo, e podemos fazer isso com o `ps`:

```
:~$ ps -ely | grep '^Z'
Z  1000  304604  299426  0  80   0 -     0 -      ?        00:00:00 bash <defunct>
```

As opções `-e`, `-l` e `-y` são para listar todos os processos e exibir as informações no formato "largo" suprimindo o campo da *flag* na primeira coluna, resultando em:

```
S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
```

Aqui, a coluna `S` é o estado do processo. Se esse valor for `Z`, significa que é um processo zumbi. Por isso eu especifiquei o padrão `^Z`  no `grep`. Trata-se de uma expressão regular, onde o padrão que eu procuro é qualquer linha que comece (`^`) com o caractere `Z`.

Por acaso, eu tenho um *walker* aqui mesmo neste momento! Trata-se de uma sessão do Bash, mas eu tenho dois PIDs nesta linha: `304604` e `299426` -- qual deles é do processo pai do Bash?

Pelo hábito, e pelo formato de saída do `ps` eu já sei que o PPID é o `299426` (quarta coluna), mas é sempre bom conferir:
  
```
:~$ ps 299426
    PID TTY      STAT   TIME COMMAND
 299426 ?        Sl     0:17 xfce4-terminal
```

Bingo! Agora, é só avisar ao terminal que o Bash já terminou:

```
:~$ kill -s SIGCHLD 299426
:~$
```

Conferindo...

```
:~$ ps -ely | grep '^Z'
:~$
```

Sem zumbis!

> Nem sempre isso funciona e, nesse caso, nós teremos que considerar "matar" o  processo pai.
