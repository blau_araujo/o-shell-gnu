# Aula 8 - Outras expansões do shell

- [Vídeo - parte 1](https://youtu.be/WhpkSW91C2M)
- [Vídeo - parte 2](https://youtu.be/V_lO8004doQ)
- [Vídeo - parte 3](https://youtu.be/74uoXRqFLK0)
- [Vídeo - parte 4](https://youtu.be/-LS-0akSQ9s)
- [Vídeo - parte 5](https://youtu.be/ID_ZzPBl7sk)
- [Playlist completa](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)

## 8.1 - A ordem de processamento das expansões

Apesar das variáveis serem os elementos que mais evidenciam as expansões de parâmetros, o shell processa outros elementos que também serão expandidos em parâmetros daquilo que deverá ser executado. A lista abaixo mostra as outras expansões do shell na ordem em que elas são processadas:

* Expansão de chaves;
* Expansão do til;
* Expansão de parâmetros;
* Expansões aritméticas;
* Substituição de comandos;
* Substituição de processos;
* Divisão de palavras;
* Expansões de nomes de arquivos;
* Remoção de aspas.

Repare que as expansões de parâmetros, aquelas que envolvem mais diretamente o acesso às variáveis, estão no terceiro lugar da ordem de processamento, sendo antecedidas da expansão de chaves (`{`...`}`) e da expansão do til (`~`). Entender essa ordem pode nos ajudar a prever o comportamento do shell na formação de linhas de comandos. Por exemplo, não é possível expandir parâmetros dentro de uma expansão de chaves, porque as chaves são expandidas antes e, por isso, não chegam a "ver" os parâmetros expandidos:

```
:~$ echo {1..5}
1 2 3 4 5
:~$ a=10
:~$ echo {1..$a}
{1..10}
```

Como veremos adiante, a expansão de chaves exige que o início e o fim de uma sequência sejam caracteres do mesmo tipo (letras ou números). Como a expansão da variável `a` não havia acontecido, o `$` e o `a` eram apenas caracteres de um tipo diferente do valor inicial da sequência, e o shell processou todo o conjunto como uma palavra qualquer. Só quando chegou o momento de expandir os parâmetros, o shell substituiu `$a` pelo valor `10`, e foi isso que comando `echo` exibiu.

Algo semelhante aconteceria com a expansão do til, observe:

```
:~$ echo ~root
/root
:~$ echo ~blau
/home/blau
:~$ u=blau
:~$ echo ~$u
~blau
```

Contudo, como as expansões de parâmetros acontecem antes das demais expansões, nós podemos utilizar variáveis tranquilamente em todas elas, por exemplo, numa expansão de nomes de arquivos:

```
:~$ d=gits
:~$ echo $d/*
gits/artigos gits/debxpress gits/dotfiles gits/fuzzy-tools gits/fuzzy-tools-plugins gits/slideshell
```

## 8.2 - Expansão de chaves

Uma expansão de chaves é um mecanismo que o shell oferece para a geração de strings que contenham algum tipo de sequência ou uma lista de strings separadas por espaço. Para que ela aconteça, a linha do comando deve conter um padrão formado por um preâmbulo (opcional) seguido de uma expressão representando uma sequência entre chaves e um apêndice (também opcional):

**Para gerar listas de strings:**

```
preâmbulo{string1,string2,...,stringN}apêndice
```
Diferente da expansão de sequências, as expansões de listas podem conter expansões de parâmetros e padrões de frormação de nomes de arquivos (expansão de nomes de arquivos, mais adiante):

```
:~$ a=10;b=20;c=30
:~$ echo {$a,$b,$c}
10 20 30
```

**Para gerar sequências:**

```
preâmbulo{sequência}apêndice
```

O preâmbulo e o apêndice serão repetidos em todas as strings geradas na expansão:

```
:~$ echo {1..5}
1 2 3 4 5
:~$ echo a{1..5}
a1 a2 a3 a4 a5
:~$ echo {1..5}b
1b 2b 3b 4b 5b
:~$ echo a{1..5}b
a1b a2b a3b a4b a5b
```

Ou então...

```
:~$ echo a{x,y,z}b
axb ayb azb
```

O ponto mais importante é que as expansões de chaves devem ser representadas na forma de uma única palavra:

```
:~$ echo a {x,y,z} b
a x y z b

:~$ echo a {1..5} b
a 1 2 3 4 5 b
```

As sequências podem ser intervalos de caracteres da tabela ASCII, intervalos de números inteiros, ou listas de strings:

```
# Intervalo de inteiros...
:~$ echo {1..5}
1 2 3 4 5


# Intervalo de caracteres da tabela ASCII...
:~$ echo {a..e}
a b c d e


# Intervalo de caracteres na ordem reversa...
:~$ echo {e..a}
e d c b a


# Isso é o que existe na tabela ASCII entre 'Z' e 'a'.
# Também existe uma '\', mas ela é omitida porque acaba
# escapando um espaço na string gerada.
:~$ echo {Z..a}
Z [  ] ^ _ ` a
    ↑
  Espaço escapado
    

# Uma lista de strings...
:~$ echo ba{na,nda,ca}na
banana bandana bacana
```

Quando usamos as expansões de chaves, nós devemos ficar atentos a três regrinhas básicas:

1. Os espaços no padrão devem ser escapados.
2. Os caracteres do início de do fim das sequências devem ser do mesmo tipo.
3. As expansões de chaves são realizadas antes de que qualquer outra expansão, portanto, não podemos expandir variáveis no padrão.

Aqui estão alguns exemplos de expansões **inválidas**:

```
# Início e fim não são do mesmo tipo...
:~$ echo {1..z}
{1..z}

# Tentando expandir parâmetros numa sequência...
:~$  a=12; b=23
:~$ echo {$a..$b}
{12..23}

# Espaço não escapado no preâmbulo...
:~$ echo a {1..5}
a 1 2 3 4 5
```

O escape dos espaços pode ser feito de várias formas:

```
:~$ echo a\ {1..5}
a 1 a 2 a 3 a 4 a 5

:~$ echo a' '{1..5}
a 1 a 2 a 3 a 4 a 5

:~$ echo a{b\ a,c\ b,d\ c}b
ab ab ac bb ad cb
```

Quando a sequência é um intervalo, nós ainda podemos definir um salto entre os valores ou os caracteres:

```
:~$ echo {1..10..2}
1 3 5 7 9

:~$ echo {a..j..2}
a c e g i
```

Quando o intervalo é de números inteiros, nós podemos preencher dígitos à esquerda com zeros:

```
:~$ echo {072..112..10}
072 082 092 102 112
```

## 8.3 - Expansão do til

É bem provável que você saiba que o til (`~`) é expandido para o caminho completo das nossas pastas pessoais:

```
# Expandir o til...
:~$ echo ~
/home/blau

# É o mesmo que expandir a variável HOME...
:~$ echo $HOME
/home/blau
```

### Expandindo a pasta pessoal de outros usuários

O que talvez você não saiba, é que o Bash nos permite fazer algumas coisas bem interessantes com o til. Por exemplo, digamos que o seu sistema seja compartilhado com outros usuários – você também pode expandir as pastas pessoas deles:

```
:~$ echo ~blau
/home/blau
:~$ echo ~nena
/home/nena
:~$ echo ~root
/root
```

### Expandindo a pasta corrente

Agora, vamos supor que você seja um adepto do minimalismo e, por isso, removeu todas as referências ao diretório de trabalho corrente do seu prompt. Para saber onde está, claro, você sempre pode expandir a variável do shell `PWD` ou executar o comando interno `pwd`, mas também podemos obter a mesma informação com a expansão `~+`:

```
:~/docs$ echo $PWD
/home/blau/docs

:~/docs$ pwd
/home/blau/docs

:~/docs$ echo ~+
/home/blau/docs
```

### Expandindo o último diretório visitado

O til também permite a expansão do último diretório visitado:

```
:~$ cd docs
:~/docs$ echo ~-
/home/blau
:~/docs$ cd -
:~$ echo ~-
/home/blau/docs
```

O comando interno `cd` faz com que o caractere especial `–` (traço) seja expandido para o conteúdo da variável interna `OLDPWD` (*antigo diretório de trabalho*), que é utilizada pelo shell para armazenar o último diretório visitado -- que é basicamente o que a expansão `~-` faz. Então, os comandos abaixo são equivalentes:

```
:~$ cd docs
:~/docs$ cd ~-
:~$ cd ~-
:~/docs$

:~$ cd docs
:~/docs$ cd $OLDPWD
:~$ cd $OLDPWD
:~/docs$

:~$ cd docs
:~/docs$ cd -
:~$ cd -
:~/docs$
```

### Expandindo diretórios empilhados

Além do comando `cd`, o shell possui mais dois comandos internos que alteram o diretório corrente: `pushd` e `popd`. A diferença deles para o `cd` é que, além da troca do diretório corrente, eles também armazenam os diretórios visitados numa pilha que pode ser listada com o comando interno `dirs`.

Enquanto `pushd` e `popd` não são usados, a pilha de diretórios contém apenas o diretório corrente:

```
:~$ dirs
~
```

Com `pushd`, nós mudamos o diretório corrente e o inserimos no topo da pilha:

```
:~$ pushd testes
~/testes ~
:~/testes$ pushd ~/downloads
~/downloads ~/testes ~
:~/downloads$ pushd ~
~ ~/downloads ~/testes ~
:~$
```

Com `popd`, nós podemos remover uma das entradas da pilha a partir de seu número, que começa a ser contado a partir do zero. A contagem pode ser positiva (+N), para a localização ser feita da esquerda para a direita, ou negativa (-N), para contarmos na direção contrária:

```
:~$ dirs
~ ~/downloads ~/testes ~
:~$ popd +2
~ ~/downloads ~
```

Com o comando `dirs`, nós também podemos exibir apenas uma das entradas da pilha utilizando o mesmo sistema de indexação de `popd`:

```
:~$ dirs
~ ~/downloads ~/testes ~
:~$ dirs +1
~/downloads
:~$ dirs -1
~/testes
```

O til também pode ser expandido para uma dessas entradas da pilha, tal como o comando `dirs` exibiria com a opção `-l`:

```
:~$ dirs -l
/home/blau /home/blau/notas /home/blau/testes

:~$ echo ~0
/home/blau
:~$ echo ~-0
/home/blau/testes
:~$ echo ~1
/home/blau/notas
```

### Atenção para a ordem da expansão do til

Não podemos nos esquecer de que a expansão do til é a segunda a ser feita: depois da expansão de sequências entre chaves e antes das expansões de parâmetros. Isso significa que não podemos concatenar a expansão de uma variável com uma expansão do til. 

Por exemplo:

```
:~$ echo ~$USER
~blau
```

Como a variável `USER` não estava expandidam o til foi interpretado como o caractere literal `~`.

> A ordem que o Bash obedece no processamento das expansões é que me faz optar quase sempre pela expansão da variável `HOME` em vez da expansão do til nos meus códigos – os parâmetros são expandidos na mesma etapa, o que torna tudo muito mais simples, seguro e consistente.

## 8.4 - Expansão de parâmetros

A terceira expansão realizada pelo shell é aquela que utiliza os valores registrados em variáveis para gerar os parâmetros na linha do comando. Nós dedicamos toda a aula 7 a este tipo de expansão, mas este pareceu ser o momento ideal para conhecermos um detalhe bem interessante sobre uma das modificações possíveis na expansão de valores em variáveis.

### Expandindo parâmetros com preâmbulos ou apêndices

Como vimos, a expansão de chaves permite a inclusão de palavras antes e depois de cada um dos valores expandidos, mas também é possível obter o mesmo resultado com a expansão dos elementos de uma variável vetorial. Essa possibilidade deriva de uma característica comum a todas as expansões envolvendo vetores: **se o índice no subscrito for `@` ou `*`, a modificação da expansão se aplica a todos os elementos**.

Por exemplo:

```
:~$ vetor=(banana laranja abacate)
:~$ echo ${vetor[@]^}
Banana Laranja Abacate
:~$ echo ${vetor[@]#*a}
nana ranja bacate
:~$ echo ${vetor[@]//a/X}
bXnXnX lXrXnjX XbXcXtX
```

Especificamente no caso da expansão com substituição de padrões (último exemplo acima), existe uma variação muito interessante em que é possível casar o padrão com o início ou o final dos valores utilizando os modificadores `#` e `%`:

```
:~$ var=arara
echo ${var/#a/X}
Xrara
:~$ echo ${var/%a/X}
ararX
```

Caso não exista a especificação de um padrão, a modificação será aplicada ao início ou ao final do valor na variável:

```
:~$ var=arara
:~$ echo ${var/%/X}
araraX
:~$ echo ${var/#/X}
Xarara

```

Se a variável for vetorial, o efeito será aplicado a cada um dos elementos:

```
:~$ vetor=(banana laranja abacate)
:~$ echo ${vetor[@]/#/X}
Xbanana Xlaranja Xabacate
:~$ echo ${vetor[@]/%/X}
bananaX laranjaX abacateX 
```

O que equivale a...

```
:~$ echo X{banana,laranja,abacate}
Xbanana Xlaranja Xabacate
:~$ echo {banana,laranja,abacate}X
bananaX laranjaX abacateX
```

## 8.5 - Substituição de comandos

A substituição de comandos é um mecanismo que permite a execução de um comando em um subshell para obter sua saída na forma de uma expansão. No Bash, a substituição acontece quando prefixamos um agrupamento de comandos entre parêntesis com um cifrão:

``` 
$(comandos)
```

Existe uma outra sintaxe mais antiga (limitada, e considerada obsoleta por alguns) onde os comandos são circundados por um par de acentos graves, mas nós falaremos sobre ela mais adiante.

### Armazenando e expandindo a saída de comandos

Quando a expansão acontece, todos os parâmetros da sessão corrente do shell são copiados para um subshell (o que nos permite expandi-los dentro do agrupamento com parêntesis) e a saída padrão do subshell é tratada como um valor passível de ser armazenado em uma variável:

```
:~$ var=$(grep $USER /etc/passwd)
:~$ echo $var
:~$ blau:x:1000:1000:Blau Araujo,,,:/home/blau:/bin/bash
```

Como a substituição de comandos expressa um valor, em vez de ser armazenada, ela também pode ser expandida diretamente – da mesma forma que fazemos com parâmetros:

```
:~$ echo O nome do usuário é $(whoami).
:~$ O nome do usuário é blau.
```

### Cuidado com o escopo das variáveis

O ponto mais importante sobre o uso de substituições de comandos é o cuidado com o escopo das variáveis. Não podemos nos esquecer de que o ambiente da sessão que iniciou a substituição é copiado para o subshell, ou seja, nós temos a impressão de que as variáveis estão disponíveis na substituição de comandos, mas são apenas cópias com os mesmos nomes.

Observe este exemplo:

```
:~$ echo PID mãe: $$ - PID subshell: $(echo $$)
PID mãe: 11365 - PID subshell: 11365
```

Como o PID da sessão mãe foi copiado, o parâmetro especial `$` do subshell tem o mesmo PID da sessão mãe, o que pode nos levar a imaginar equivocadamente que se trata de uma mesma sessão, e não de um subshell (que teria seu próprio PID). Com a variável `BASHPID`, que sempre é gerada no momento em que uma sessão do Bash é iniciada, nós temos como ver a diferença:

```
:~$ echo PID mãe: $BASHPID - PID subshell: $(echo $BASHPID)
PID mãe: 11365 - PID subshell: 22155
```

Consequentemente, as alterações feitas na cópia de uma variável não se refletem nos valores originais:

```
:~$ bicho=gato
:~$ echo $(echo $bicho; bicho=zebra; echo $bicho)
gato zebra
:~$ echo $bicho
gato
```

Aliás, aproveitando o exemplo, repare que a substituição de comandos produziu duas saídas a partir de dois comandos `echo`.
 
### Saída em múltiplas linhas

Sabendo que o comando `echo` inclui uma quebra de linha na string enviada para a saída padrão, é interessante notar que elas (as quebras) foram colapsadas em um espaço no processo de separação de palavras do shell. Para que continuassem sendo caracteres de quebras de linha, e não operadores de controle do shell, nós teríamos que envolver a substituição de comandos com aspas duplas:

```
:~$ bicho=gato
:~$ echo "$(echo $bicho; bicho=zebra; echo $bicho)"
gato
zebra
```

Mas, como as quebras de linha faziam parte das saídas dos comandos `echo` no interior da substituição de comando, elas estão armazenadas na variável, bastando expandir `var` entre aspas duplas para que o shell não as trate as quebras de linha como operadores de controle:

```
:~$ bicho=gato
:~$ var=$(echo $bicho; bicho=zebra; echo $bicho)
:~$ echo $var
gato zebra
:~$ echo "$var"
gato
zebra
```

### Expandindo o conteúdo de arquivos

Esta capacidade da substituição de comandos, de expandir valores com múltiplas linhas, a torna muito útil para a visualização do conteúdo de arquivos, inclusive como uma alternativa mais rápida para o utilitário `cat`:

```
:~$ echo "$(< compras.txt)"
leite
pão
ovos
banana
laranja
```

### Comprando 'cat' por lebre

No exemplo anterior, `$(< arquivo)` se parece com um redirecionamento, se parece com uma substituição de comandos e ainda funciona como uma expansão, mas não é nada disso!

Na verdade, trata-se de uma implementação em Bash de um operador especial do Korn Shell (ksh) para a leitura do conteúdo de arquivos. A semelhança com uma substituição de comandos, e o fato de ser apresentado como tal no manual e no código fonte do Bash, é de dar um nó na cabeça de mentes mais atentas e curiosas, pois, se fosse realmente um redirecionamento, o conteúdo do arquivo estaria sendo enviado para a entrada padrão (stdin) e nós não veríamos nada:

```
:~$ < /etc/passwd
:~$
```

Logo, também não haveria nada na saída da substituição de comandos para ser expandido.

### A sintaxe antiga

Sobre a sintaxe entre acentos graves, ela é considerada limitada e obsoleta, e há bons motivos para isso. Para começar, a forma `$(comando)` é POSIX, é mais legível e fácil de identificar no código, além de permitir aninhamentos com muita facilidade. Mas, o principal problema da sintaxe antiga é o inferno das aspas e do escape de caracteres.

Na forma antiga, a barra invertida tem valor literal, a menos que venha seguida do cifrão (`$`), do acento grave (**`**) ou de outra barra invertida (`\`). Ou seja, ao estabelecer as próprias regras para lidar com as aspas e o caractere de escape, a forma antiga introduz uma complexidade totalmente desnecessária oferecendo muito pouco em troca.

É ótimo que você saiba que os acentos graves também são uma forma válida de substituição de comandos, mas só para entender o código dos outros -- fora isso, devemos evitar seu uso.

## 8.6 - Expansões aritméticas

Uma expansão aritmética é o mecanismo pelo qual o Bash substitui a avaliação de uma expressão aritmética com o comando composto `((` pelo valor resultante da avaliação da expressão. Para que isso aconteça, o comando composto deve ser prefixado pelo cifrão:

```
$((expressão))
```

Assim, o resultado da expansão pode ser exibido ou armazenado em uma variável, exatamente como vimos na substituição de comandos – a diferença, porém, é que a expansão aritmética não inicia um subshell.

Apenas com o comando composto `((`, o valor da expressão não é enviado para a saída padrão e só pode ser capturado se atribuído a uma variável:

```
:~$ ((2 + 2))
:~$
:~$ ((a = 2 + 2))
:~$ echo $a
4
```

> **Importante!** Não confunda o 'estado de saída' do comando composto `((` com o valor da 'avaliação da expressão'. O estado de saída será sempre `1` (erro) se a expressão resultar no valor zero, e será `0` (sucesso) se a avaliação resultar em qualquer valor diferente de zero.

Com a expansão aritmética nós temos acesso ao valor da expressão:

```
:~$ echo $((2 + 2))
4

:~$ echo O resultado da multiplicação é $((4 * 5)).
O resultado da multiplicação é 20.

:~$ div=$((15 / 3))
:~$ echo 15 / 3 = $div
15 / 3 = 5
```

A expansão em si é muito simples e há pouco a ser dito sobre ela, mas nós a utilizaremos bastante quando falarmos de **operações aritméticas** no Bash.

## 8.7 - Substituição de processos

Uma expansão pouco documentada é a substituição de processos, cuja finalidade básica é fazer com que uma lista de comandos seja tratada como um arquivo:

```
<(lista) → Substitui um arquivo para leitura.
>(lista) → Substitui um arquivo para escrita.
```

> **Importante!** Não há espaços entre os caracteres `<` e `>` e o restante da expansão! Apesar da semelhança, essa expansão não tem (quase) nada a ver com redirecionamentos.

A expansão é feita trocando a ocorrência da substituição de processos pelo nome de um arquivo criado pelo Bash para uso temporário. Com isso, nós podemos utilizar as substituições de processos em comandos e programas que exigem nomes de arquivos como argumentos, tanto para a leitura quanto para a escrita de dados.

A lista de comandos geralmente é formada pelo encadeamento de comandos através de pipes (|), que é quando a substituição de processos mostra todo o seu poder.

Nós falaremos de pipes e redirecionamentos nas próximas aulas, mas, de forma resumida, quando uma lista de comandos está encadeada por pipes, cada comando após o operador `|` é executado em um subshell, utilizando como entrada de dados a saída do comando anterior.

Infelizmente, nem todos os sistemas têm o shell configurado para permitir esse tipo de expansão -- não é um recurso compatível com as normas POSIX, e só funciona no Bash se a opção `posix` estiver desabilitada:

```
set -o posix    # Ativa e restringe alguns recursos do Bash.
set +o posix    # Desativa o modo POSIX.
```

Na versão 10 do Debian GNU/Linux, o modo POSIX do Bash vem desativado por padrão nos modos interativo e não-interativo, o que permite o uso de substituições de processos:

```
:~$ shopt -o posix
posix          	off
:~$ bash -c 'shopt -o posix'
posix          	off
```

Um experimento interessante é tentar descobrir o nome do arquivo que o shell cria para expandir uma substituição de processos, observe:

```
# Expandindo um nome de arquivo para leitura.
:~$ echo <(:)
/dev/fd/63

# Expandindo um nome de arquivo para escrita.
:~$ echo >(:)
/dev/fd/63
```

O comando nulo (`:`) só está no exemplo para que possamos fazer uma substituição de processos e forçar o shell a expandir o nome do arquivo que ele vai utilizar – no caso `/dev/fd/63`.

O diretório `/dev/fd` contém os chamados descritores de arquivos (entre eles, os descritores de arquivos `0`, `1` e `2`, que recebem os fluxos de entrada e saída de dados: stdin, stdout e stderr). Portanto, o nosso experimento demonstrou que o shell criou o um descritor de arquivo de nome `63`, tanto para o nome de arquivo de leitura quanto para o de escrita.

Caso ainda não esteja claro, quando um nome de arquivo é expandido para leitura, ele pode ser utilizado como fonte de dados para um comando ou um programa, enquanto que um nome de arquivo expandido para escrita, será utilizado para receber os dados enviados por um comando ou do programa.

Isso quer dizer que, se o comando esperar mais de um arquivo nos seus argumentos (o que é muito comum, por exemplo, em programas de conversão de mídia, onde temos que informar o arquivo original e o arquivo de destino), nós podemos utilizar várias substituições de processos:

```
:~$ echo <(:) >(:)
/dev/fd/63 /dev/fd/62
```

Note que o nome inicial (o número) continuou o mesmo: o arquivo `63`, mas a numeração do nome seguinte caiu para `62`, e continuaria caindo se expandíssemos mais nomes.

> **Importantíssimo!** A substituição de processos em si não tem nada de complicado – ela apenas gera um nome de arquivo e pronto. O que realmente causa muita confusão são os caracteres `<` e `>` da sua sintaxe, porque eles quase sempre causam a impressão de serem operadores de redirecionamento. Eles não são!

## 8.8 - Divisão de palavras

Quando as expansões de parâmetros, as substituições de comandos ou as expansões aritméticas não acontecem entre aspas duplas, o resultado pode conter um ou mais caracteres que, se estiverem definidos numa variável especial chamada `IFS` (de *internal field separator*, em inglês), funcionarão como terminadores de palavras. O shell, portanto, faz uma varredura do resultado dessas expansões utilizando os caracteres em `IFS` para dividi-lo em palavras.

Por padrão, os separadores definidos na variável `IFS` são os caracteres espaço, tabulação e quebra de linha. Então, seja quais forem os caracteres utilizados como separadores numa linha de texto, eles serão removidos do início e do fim da string resultante das expansões, e os que sobrarem no "interior" da string, estes sim, serão tratados como separadores na divisão das palavras. No final, após a divisão, eles também serão removidos, de modo que restem apenas as palavras. Se o valor em `IFS` for nulo, não haverá divisão de palavras!

É uma prática bastante comum manipular o valor em `IFS` para forçar o  shell a dividir o resultado de uma expansão da forma desejada. Isso não é errado nem oferece riscos, mas é um recurso que, pessoalmente, eu sempre tento evitar. O problema não está em poder ou não alterar o valor em `IFS`, mas na motivação para isso. Na maioria das vezes, o programador é levado a esse tipo de solução apenas por não ter feito um tratamento correto dos dados ou porque seu código está desnecessariamente complexo. Além disso, nós sempre devemos ponderar se uma solução em "Bash puro" é realmente a solução mais adequada. Especialmente no trabalho com dados em texto, eu sempre me pergunto: será que não está na hora de recorrer a um utilitário como o `awk` ou o `sed`, por exemplo?

Como diz o nosso amigo, Paulo Kretcheu:

> "O fato de ser possível fazer alguma coisa não significa que nós somos obrigados a fazer essa coisa."

## 8.9 - Expansão de nomes de arquivos

Quando o shell analisa a linha de um comando, há um momento em que ele busca pelos caracteres `*`, `?` e `[`. Se, depois de processadas todas as demais expansões, um desses caracteres aparecer numa palavra, ela será identificada como um **padrão** e será comparada com uma lista de nomes de arquivos.

Para cada nome de arquivo que corresponder ao padrão, uma string com o nome desse arquivo será gerada na expansão, logo, isso significa que o shell é capaz de identificar onde começa e onde termina um nome de arquivo, mesmo que ele contenha espaços entre seus caracteres, e cada nome será uma palavra na expansão resultante. Se nada for encontrado, o Bash mantém a palavra na sua forma original na linha do comando ou, se estiver configurado para isso, gera uma string vazia.

> É a opção `nullglob` que determina se haverá ou não a expansão para uma string vazia no caso do padrão não possuir correspondência. Ela vem desligada por padrão na maioria dos sistemas, mas pode ser habilitada com o comando `shopt`.

O que quase sempre passa despercebido, é o fato de que a expansão de nomes de arquivos é apenas um mecanismo de geração de strings – como a expansão de chaves que vimos antes, por exemplo. Então, quando nós digitamos o comando `ls *.txt` no terminal, não é o utilitário ls que vai tentar entender o que é `*.txt` para sair procurando arquivos com a terminação `.txt`. Na verdade, o `ls` nem chega a saber que nós digitamos `*.txt`, pois, antes dele receber qualquer argumento, o shell vai proceder a expansão dos nomes de arquivos.

Por exemplo, considere um diretório com os arquivos abaixo:

```
aula.doc
aula.md
aula.txt
exemplo.doc
exemplo.md
exemplo.txt
teste.doc
teste.md
teste.txt
```

Se quisermos listar todos os nomes de arquivos que terminam com `.md`, nós executamos:

```
:~$ ls *.md
```

Antes do comando ser executado, o shell substitui `*.md` pela lista de nomes de arquivos que ele gerou ao casar o padrão com a lista dos arquivos existentes no diretório. Então, no exemplo, é como se nós tivéssemos digitado:

```
:~$ ls aula.md exemplo.md teste.md
```

### Listando arquivos com os comandos 'echo' e 'printf'

Na verdade, se o objetivo for apenas exibir uma lista de nomes de arquivos que correspondem a determinado padrão, nós nem precisamos do `ls`, pois o shell, através da expansão, é quem gera as strings com os nomes dos arquivos.

Sendo assim, o comando abaixo é perfeitamente válido para esta finalidade:

```
:~$ echo *.md
aula.md exemplo.md teste.md
```

Melhor ainda com as opções de formatação que o comando `printf` oferece:

```  
:~$ printf "%s\n" *.md
aula.md
exemplo.md
teste.md
```


Aqui, nosso objetivo ainda não é falar sobre o `printf`, mas apenas dar uma ideia do que é possível fazer com as expansões de nomes de arquivos. Só para adiantar o assunto, o `printf` exibe textos conforme uma string de formatação definida como primeiro argumento. Essa string pode conter, entre outras coisas, os **especificadores de formato**.

No nosso caso, nós utilizamos o especificador `%s`, que irá reservar um espaço para cada argumento seguinte e irá tratá-los como strings. Então, concatenando os demais argumentos com o caractere de quebra de linha, nós temos o resultado do exemplo.

Agora, imagine que você procura um arquivo iniciado com o caractere `b`. Como não existe nenhum no nosso diretório hipotético, a expansão não acontece e o padrão é exibido intacto:

```
:~$ echo b*
b*
:~$ echo $?
0
```

Com o ls, nós teríamos um erro:

```
:~$ ls b*
ls: não foi possível acessar 'b*': Arquivo ou diretório
inexistente
:~$ echo $?
2
```

### A opção 'nullglob'

Repare, na mensagem de erro acima, que `b*` foi o argumento que o utilitário recebeu. Mas, este comportamento de manter a string do padrão pode ser alterado com a opção do shell `nullglob`. Com ela habilitada, padrões sem correspondência resultam em uma string vazia:

```
# Comportamento padrão...
:~$ echo b*
b*

# Habilitando ‘nullglob’...
:~$ shopt -s nullglob

# Novo resultado da expansão...
:~$ echo b*

:~$
```

Consequentemente, neste caso o utilitário `ls` não teria argumentos e exibiria:

```
:~$ ls b*
aula.doc  aula.txt     exemplo.md   teste.doc  teste.txt
aula.md   exemplo.doc  exemplo.txt  teste.md
```

Para desabilitar a opção `nullglob` e ter a string original de volta:

```
:~$ shopt -u nullglob
```

### Formação de padrões

A formação básica dos padrões para os nomes de arquivos utiliza apenas os três símbolos da tabela abaixo:


| Símbolo | Significado |
|---|---|
| `*`| Zero ou mais caracteres. |
| `?` | Um caractere. |
| `[…]` | Qualquer um dos caracteres listados entre os colchetes. |

Mas, existem alguns detalhes que precisamos conhecer sobre eles, especialmente sobre o asterisco (`*`) e a lista (`[…]`).

**O asterisco (*)...**

* Casa com qualquer string, inclusive uma string vazia.

* O padrão `*/` casa apenas com diretórios.

* Se a opção do shell `globstar` estiver habilitada, dois asteriscos seguidos (`**`) casarão com todos os arquivos de zero ou mais diretórios e seus subdiretórios.

* Com `globstar` habilitado, o padrão `**/` casa apenas com diretórios e subdiretórios.

**A lista ([…])...**

* Casa com um dos caracteres entre os colchetes.

* O nome "oficial" da lista é **classe de caracteres**.

* Se o primeiro caractere listado for `^` ou `!`, a lista representará os caracteres proibidos no casamento do padrão.

* Um par de caracteres da mesma classe separados por um traço (`-`) indica uma faixa de caracteres da tabela definida pelos padrões de localização do sistema.

* Para forçar o uso da tabela de caracteres da localidade `C` (ASCII), a variável de ambiente `LC_ALL` deve receber o valor `C`.

* Para diferenciar um traço literal (`-`) do traço utilizado em faixas de caracteres, ele deve ser o primeiro caractere da lista.

* Da mesma forma, para diferenciar um caractere `]` do colchete que fecha a lista, ele deve vir no começo da lista.

* A lista pode conter as classes de caracteres POSIX (já veremos o que é isso).

### Arquivos ocultos (opção 'dotglob')

Por padrão, a expansão de nomes de arquivos ignora os arquivos ocultos, a menos que o ponto (`.`) seja incluído no início do padrão. Mas, se a opção do shell `dotglob` estiver habilitada (e não é comum estar), os arquivos ocultos também participarão do casamento dos padrões.

> O ponto não tem significado especial nas expansões de nomes de arquivos. Para o shell, eles só têm algum valor quando são o primeiro caractere do nome de um arquivo, o que indica que o arquivo é oculto e não deve ser, em princípio, incluído na listagem que será comparada com o padrão. Portanto, o único sentido de escrever padrões do tipo `*.*`, é se você estiver procurando arquivos visíveis que tenham um ponto em qualquer posição de seus nomes.

### Ignorando nomes de arquivos com 'GLOBIGNORE'

A variável especial `GLOBIGNORE`, do Bash, pode ser utilizada para excluir certos padrões do resultado de uma expansão de nomes de arquivos.

Por exemplo, se quisermos ignorar os nomes iniciados com o caractere `a` no padrão `*md`, basta atribuir o padrão `a*` à variável:

```
:~$ echo *md
aula.md exemplo.md teste.md
:~$ GLOBIGNORE=a*
:~$ echo *md
exemplo.md teste.md
```

Mas isso tem alguns efeitos colaterais. para começar, é que o padrão a ser ignorado é aplicado a todas as expansões de nomes de arquivo subsequentes:

```
:~$ echo *
exemplo.doc exemplo.md exemplo.txt teste.doc
teste.md teste.txt
```

Outro efeito colateral é que, quando `GLOBIGNORE` está definida, os arquivos ocultos (iniciados com ponto) passam a figurar nas expansões:

```
# Criando um arquivo oculto...
:~$ >> .oculto

:~$ echo *
exemplo.doc exemplo.md exemplo.txt .oculto teste.doc
teste.md teste.txt                     ↑
```

Nós também podemos especificar vários padrões a serem ignorados separando-os com dois pontos (`:`):

```
GLOBIGNORE=padrão1:padrão2:…
```

Para restaurar o comportamento normal da expansão de nomes de arquivos, basta destruir `GLOBIGNORE`:

```
# Destruindo...
:~$ unset GLOBIGNORE
```

### Globs estendidos

Os padrões estendidos para expansão de nomes de arquivos (também chamados de **globs estendidos**) nos oferecem algumas formas adicionais para formar padrões que seriam impossíveis de serem representados apenas com os três símbolos que já conhecemos. O problema, porém, é que a disponibilidade desse recurso depende de uma opção do shell estar habilitada: a opção `extglob`.

O mais comum, é que ela venha habilitada para uso no modo interativo e desabilitada no modo não-interativo, mas você pode testar se este é o seu caso:

```
# Testando o estado no modo interativo...
:~$ shopt extglob
extglob        	on

# Testando o estado no modo não-interativo...
:~$ bash -c 'shopt extglob'
extglob        	off
```

Atente para o fato de que o comando `bash -c` faz com que a string passada como argumento seja executada como uma linha de comando no shell **não-interativo**.

Nos nossos scripts, nós podemos habilitar a opção extglob, mas isso só pode ser feito antes e fora de qualquer função ou comando composto!

Por exemplo:

```
#!/usr/bin/env bash

# Habilitando 'extglob'...
shopt -s extglob

… seu código que depende dos globs estendidos …

# Desabilitar é opcional...
shopt -u extglob
```

> Normalmente, você não precisa se preocupar em desabilitar a opção `extglob` no seu script. Além da mudança só afetar a sessão do shell em curso, pode haver complicações se você precisar habilitá-la novamente.

A tabela abaixo mostra o que podemos fazer com os globs estendidos:

| Sintaxe | Descrição |
|---|---|
| `?(lista-de-padrões)` | Casa com zero ou uma ocorrência dos padrões. |
| `*(lista-de-padrões)` | Casa com zero ou mais ocorrências dos padrões. |
| `+(lista-de-padrões)` | Casa com uma ou mais ocorrências dos padrões. |
| `@(lista-de-padrões)` | Casa com um dos padrões na lista. |
| `!(lista-de-padrões)` | Casa com tudo menos os padrões na lista. |

Aqui, `lista-de-padrões` é um ou mais padrões de nome de arquivos formados com os três símbolos básicos já vistos. Os padrões são separados entre si por uma barra vertical (|) que pode ser lida como a palavra "ou".

Utilizando o diretório hipotético abaixo, vejamos alguns exemplos:

```
:~$ echo *
aaa aab aac aba abb abc aca acb acc baa bab
bac bba bbb bbc bca bcb bcc caa cab cac cba
cbb cbc cca ccb ccc

# Nomes que contenham apenas caracteres 'a' ou 'b'...
:~$ echo +(a|b)
aaa aab aba abb baa bab bba bbb

# Qualquer nome que **não contenha apenas** 'a' ou 'b'...
:~$ echo !(+(a|b))
aac abc aca acb acc bac bbc bca bcb bcc caa
cab cac cba cbb cbc cca ccb ccc

# Nomes que contenham apenas o caractere 'c'...
:~$ echo +(c)
ccc

# Nomes que contenham apenas 'a', 'b' ou 'c'...
:~$ echo @(+(a)|+(b)|+(c))
aaa bbb ccc

# Nomes que comecem com 'aa' ou 'bb' e terminem com 'c'...
:~$ echo @(aa|bb)c
aac bbc
```

Porém, a melhor parte do uso dos globs estendidos é que eles podem ser utilizados em expressões de comparação de strings, permitindo a formação de padrões que só poderiam ser representados com expressões regulares:

```
:~$ str=banana
:~$ [[ $str == ba+(na|nda)na ]]; echo $?
0
:~$ str=bandana
:~$ [[ $str == ba+(na|nda)na ]]; echo $?
0
:~$ str=bacana
:~$ [[ $str == ba+(na|nda)na ]]; echo $?
1
```

## 8.10 - Remoção de caracteres de citação

A última expansão realizada pelo shell é a remoção dos caracteres `\`, `'` e `"` utilizados na sua função de remover o significado especial de operadores e metacaracteres do shell e que não resultarem de alguma das expansões anteriores.

Por exemplo:

```
:~$ fruta='abacate\ pera'
:~$ echo "banana $frutas laranja"
banana abacate\ pera laranja
```
