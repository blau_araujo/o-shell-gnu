# Aula 4 - Processos têm vida

- [Vídeo desta aula](https://youtu.be/kwyi0AhW8p8)
- [Playlist completa](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)

## 4.1 - Execução de comandos no shell

Quando um comando é executado no shell, são aplicados os mesmos princípios da criação de processos que vimos na aula anterior.

* O shell é executado como um processo filho do terminal e fica aguardando comandos.
* Quando nós entramos com um comando, o shell faz um **fork** de si mesmo.
* O **fork** do shell faz uma chamada de sistema **exec** e será substituído pelo processo do comando que será executado.
* Quando o processo do comando termina sua execução, o controle volta para o processo do shell original.
* O shell original volta a esperar por comandos.

Observe no diagrama abaixo como isso funciona:

![](https://blauaraujo.com/wp-content/uploads/2021/01/processos-terminal.png)

É interessante notar que a maioria dos processos iniciados diretamente pelo usuário passará pelas chamadas **fork** e **exec**, mesmo quando o programa não for invocado pela linha de comando do shell. Se o processo for criado apenas por um **fork** (sem **exec**), a coluna **F** da saída do comando `ps` exibirá o valor `1`.

> O processo pelo qual os valores da coluna **F** (de 'flag') do comando `ps` são determinados é totalmente obscuro. Sabemos apenas que se trata de uma operação **OU bit-a-bit** de certos valores de controle do processo e o significado de dois valores: `1`, processo forcado mais não executado, e `4`, processo executado com privilégios de super usuário.

## 4.2 - O ciclo de vida de um processo

Quando falamos do **ciclo de vida** de um processo, estamos nos referindo aos cinco estados gerais pelos quais ele pode passar, desde o momento em que é criado até a sua terminação e remoção da tabela de processos.

### Estados

* **R** - Em execução (*Running*) ou executável (*Runnable*)
* **S** - Sono interruptível (*Interruptible Sleep*)
* **D** - Sono não-interruptível (*Uninterruptible Sleep*)
* **T** - Parado (*Stopped*)
* **Z** - Zumbi (*Zombie*)

> O processo ainda pode entrar no estado `I` (de *idle*, ocioso), que tem basicamente a finalidade de reduzir o consumo de energia quando não há processos a serem executados em uma CPU.

O diagrama abaixo mostra como podem ocorrer as transições de estados de um processo:

![](https://blauaraujo.com/wp-content/uploads/2021/01/estados-2.png)

#### Estado de sono não interruptível (D)

O processo não pode ser morto nem interrompido, e é o estado em que um processo entra quando precisa acessar dados em um dispositivo de entrada e saída (I/O). Quando um processo entra nesse estado e, por alguma falha, não consegue sair dele, não existe uma forma fácil de matá-lo e pode ser necessário um *reboot*.

#### Estado de execução ou executável ( R )

Processos no estado **R**, ou estão sendo executados no momento, ou estão de prontidão na fila de espera por um núcleo da CPU para serem executados.

#### Estado de sono interruptível (S)

Os processos entram nesse estado quando esperam por um evento ou um sinal para continuarem.

#### Estado parado (T)

Quando um processo é parado ou suspenso manualmente, ele entra no estado **T**, que corresponde ao estágio de **t**erminação no ciclo de vida de um processo. No exemplo abaixo, o processo do editor `nano` foi suspenso com o atalho `Ctrl`+`Z`. Listando os processos no terminal, veja que ele aparece com o estado **T**:

```
:~$ ps -ely | grep pts
S  1000  529409  529400  0  80   0  5316  2080 -      pts/0    00:00:00 bash
T  1000  530341  529409  0  80   0  3508  1701 -      pts/0    00:00:00 nano
R  1000  530373  529409  0  80   0  1232  2389 -      pts/0    00:00:00 ps
S  1000  530374  529409  0  80   0   664  1583 -      pts/0    00:00:00 grep
```

#### Estado de zumbi (Z)

Acontece quando um processo filho é terminado, mas o processo pai, por algum motivo, ainda não consegue ler seu estado de saída.

## 4.3 - Sinais

A comunicação entre os processos (e com os processos) é feita por meio de sinais. Nós podemos interferir nos processos enviando sinais para eles com a ferramenta `kill`.

No GNU/Linux, existem dois `kill`: o programa utilitário e o comando interno da maioria dos shells derivados do Bash -- as diferenças são sutis, mas existem. Com o comando interno `type`, é possível ver quais deles estão à disposição no seu sistema. No meu, eu obtive esses resultados:

```
:~$ type -a kill
kill é um comando interno do shell
kill é /usr/bin/kill
kill é /bin/kill
```

> O comando 'builtin' `type` com a opção `-a` exibe todos os caminhos para executáveis, apelidos ('aliases'), funções e comandos internos que contenham o nome consultado.


Para o contexto do nosso curso, porém, o que interessa é o *builtin* do Bash (por isso, a partir de agora eu vou me referir a ele como *"comando kill"*). Felizmente, é o comando `kill` que será executado por padrão, a menos que o caminho do utilitário seja informado. Veja abaixo como eles diferem na exibição da lista de sinais, por exemplo:

```
:~$ kill -l
 1) SIGHUP         2) SIGINT         3) SIGQUIT        4) SIGILL         5) SIGTRAP
 6) SIGABRT        7) SIGBUS         8) SIGFPE         9) SIGKILL       10) SIGUSR1
11) SIGSEGV       12) SIGUSR2       13) SIGPIPE       14) SIGALRM       15) SIGTERM
16) SIGSTKFLT     17) SIGCHLD       18) SIGCONT       19) SIGSTOP       20) SIGTSTP
21) SIGTTIN       22) SIGTTOU       23) SIGURG        24) SIGXCPU       25) SIGXFSZ
26) SIGVTALRM     27) SIGPROF       28) SIGWINCH      29) SIGIO         30) SIGPWR
31) SIGSYS        34) SIGRTMIN      35) SIGRTMIN+1    36) SIGRTMIN+2    37) SIGRTMIN+3
38) SIGRTMIN+4    39) SIGRTMIN+5    40) SIGRTMIN+6    41) SIGRTMIN+7    42) SIGRTMIN+8
43) SIGRTMIN+9    44) SIGRTMIN+10   45) SIGRTMIN+11   46) SIGRTMIN+12   47) SIGRTMIN+13
48) SIGRTMIN+14   49) SIGRTMIN+15   50) SIGRTMAX-14   51) SIGRTMAX-13   52) SIGRTMAX-12
53) SIGRTMAX-11   54) SIGRTMAX-10   55) SIGRTMAX-9    56) SIGRTMAX-8    57) SIGRTMAX-7
58) SIGRTMAX-6    59) SIGRTMAX-5    60) SIGRTMAX-4    61) SIGRTMAX-3    62) SIGRTMAX-2
63) SIGRTMAX-1    64) SIGRTMAX

:~$ /usr/bin/kill -L
 1 HUP      2 INT      3 QUIT     4 ILL      5 TRAP     6 ABRT     7 BUS
 8 FPE      9 KILL    10 USR1    11 SEGV    12 USR2    13 PIPE    14 ALRM
15 TERM    16 STKFLT  17 CHLD    18 CONT    19 STOP    20 TSTP    21 TTIN
22 TTOU    23 URG     24 XCPU    25 XFSZ    26 VTALRM  27 PROF    28 WINCH
29 POLL    30 PWR     31 SYS  
```

> Para exibir os sinais na forma de uma tabela, o **programa** `kill` utiliza a opção `-L`.

Você encontra mais informações sobre o comando `kill` com o comando interno `help kill`. Mas, se quiser saber mais sobre o programa, o comando é `man kill`.

### A sintaxe do 'kill'

Nós podemos enviar sinais para processos de duas formas: pelo número (SIGNUM) ou pelo nome (SIGSPEC) do sinal:

```
kill -s SIGSPEC PID

kill -n SIGNUM PID
```

Mas também é possível enviar sinais sem as opções `-s` e `-n`:

```
kill -SIGSPEC PID

kill -SIGNUM PID
```

Se nenhum sinal for especificado, por padrão será envidado o sinal `SIGTERM` (parar processo), daí o nome do comando ser `kill`:

```
kill PID
```

### Sinais para terminação de processos

A tabela abaixo mostra alguns sinais úteis para terminar e interromper processos:

| SIGSPEC | SIGNUM | Descrição |
|---|---|---|
| SIGHUP | 1 | De *"hang up"* (desconectado). Este sinal é enviado para o processo quando o terminal que o controla é fechado. Quando enviado "manualmente", ele causa a recarga das configurações do processo. |
| SIGINT | 2 | Utilizado para interromper um processo. |
| SIGKILL | 9 | Força a terminação de um processo e não pode ser ignorado. |
| SIGTERM | 15 | É o sinal padrão para um término de um processo. |
| SIGCHLD | 17 | Enviado para um processo pai quando o processo filho é terminado. Quando enviado "manualmente", instrui o sistema operacional a limpar os recursos de um processo zumbi. |

### Sinais associados a atalhos de teclado

Como vimos na segunda aula, esses atalhos são determinados nas configurações de linha de um terminal, e podem ou não estar disponíveis em todos os sistemas GNU/Linux. Para conferir a sua configuração, execute o comando `stty -a`.

| Atalho | SIGSPEC | SIGNUM | Descrição |
|---|---|---|---|
| `Ctrl`+`C` | SIGINT | 2 | Interrompe a execução do processo. |
| `Ctrl`+`Z` | SIGTSTP | 20 | Suspende a execução do processo e o coloca em segundo plano. |
| `Ctrl`+`S` | SIGSTOP | 19 | Pausa a execução do processo. |
| `Ctrl`+`Q` | SIGCONT | 18 | Continua a execução de um processo pausado por SIGSTOP. |
| `Ctrl`+`\` | SIGQUIT | 3 | Sinal padrão para termina um processo por ação do usuário e realizar um *dump* ("despejo") do conteúdo da memória em um arquivo. |

> Existem documentos e tutoriais conflitantes sobre o atalho `Ctrl`+`D` gerar ou não um sinal `SIGQUIT`. No shell, um `Ctrl`+`D` tem o mesmo efeito do comando interno `exit`, e também pode ser utilizado para encerrar sessões de outros shells iniciadas na linha de comando, sessões de SSH ou o próprio shell em execução, por exemplo. Se o programa estiver lendo um arquivo ou a entrada padrão (stdin), o `Ctrl`+`D` tem o efeito do byte de controle de "fim de transmissão" (&#9220;) que, em sistemas 'unix-like', faz com que o conteúdo do 'buffer' seja disponibilizado para escrita imediatamente sem a necessidade de um `Enter` (quebra de linha).

### Sinais 'real-time'

Existem sinais de propósito geral que podem ser utilizados por programadores para informar a um dado processo a ocorrência de um evento que ele esteja monitorando. Em plataformas POSIX, a faixa de sinais que vão de SIGRTMIN a SIGRTMAX. Como esses sinais podem ter valores diferentes em diferentes plataformas, eles são utilizados pelo seu nome seguido de um valor inteiro dentro dos limites exibidos em `kill -l`. No meu caso, como visto acima:

```
35) SIGRTMIN+1
36) SIGRTMIN+2
37) SIGRTMIN+3
38) SIGRTMIN+4
39) SIGRTMIN+5
...
45) SIGRTMIN+11
46) SIGRTMIN+12
47) SIGRTMIN+13
48) SIGRTMIN+14
49) SIGRTMIN+15

50) SIGRTMAX-14
51) SIGRTMAX-13
52) SIGRTMAX-12
53) SIGRTMAX-11
54) SIGRTMAX-10
...
59) SIGRTMAX-5
60) SIGRTMAX-4
61) SIGRTMAX-3
62) SIGRTMAX-2
63) SIGRTMAX-1
```

O trecho abaixo está na configuração do meu gerenciador de janelas, o **i3wm**, onde eu utilizo o SIGRTMIN para sinalizar vários tipos de eventos:

```
# i3blocks - keylock leds status...

bindsym --release Caps_Lock   $exe pkill -SIGRTMIN+11 i3blocks
bindsym --release Num_Lock    $exe pkill -SIGRTMIN+11 i3blocks

# i3blocks - upgrades...

$sup+Shift+u $exe pkill -SIGRTMIN+14 i3blocks

# volume control and i3blocks volume status...

set $vol_signal pkill -SIGRTMIN+10 i3blocks

$sup+F9  $exe pavucontrol                           && $vol_signal
$sup+F10 $exe amixer set Master toggle              && $vol_signal
$sup+F11 $exe amixer set Master playback 5%- unmute && $vol_signal
$sup+F12 $exe amixer set Master playback 5%+ unmute && $vol_signal
```

Repare que, em todos esses trechos, eu utilizo o `pkill`, que também é um utilitário para enviar sinais a processos, só que com base no nome do processo em vez de seu PID. No caso, eu envio esses sinais *real-time* para o processo do meu painel, o **i3blocks**, por exemplo:

```
pkill -SIGRTMIN+10 i3blocks
```

Uma outra ferramenta muito conhecida para enviar sinais para processos pelos seus nomes é o `killall`. 

> Com base no que vimos aqui, você já saberia explicar um dos comandos mais encontrados nos tutoriais da internet, o `killall -9 <nome-do-programa>`?

## 4.4 - A árvore da vida

Como vimos, todo processo é criado a partir de outro processo, o que forma uma verdadeira "árvore genealógica" de processos, onde todos os ramos podem ser rastreados de volta até o PID 1.

Existe um utilitário que possibilita a visualização desta árvore, o `pstree`:

```
:~$ pstree -A
systemd-+-ModemManager---2*[{ModemManager}]
        |-NetworkManager---2*[{NetworkManager}]
        |-avahi-daemon---avahi-daemon
        |-cron
        |-dbus-daemon
        |-i3bar---sh---i3blocks
        |-login---bash---startx---xinit-+-Xorg---6*[{Xorg}]
        |                               `-x-window-manage---ssh-agent
        |-php-fpm7.4---2*[php-fpm7.4]
        |-polkitd---2*[{polkitd}]
        |-rsyslogd---3*[{rsyslogd}]
        |-rtkit-daemon---2*[{rtkit-daemon}]
        |-sh---polkit-gnome-au---7*[{polkit-gnome-au}]
        |-sh---xfce4-power-man---7*[{xfce4-power-man}]
        |-sh---nm-applet---8*[{nm-applet}]
        |-sh---xfce4-clipman---7*[{xfce4-clipman}]
        |-sh---compton---5*[{compton}]
        |-sshd---sshd---sshd---bash---pstree
        |-systemd-+-(sd-pam)
        |         |-at-spi-bus-laun-+-dbus-daemon
        |         |                 `-3*[{at-spi-bus-laun}]
        |         |-at-spi2-registr---2*[{at-spi2-registr}]
        |         |-dbus-daemon
        |         |-gvfsd---2*[{gvfsd}]
        |         |-gvfsd-fuse---5*[{gvfsd-fuse}]
        |         |-pulseaudio---2*[{pulseaudio}]
        |         |-xfce4-notifyd---2*[{xfce4-notifyd}]
        |         `-xfconfd---2*[{xfconfd}]
        |-systemd-journal
        |-systemd-logind
        |-systemd-timesyn---{systemd-timesyn}
        |-systemd-udevd
        |-udisksd---4*[{udisksd}]
        |-upowerd---2*[{upowerd}]
        `-wpa_supplicant
```

> A opção `-A` serve para que o desenho da árvore seja feito com caracteres ASCII, o que fica melhor para a exibição nesta apostila.

Por padrão, o `pstree` agrupa ramos com o mesmo nome entre colchetes antecedidos pelo número de ramificações e um `*`, que pode ser lido como o sinal de multiplicação (vezes), por exemplo:

```
Xorg---6*[{Xorg}]  
```

Isso é o mesmo que...

```
Xorg-+-{Xorg}
     |-{Xorg}
     |-{Xorg}
     |-{Xorg}
     |-{Xorg}
     `-{Xorg}
```

Esse comportamento pode ser alterado com a opção `-c`.

### Threads (linhas de execução)

Um mesmo programa pode precisar se dividir para realizar duas ou mais tarefas simultâneas. Então, em vez de criar um novo processo de si mesmo para executar cada uma dessas tarefas, se o sistema suportar, o que ele faz é criar novas **linhas de execução**, ou ***threads***. Isso oferece algumas vantagens sobre a criação de novos processos:

* Simplifica o modelo de desenvolvimento;
* *Threads* não são associadas a recursos do sistema;
* *Threads* podem ser criadas e destruídas mais rapidamente;
* A sobreposição das tarefas "acelera" o processamento.

Na saída do `pstree`, as linhas de execução "filhas" de um processo são representadas entre colchetes:

```
{Xorg}
```

> Neste curso, nós não pretendemos falar muito sobre *threads*. Para nós, o importante é investigar a sua relação com os processos -- ainda mais pelo fato delas aparecerem na saída do `pstree`.

### Grupos de 'threads'

Já que estamos utilizando a ramificação do `Xorg` como exemplo, vejamos o que podemos descobrir sobre suas linhas de execução. Para começar, vamos descobrir o PID do processo `Xorg`:

```
:~$ ps -e | grep Xorg
   1072 tty1     00:00:55 Xorg
```

Sabendo o PID, nós podemos exibir apenas o ramo do processo do `Xorg` com o `pstree`. Também vamos utilizar a opção `-p` para que os PIDs sejam exibidos:

```
:~$ pstree -Acp 1072
Xorg(1072)-+-{Xorg}(1073)
           |-{Xorg}(1074)
           |-{Xorg}(1075)
           |-{Xorg}(1076)
           |-{Xorg}(1077)
           `-{Xorg}(1082)
```

Como você pode ver, cada linha de execução do `Xorg` também possui um PID. Então, vamos tentar ver os seus diretórios em `/proc`. Para facilitar o nosso trabalho, vamos utilizar o comando `echo` em vez do `ls`, que daria uma saída muito mais difícil de colar aqui na apostila:

```
:~$ echo /proc/10*
/proc/10 /proc/1019 /proc/1020 /proc/1039 /proc/1040 /proc/1043
/proc/1046 /proc/1049 /proc/105 /proc/107 /proc/1071 /proc/1072
/proc/1083

```

Onde estão?!

Na verdade, os PIDs das *threads* aparecem dentro do diretório `/proc/[PID]/task` ("tarefa", em inglês):

```
:~$ ls -l /proc/1072/task/
total 0
dr-xr-xr-x 7 blau blau 0 jan 11 08:08 1072
dr-xr-xr-x 7 blau blau 0 jan 11 08:08 1073
dr-xr-xr-x 7 blau blau 0 jan 11 08:08 1074
dr-xr-xr-x 7 blau blau 0 jan 11 08:08 1075
dr-xr-xr-x 7 blau blau 0 jan 11 08:08 1076
dr-xr-xr-x 7 blau blau 0 jan 11 08:08 1077
dr-xr-xr-x 7 blau blau 0 jan 11 08:08 1082
```

Intrigante (por vários motivos), mas vejamos um outro processo que não tenha *threads* no `pstree`, o processo do `cron`, por exemplo:

```
:~$ ps -e | grep cron
    360 ?        00:00:01 cron
:~$ ls -l /proc/360/task/
total 0
dr-xr-xr-x 7 root root 0 jan 11 08:08 360
```

Este é o primeiro ponto importante: no sistema GNU/Linux, todo processo tem pelo menos uma tarefa associada a ele e que recebe o mesmo número de identificação (`1072`, no caso do `Xorg` e `360`, no caso do `cron`). Por identificar um grupo de tarefas, este número é chamado de **TGID** (*thread group ID*).

Mas a coisa pode ficar ainda mais intrigante!

Como vimos os "PID's" das *threads* não aparecem em `/proc`, mas acompanhe...

```
:~$ pstree -Acp 1072
Xorg(1072)-+-{Xorg}(1073)
           |-{Xorg}(1074)
           |-{Xorg}(1075)
           |-{Xorg}(1076)
           |-{Xorg}(1077)
           `-{Xorg}(1082)
:~$ cat /proc/1072/task/1073/comm
Xorg:rcs0
```

Até aqui, tudo bem, mas observe...

```
:~$ cat /proc/1073/comm
Xorg:rcs0
```

> Não importa o conteúdo de `/proc/1073/comm`, e sim o fato dele ter sido lido sem que exista um diretório `/proc/1073`.

### Processos leves (LWP)

O comportamento que vimos decorre da forma como a implementação das *threads* é feita no kernel. No Linux, as *threads* são fluxos de execução de processos, ou seja, no fim das contas, todo processo é uma *thread* e toda *thread* é implementada como um processo.

Isso quer dizer que os processos também podem compartilhar os recursos do seu contexto de execução e criar os fluxos das suas próprias tarefas que, por semelhança com os conceitos clássicos em sistemas *unix-like*, recebem o nome de *light weight processes* (processos leves), ou **LWP**. Os LWP's também são gerenciados pelo kernel através das rotas de seus respectivos processos pais, como se fossem processos filhos comuns, a diferença é que eles não recebem recursos próprios.

Então, lembrando que o conteúdo do diretório `/proc` é um sistema de arquivos virtual que serve como estrutura de dados e interface para o kernel gerenciar os processos, mesmo que os LWP's não estejam visíveis no diretório principal, o kernel conhece as rotas até eles e retorna o conteúdo dos seus diretórios e arquivos.

Por padrão, o utilitário `ps` não exibe os LWP's. Veja os campos exibidos por padrão:

```
:~$ ps -e | head -n 1
    PID TTY          TIME CMD
```

Para exibir as linhas de execução dos processos, nós utilizamos a opção `-L`:

```
:~$ ps -eL | head -n 1
    PID     LWP TTY          TIME CMD
```

> O utilitário `head`, do 'coreutils', exibe as 10 primeiras linhas da saída de um comando, mas nós podemos alterar esse limite com a opção `-n`.

Vejamos o que o `ps` nos mostra sobre o processo `Xorg`. Desta vez, para termos o cabeçalho na saída do nosso comando, nós vamos utilizar uma expressão regular que fará com que o `grep` encontre  correspondência com os padrões `Xorg` ou (`|`) `CMD`. Para esse tipo de busca, nós precisamos da opção `-E` do `grep`.


```
:~$ ps -eL | grep -E 'Xorg|CMD'
    PID     LWP TTY          TIME CMD
   1072    1072 tty1     00:00:52 Xorg
   1072    1073 tty1     00:00:04 Xorg:rcs0
   1072    1074 tty1     00:00:00 Xorg:disk$0
   1072    1075 tty1     00:00:00 Xorg:disk$1
   1072    1076 tty1     00:00:00 Xorg:disk$2
   1072    1077 tty1     00:00:00 Xorg:disk$3
```

Isso deve deixar um pouco mais clara a distinção ente os processos "de fato" e as linhas de execução desses processos. 


## 4.5 - Sessões e grupos de processos

Em sistemas *unix-like*, o mais comum é que os usuários interajam com grupos de processos relacionados entre si. Mesmo quando abrimos um terminal para utilizar um único processo, o shell, nós acabamos criando vários processos com as nossas ações, por exemplo:

* Executando scripts (modo não-interativo);
* Alternando entre tarefas em primeiro e segundo plano;
* Executando comandos encadeados por pipe.

O gerenciamento desses processos é bem mais complicado do que uma simples relação de processos pais e filhos, mas o kernel consegue se organizar criando **grupos de processos** e **sessões**.

### Sessões

Uma sessão geralmente está associada ao conjunto de tarefas iniciadas a partir do processo de um shell, desde o momento em que ele é iniciado até ser terminado. Originalmente, o conceito de *sessão* tinha relação com o fato de termos que fazer o *login* em um console tty, executarmos nossas tarefas e fazer um *logout*. Portanto, o ***líder*** da sessão será o primeiro processo inciado após o login -- no caso, o shell.

Por exemplo, vamos abrir um terminal e buscar no conteúdo do arquivo `/proc/$$/status` a linha que contém o campo `NSsid` (namespace session id):

```
:~$ echo $$
588205
:~$ grep NSsid /proc/$$/status
NSsid:	588205
```

Como podemos ver, o `PID` e o `NSsid` do Bash são iguais, e isso significa que o processo `588205` é o líder da sessão. Mas também podemos obter essa informação com o `ps`:

```
:~$ ps -o stat,pid,ppid,sid,command
STAT     PID    PPID     SID COMMAND
Ss    588205  578246  588205 bash
R+    588274  588205  588205 ps -o stat,pid,ppid,pgid,sid,command
```

> Por padrão, o `ps` (sem a opção `-e`, por exemplo) só exibe os processos em execução no terminal em uso. Com a opção `-o`, nós podemos definir as colunas que serão exibidas.

Desta forma, nós temos a informação do ID da sessão (**SID**) e, em dois lugares, de quem é o líder da sessão -- primeiro, com o fato do SID e do PID serem iguais, mas também pelo caractere que aparece depois do estado do processo (`Ss`).

> O sinal de `+` que aparece depois do estado `R` indica que o processo está em execução em primeiro plano ('foregroud').

Para investigar como ficaria com mais processos na sessão, vamos iniciar outro Bash a partir da linha de comando e repetir os experimentos:

```
:~$ # Iniciando um outro Bash...
:~$ bash

:~$ # Checando o PID e o PPID do Bash atual...
:~$ echo $$ $PPID
588550 588205

:~$ # Checando o NSsid do Bash atual...
:~$ grep NSsid /proc/$$/status
NSsid:	588205

:~$ # O que o 'ps' nos mostra...
:~$ ps -o stat,pid,ppid,sid,command
STAT     PID    PPID     SID COMMAND
Ss    588205  578246  588205 bash
S     588550  588205  588205 bash
R+    588579  588550  588205 ps -o stat,pid,ppid,sid,command
```

Note que o campo `STAT` do processo do segundo shell (`588550`) não tem o caractere `s`, o que indica que ele é apenas um **membro** da sessão corrente, cujo SID é `588205`.

> Embora não tenha precisão técnica, é aceitável nos referirmos a diferentes processos do shell de uma mesma sessão de "sessões do shell".

### Grupos de processos

A modularidade é um dos princípios de desenvolvimento mais fundamentais para sistemas *unix-like*, como vimos na primeira aula. A meta original era criar programas simples e especializados que pudessem ser encadeados para a realização de tarefas complexas, o que se concretizou com a implementação de mecanismos como os *pipes*, por exemplo. Sendo assim, uma linha de comando pode causar a criação de vários processos, todos eles pertencentes ao mesmo **grupo**, que será identificado (**PGID**) pelo PID de seu primeiro membro.

Para demonstrar a diferença entre grupo e sessão, vamos fazer um *pipe* totalmente inútil para um `grep` no mesmo comando que utilizamos quando falamos das sessões. Só que, desta vez, nós vamos incluir a coluna `pgid` no `ps`:

```
:~$ ps -o stat,pid,ppid,sid,pgid,command | grep ''
STAT     PID    PPID     SID    PGID COMMAND
Ss    588205  578246  588205  588205 bash
R+    588935  588205  588205  588935 ps -o stat,pid,ppid,sid,pgid,command
S+    588936  588205  588205  588935 grep --color=auto
```

Observe que, na coluna `PGID`, os comandos `ps` e `grep` pertencem ao mesmo grupo identificado por `588935`, que é o PID do primeiro membro do grupo -- o PID do comando `ps`. Atente, também, para o fato de que o SID é o mesmo para os três processos, o que nos diz que todos são membros de uma mesma sessão.

### Jobs (trabalhos)

Outra operação que causa a criação de grupos é quando nós colocamos processos em segundo plano com o atalho `Ctrl`+`Z` ou com a execução de comandos com o operador de controle `&` no final da linha de comando. Esse tipo especial de agrupamento recebe o nome de ***jobs*** (*trabalhos*), visto que são processos controlados pelo próprio utilizador.

O shell oferece algumas ferramentas para trabalharmos com eles, todas relacionadas exclusivamente com os **jobs da sessão corrente**, mas nós falaremos mais sobre isso nas próximas aulas.