# Curso Shell GNU

## Índice

### [Aula 1 - Shell GNU?](aula-01.md)

* [1.1 - Por que 'Shell GNU'?](aula-01.md#1-1-por-que-shell-gnu)
* [1.2 - Solução de problemas](aula-01.md#1-2-solu%C3%A7%C3%A3o-de-problemas)
* [1.3 - Nosso método](aula-01.md#1-3-nosso-m%C3%A9todo)
* [1.4 - O que é um sistema operacional](aula-01.md#1-4-o-que-%C3%A9-um-sistema-operacional)
* [1.5 - O sistema operacional GNU](aula-01.md#1-5-o-sistema-operacional-gnu)
* [1.6 - A Filosofia UNIX](aula-01.md#1-6-a-filosofia-unix)
* [1.7 - O que é um shell](aula-01.md#1-7-o-que-%C3%A9-um-shell)

### [Aula 2 - O que acontece antes do 'Enter'](aula-02.md)

* [2.1 - O shell é um programa como outro qualquer](aula-02.md#2-1-o-shell-%C3%A9-um-programa-como-outro-qualquer)
* [2.2 - Terminais e consoles](aula-02.md#2-2-terminais-e-consoles)
* [2.3 - Teletipos](aula-02.md#2-3-teletipos)
* [2.4 - Disciplina de linha](aula-02.md#2-4-disciplina-de-linha)
* [2.5 - Emuladores de terminal](aula-02.md#2-5-emuladores-de-terminal)
* [2.6 - Espaço do usuário](aula-02.md#2-6-espa%C3%A7o-do-usu%C3%A1rio)
* [2.7 - Pseudoterminais](aula-02.md#2-7-pseudoterminais)
* [2.8 - Tudo são arquivos](aula-02.md#2-8-tudo-s%C3%A3o-arquivos)
* [Notas e curiosidades](aula-02.md#notas-e-curiosidades)

### [Aula 3 - Programas e Processos](aula-03.md)

* [3.1 - O que "não são" processos](aula-03.md#3-1-o-que-n%C3%A3o-s%C3%A3o-processos)
* [3.2 - Executáveis são um formato de arquivo](aula-03.md#3-2-execut%C3%A1veis-s%C3%A3o-um-formato-de-arquivo)
* [3.3 - Programas interpretados](aula-03.md#3-3-programas-interpretados)
* [3.4 - Contexto de execução](aula-03.md#3-4-contexto-de-execu%C3%A7%C3%A3o)
* [3.5 - O que é um processo](aula-03.md#3-5-o-que-%C3%A9-um-processo)
* [3.6 - Os diretórios '/proc/[PID]'](aula-03.md#3-6-os-diret%C3%B3rios-proc-pid)
* [3.7 - Inspecionando o processo do shell corrente](aula-03.md#3-7-inspecionando-o-processo-do-shell-corrente)
* [3.8 - Todo processo tem um pai](aula-03.md#3-8-todo-processo-tem-um-pai)
* [3.9 - A genealogia dos processos](aula-03.md#3-9-a-genealogia-dos-processos)

### [Aula 4 - Processos têm vida](aula-04.md)

* [4.1 - A execução de comandos no shell](aula-04.md#4-1-execu%C3%A7%C3%A3o-de-comandos-no-shell)
* [4.2 - O ciclo de vida de um processo](aula-04.md#4-2-o-ciclo-de-vida-de-um-processo)
* [4.3 - Sinais](aula-04.md#4-3-sinais)
* [4.4 - A árvore da vida](aula-04.md#4-4-a-%C3%A1rvore-da-vida)
* [4.5 - Sessões e grupos de processos](aula-04.md#4-5-sess%C3%B5es-e-grupos-de-processos)

### [Aula 5 - Depois do 'Enter'](aula-05.md)

* [5.1 - Etapas de processamento de comandos](aula-05.md#5-1-etapas-de-processamento-de-comandos)
* [5.2 - Palavras e operadores (tokens)](aula-05.md#5-2-palavras-e-operadores-tokens)
* [5.3 - Regras de citação (quoting)](aula-05.md#5-3-regras-de-cita%C3%A7%C3%A3o-quoting)
* [5.4 - Expansão de apelidos (aliases)](aula-05.md#5-4-expans%C3%A3o-de-apelidos-aliases)
* [5.5 - Estados de saída](aula-05.md#5-5-estados-de-sa%C3%ADda)
* [5.6 - Operadores de controle](aula-05.md#5-6-operadores-de-controle)

### [Aula 6 - Comandos simples, complexos e compostos](aula-06.md)

* [6.1 - Comandos simples](aula-06.md#6-1-comandos-simples)
* [6.2 - Listas de comandos](aula-06.md#6-2-listas-de-comandos)
* [6.3 - Comandos compostos](aula-06.md#6-3-comandos-compostos)
* [6.4 - Agrupamentos de comandos](aula-06.md#6-4-agrupamentos-de-comandos)
* [6.5 - Subshells](aula-06.md#6-5-subshells)
* [6.6 - Estruturas de decisão](aula-06.md#6-6-estruturas-de-decis%C3%A3o)
* [6.7 - O comando composto 'if'](aula-06.md#6-7-o-comando-composto-if)
* [6.8 - O comando composto 'case'](aula-06.md#6-8-o-comando-composto-case)
* [6.9 - Estruturas de repetição](aula-06.md#6-9-estruturas-de-repeti%C3%A7%C3%A3o)
* [6.10 - O loop 'for'](aula-06.md#6-10-o-loop-for)
* [6.11 - Os loop 'while' e 'until'](aula-06.md#6-11-os-loops-while-e-until)
* [6.12 - O menu 'select'](aula-06.md#6-12-o-menu-select)
* [6.13 - Funções](aula-06.md#6-13-fun%C3%A7%C3%B5es)

### [Aula 7 - Expansões de parâmetros](aula-07.md)

* [7.1 - Argumentos e parâmetros](aula-07.md#7-1-argumentos-e-par%C3%A2metros)
* [7.2 - Variáveis no shell](aula-07.md#7-2-vari%C3%A1veis-no-shell)
* [7.3 - Vetores](aula-07.md#7-3-vetores-arrays)
* [7.4 - Expansão de valores em variáveis](aula-07.md#7-4-expans%C3%A3o-de-valores-em-vari%C3%A1veis)
* [7.5 - Indireções](aula-07.md#7-5-indire%C3%A7%C3%B5es)
* [7.6 - Número de elementos e de caracteres](aula-07.md#7-6-n%C3%BAmero-de-elementos-e-de-caracteres)
* [7.7 - Escopo de variáveis](aula-07.md#7-7-escopo-de-vari%C3%A1veis)
* [7.8 - Exportando variáveis apenas para um novo processo](aula-07.md#7-8-exportando-vari%C3%A1veis-apenas-para-um-novo-processo)
* [7.9 - Variáveis inalteráveis (read-only)](aula-07.md#7-9-vari%C3%A1veis-inalter%C3%A1veis-read-only)
* [7.10 - Destruindo variáveis](aula-07.md#7-10-destruindo-vari%C3%A1veis)
* [7.11 - Outras expansões de parâmetros](aula-07.md#7-11-outras-expans%C3%B5es-de-par%C3%A2metros)
* [7.12 - Substrings](aula-07.md#7-12-substrings)
* [7.13 - Faixas de elementos de vetores](aula-07.md#7-13-faixas-de-elementos-de-vetores)
* [7.14 - Remoções e substituições a partir de padrões](aula-07.md#7-14-remo%C3%A7%C3%B5es-e-substitui%C3%A7%C3%B5es-a-partir-de-padr%C3%B5es)
* [7.15 - Expandindo valores condicionalmente](aula-07.md#7-15-expandindo-valores-condicionalmente)
* [7.16 - Alterando a caixa do texto](aula-07.md#7-16-alterando-a-caixa-do-texto)
* [7.17 - Mais expansões de parâmetros](aula-07.md#7-17-mais-expans%C3%B5es-de-par%C3%A2metros)

### [Aula 8 - Outras expansões do shell](aula-08.md)

* [8.1 - A ordem de processamento das expansões](aula-08.md#8-1-a-ordem-de-processamento-das-expans%C3%B5es)
* [8.2 - Expansões de chaves](aula-08.md#8-2-expans%C3%A3o-de-chaves)
* [8.3 - Expansões do til](aula-08.md#8-3-expans%C3%A3o-do-til)
* [8.4 - Expansão de parâmetros](aula-08.md#8-4-expans%C3%A3o-de-par%C3%A2metros)
* [8.5 - Substituição de comandos](aula-08.md#8-5-substitui%C3%A7%C3%A3o-de-comandos)
* [8.6 - Expansões aritméticas](aula-08.md#8-6-expans%C3%B5es-aritm%C3%A9ticas)
* [8.7 - Substituição de processos](aula-08.md#8-7-substitui%C3%A7%C3%A3o-de-processos)
* [8.8 - Divisão de palavras](aula-08.md#8-8-divis%C3%A3o-de-palavras)
* [8.9 - Expansão de nomes de arquivos](aula-08.md#8-9-expans%C3%A3o-de-nomes-de-arquivos)
* [8.10 - Remoção de caracteres de citação](aula-08.md#8-10-remo%C3%A7%C3%A3o-de-caracteres-de-cita%C3%A7%C3%A3o)

### [Aula 9 - Redirecionamentos](aula-09.md)

* [9.1 - Descritores de arquivos](aula-09.md#9-1-descritores-de-arquivos)
* [9.2 - Fluxos de entrada e saída de dados](aula-09.md#9-2-fluxos-de-entrada-e-sa%C3%ADda-de-dados)
* [9.3 - Descritores de arquivos padrão](aula-09.md#9-3-descritores-de-arquivos-padr%C3%A3o)
* [9.4 - Operadores de redirecionamento](aula-09.md#9-4-operadores-de-redirecionamento)
* [9.5 - Criando descritores a arquivos para escrita](aula-09.md#9-5-criando-descritores-a-arquivos-para-escrita)
* [9.6 - Criando descritores de arquivos para leitura](aula-09.md#9-6-criando-descritores-de-arquivos-para-leitura)
* [9.7 - Criando descritores para leitura e escrita](aula-09.md#9-7-criando-descritores-para-leitura-e-escrita)
* [9.8 - Não confunda os operadores](aula-09.md#9-8-n%C3%A3o-confunda-os-operadores)
* [9.9 - Operações avançadas com descritores de arquivos](aula-09.md#9-9-opera%C3%A7%C3%B5es-avan%C3%A7adas-com-descritores-de-arquivos)
* [9.10 - 'Here docs'](aula-09.md#9-10-here-docs)
* [9.11 - 'Here strings'](aula-09.md#9-11-here-strings)

### [Aula 10 - Pipes](aula-10.md)

* [10.1 - Revendo as chamadas 'fork()' e 'exec()'](aula-10.md#10-1-revendo-as-chamadas-fork-e-exec)
* [10.2 - Definição e implementações](aula-10.md#10-2-defini%C3%A7%C3%A3o-e-implementa%C3%A7%C3%B5es)
* [10.3 - O sistema de arquivos 'pipefs'](aula-10.md#10-3-o-sistema-de-arquivos-pipefs)
* [10.4 - Dispositivos FIFO](aula-10.md#10-4-dispositivos-fifo)
* [10.5 - Here strings e here docs](aula-10.md#10-5-here-strings-e-here-docs)
