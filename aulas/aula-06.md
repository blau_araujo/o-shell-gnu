# Aula 6 - Comandos simples, complexos e compostos

- [Vídeo desta aula](https://youtu.be/XRZdmulV-Ao)
- [Playlist completa](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)

## 6.1 - Comandos simples

Como vimos na aula passada, o espaço é um operador de controle cujo papel se limita a separar as palavras recebidas pelo shell na forma de sequências de caracteres. Todos os demais operadores de controle, além de separar as palavras, também acumulam a função de **separar comandos**. Portanto, um comando é uma sequência de palavras delimitada por qualquer **operador de controle** que não seja o espaço.

> **Importante!** Outras categorias de operadores também separam palavras, mas apenas os **operadores de controle** delimitam comandos.

Na maioria das vezes, os operadores de controle delimitam comandos que contém apenas uma palavra identificável como o nome de um *bultin* do shell ou de um programa executável, seguida ou não de outras palavras que servirão como argumentos -- nestes casos, nós temos um **comando simples**.

Por exemplo...

```
echo Olá, mundo!
help
ps -elf
whoami
```

O estado de saída de um comando simples é o retorno da chamada da função do sistema `waitpid()`, que suspende a execução do processo chamador (processo pai) até que o processo filho mude de estado. Por padrão, `waitpid()` espera apenas pelo término do processo filho.

Dependendo do operador de controle, porém, é possível que exista uma relação entre os comandos simples identificados, o que daria origem aos cinco arranjos básicos na formação de comandos mais complexos:

* Listas de comandos
* Agrupamentos de comandos
* Estruturas condicionais
* Estruturas de repetição
* Pipelines

> Dessa listagem, os 'agrupamentos de comandos', as 'estruturas condicionais e de repetição', além dos comandos de avaliação de expressões `[[` e `((`, fazem parte da categoria dos **comandos compostos**, que será vista mais adiante. As 'pipelines' serão assunto da aula 10.

O Bash ainda categoriza mais dois tipos de comandos complexos:

* Coprocessamento (palavra reservada `coproc`)
* Paralelismo (utilitário GNU Parallel)

Ambos serão assunto da aula 12.

## 6.2 - Listas de comandos

Os operadores de controle tomados como referência para determinar o momento da execução de um comando, seja qual for o seu grau de complexidade, são:

* A quebra de linha
* O caractere `;`
* O operador de execução assíncrona `&`
* Os operadores de encadeamento condicional `&&` e `||`

Observe o exemplo abaixo:

```
:~$ bash -c 'echo banana whoami'
banana whoami
```

Aqui, o utilitário `whoami`, que exibe o nome do usuário logado na sessão do shell, não é executado porque não existe um operador de controle delimitando o fim de um comando e o início do outro. Sendo assim, a palavra `whoami` aparece como um argumento do comando `echo`.

Com o operador `;`, nós podemos delimitar os dois comandos, o que resultaria na execução de cada um deles separadamente:

```
:~$ bash -c 'echo banana;whoami'
banana
blau
```

Mas, o efeito seria o mesmo se, em vez do `;`, nós utilizássemos o caractere ANSI-C `\n` que, devidamente decodificado, é a quebra de linha:

```
:~$ bash -c $'echo banana\nwhoami'
banana
blau
```

Acontece que, **no terminal**, somente a quebra de linha determina o envio da sequência de caracteres para a interpretação do shell (tecla `Enter`). Por conta desse papel especial, nós dizemos que...

> Toda sequência de comandos (simples ou complexos) delimitada pelos operadores `;`, `&`, `&&` ou `||`, antes de uma quebra de linha, é uma **lista de comandos**.

Numa lista de comandos, o separador padrão (aquele que não faz nada além de separar os comandos) é o `;`. Os operadores de encadeamento condicional (`&&` e `||`), além de separarem os comandos de uma lista, condicionam a execução do comando seguinte ao estado de saída do comando anterior (como vimos na aula 5). Já o operador de execução assíncrona (`&`), faz com que o comando cujo fim ele delimita seja executado em segundo plano, de forma que o shell não aguarde o seu término para a execução do comando seguinte.

> Nós falaremos mais sobre o operador `&` numa aula específica sobre o gerenciamento de ***jobs*** (aula 13).

### Precedência

Um dos atributos típicos de operadores é a sua precedência, ou seja, encontrados mais de um operador em um mesmo comando ou expressão, quais operações serão avaliadas antes das demais. No caso das listas de comandos, os operadores `&&` e `||` têm a maior precedência e, por isso, serão avaliados internamente pelo shell antes dos operadores `;` e `&`.

## 6.3 - Comandos compostos

Os comandos compostos são estruturas do shell que permitem o agrupamento de comandos de modo eles seja capazes de executar tarefas complexas. Existem quatro categorias de comandos compostos, cada uma delas classificada pela forma de agrupamento dos comandos e pelo seu contexto léxico:

**Agrupamentos de comandos**

* Agrupamento com chaves
* Agrupamento com parêntesis

**Estruturas de repetição (loops)**

* Loop `for`
* Loops `while` e `until`
* Menu `select`

**Estruturas de decisão**

* Bloco `if`
* Bloco `case`

**Estruturas de avaliação de expressões**

* Avaliação de expressões afirmativas: `[[ expressão ]]`
* Avaliação de expressões aritméticas: `((expressão))`

> As estruturas de avaliação de expressões serão assunto da aula 11.

### Palavras reservadas

A maior diferença entre os comandos simples e os comandos compostos é que, em vez de começarem com o nome de um comando interno do shell ou com o nome de um executável, os comandos compostos geralmente começam com **palavras reservadas** (*keywords*).

A menos que estejam citadas (ver as **regras de citação** na aula anterior), essas palavras serão identificadas como reservadas pelo shell:

```
if      then    elif     else    fi        time
for     in      until    while   do        done
case    esac    coproc   select  function  
{       }       [[       ]]      !
```

> **Importante!** Palavras reservadas são palavras e, portanto, exigem que um operador de controle as separem de outras palavras.

## 6.4 - Agrupamentos de comandos

A principal utilidade de um agrupamento está na possibilidade de capturarmos a saída de cada comando em um único fluxo de dados. Por exemplo, o operador de redirecionamento `>` zera o conteúdo do arquivo de destino cada vez que faz uma escrita:

```
:~$ echo banana > frutas.txt; echo laranja > frutas.txt
:~$ cat frutas.txt
laranja
```

Aqui, o arquivo `fruta.txt` foi zerado antes de cada redirecionamento da saída de um comando `echo`, o que fez com que seu conteúdo fosse apenas `laranja` ao final. Observe o que acontece com um agrupamento com parêntesis:

```
:~$ { echo banana; echo laranja; } > frutas.txt
:~$ cat frutas.txt
banana
laranja
```

Com os comandos agrupados, existe apenas um fluxo de dados e ele só se fecha ao término da execução do último comando. O mesmo poderia ser feito com parêntesis:

```
:~$ (echo fusca; echo corcel) > carros.txt
:~$ cat carros.txt
fusca
corcel
```

Mas existem diferenças importantes entre as duas formas de agrupamento:

**Agrupamento com chaves:**

* Os comandos são executados na mesma sessão do shell;
* As chaves são palavras reservadas do shell e, por isso, exigem espaços entre elas e os comandos da lista;
* Também é obrigatório haver algum tipo de operador de controle terminando o último comando -- geralmente `;`, `&`, ou uma quebra de linha.

**Agrupamento com parêntesis:**

* Os comandos são executados em um ***subshell***;
* Os parêntesis são metacaracteres (separam palavras), por isso não precisamos de espaços nem da terminação do último comando;
* Se prefixado com `$`, um agrupamento com parêntesis será tratado como uma expansão, a **substituição de comandos**, e suas saídas poderão ser armazenadas em variáveis.

> O estado de saída dos agrupamentos será o estado de saída do último comando executado.

## 6.5 - Subshells

Um **subshell**, é um processo filho do shell corrente que não passa pelos estágios de reconfiguração pelos quais uma sessão ou um novo processo "normal" do shell passariam. Todas as variáveis em uso no processo pai são copiadas para o *subshell*, inclusive as variáveis de ambiente, a variável especial `$` (PID do shell corrente) e a variável `SHLVL` (*shell level*), que armazena o nível da sessão corrente.

Isso pode causar algumas confusões:

```
:~$ echo $$
30785
:~$ (echo $$)
30785
```

Os comandos agrupados **com parêntesis**, diferente dos comandos agrupados com chaves, sempre são executados em um *subshell*. Por isso, o PID expandido com o `$$` foi o mesmo do processo do shell que estava em execução no terminal.

Para diferenciar o PID do processo pai do PID do subshell, o Bash possui uma variável que não pode ser copiada para o ambiente do um subshell –- a variável `BASHPID`.

Então, para descobrir o PID de um subshell:

```
:~$ echo $$ $BASHPID
30785 30785
:~$ (echo $$ $BASHPID)
30785 31763
```

### Escopo de variáveis em um subshell

Um *subshell* herda uma cópia de todas as variáveis em uso na sessão mãe. Mas, como são cópias, as alterações feitas no *subshell* não são refletidas nas variáveis da sessão original.

Observe:

```
:~$ fruta=banana; animal=gato
:~$ echo $fruta $animal
banana gato
:~$ (animal=zebra; echo $fruta $animal)
banana zebra
:~$ echo $fruta $animal
banana gato
```

Dentro do agrupamento com parêntesis, onde os comandos são executados em um subshell, nós alteramos o valor da variável `animal` para `zebra`, mas a alteração só teve efeito na cópia da variável existente no *subshell* -- a original continuou inalterada.

## 6.6 - Estruturas de decisão

As estruturas de decisão são aquelas em que blocos de comandos são executados condicionalmente. No Bash, nós podemos dizer que existem dois tipos de condições: o estado de saída de um comando e o casamento de uma string com um dado padrão.

Quando um **estado de saída** é a condição, nós podemos utilizar os operadores de encadeamento condicional (`&&` e `||`) ou o **comando composto** `if`. Se a condição vier do casamento de um padrão, nós ainda podemos utilizar os operadores `&&` e `||` ou o comando composto `if` testando a saída de qualquer comando que avalie uma comparação de strings (com os comandos `test`, `[` e `[[`, por exemplo), mas a forma mais direta e comum de se fazer isso é com o **comando composto** `case`.

### Grupos e operadores de encadeamento condicional

Os operadores de encadeamento condicional não definem comandos compostos, mas, com alguns cuidados, eles podem trabalhar com blocos de comandos agrupados com chaves ou parêntesis:

```
COMANDO && {
	COMANDOS SE SUCESSO (0)
	true
} || {
	COMANDOS SE ERRO (!=0)    
}
```

Ou então...

```
COMANDO || {
	COMANDOS SE ERRO (!=0)
	false
} && {
	COMANDOS SE SUCESSO (0)    
}
```

Repare que nós terminamos os primeiros blocos com um comando nulo `true` ou `false`, dependendo dos operadores utilizados no bloco seguinte. Isso é exigido para garantir que o último comando de um agrupamento não saia com o estado necessário para permitir a execução do agrupamento seguinte.

> Apesar de ser uma solução engenhosa (para não dizer "gambiarra"), o melhor mesmo é optar por estruturas que possuam um valor semântico e evitar comandos que não expressem o que desejamos fazer.

Por outro lado, se essas estruturas forem utilizadas com agrupamentos que terminem com comandos com estados de saída previsíveis, como o comando `echo` ou as atribuições de valores a variáveis, que sempre terminam com sucesso, ou ainda, se os agrupamentos terminarem com os comandos `exit` ou `return`, é seguro e semântico utilizar os operadores de encadeamento condicional.

## 6.7 - O comando composto 'if'

Embora não tenha a flexibilidade dos operadores de controle condicional, o comando composto `if` oferece uma opção bem mais segura quanto ao que será ou não executado.

Nós chamamos a estrutura condicional de "comando `if`", mas ela é composta de vários elementos iniciados com as palavras reservadas `elif`, `then` e `else`. Além disso, o comando composto `if` precisa ser terminado com a palavra reservada `fi` (*if* ao contrário).

### Elementos da estrutura condicional 'if'

Abaixo, nós veremos cada elemento do comando composto `if` separadamente. Isso é importante porque não existe uma sintaxe fixa e pré-determinada para esse tipo de estrutura.

```
if COMANDO-TESTADO;
```

A palavra reservada `if` indica o início da estrutura condicionada ao estado de saída de `COMANDO-TESTADO`. A única estrutura obrigatória após o `if` é um bloco de comandos iniciado pela palavra reservada `then`.

```
then LISTA-DE-COMANDOS;
```

A palavra reservada `then` indica o início de um bloco de comandos que será executado caso o comando testado por um `if` ou um `elif` (abaixo) saia com estado de **sucesso**. Todo bloco `then` precisa ser obrigatoriamente seguido de um bloco `elif`, ou de um bloco `else`, ou do terminador `fi`.

```
elif OUTRO-COMANDO-TESTADO;
```

Caso o comando testado pelo `if` saia com erro, nós temos a opção de testar outros comandos na mesma estrutura condicional com a palavra reservada `elif` (*else if*). Todo bloco `elif` precisa obrigatoriamente ser antecedido e seguido por um bloco `then`.

```
else LISTA-DE-COMANDOS;
```

A palavra reservada `else` inicia um bloco opcional de comandos que serão executados caso os comandos testados com o `if` ou com o `elif` saiam com erro. O bloco `else` precisa ser obrigatoriamente antecedido por um bloco `then` e seguido pelo terminador `fi`.

```
fi
```

A palavra reservada `fi` delimita o fim do comando composto iniciado pelo if.

Os exemplos abaixo mostram algumas combinações possíveis dos elementos do comando composto `if`:

```
# Não faz nada no caso do comando testado sair com erro...
if COMANDO-TESTADO; then
	COMANDOS SE SUCESSO
fi

# Oferece uma alternativa se o comando testado sair com erro...
if COMANDO-TESTADO; then
	COMANDOS SE SUCESSO
else
	COMANDOS SE ERRO
fi

# Testa outro comando se o comando testado sair com erro...
if COMANDO-TESTADO; then
	COMANDOS SE SUCESSO
elif OUTRO-COMANDO-TESTADO; then
	OUTROS COMANDOS SE SUCESSO
else
	COMANDOS SE ERRO
fi
```

### Testando expressões

O `if` e o `elif` podem testar qualquer comando, mas é muito comum que eles sejam vistos testando a saída dos comandos `((`, `[[` ou `[`, o que sempre confunde quem está começando no shell, especialmente aqueles que possuem alguma experiência com outras linguagens de programação e acham que esses comandos fazem parte da estrutura condicional.

Então, não custa nada repetir:

> A estrutura `if` testa a saída de **comandos**, qualquer comando, inclusive dos comandos `((`, `[[` e `[`, que testam expressões.

### Invertendo os testes

Uma outra situação muito comum é precisarmos apenas de um bloco que seja executado caso o comando testado saia com estado de erro. Como o bloco `else` precisa ser antecedido por um bloco then, isso não seria possível:

```
:~$ if false; else echo erro; fi
bash: erro de sintaxe próximo ao token inesperado `else'
```

Nesses casos, a solução é inverter o resultado do teste com uma exclamação (`!`) e utilizar um bloco `then` normalmente:

```
:~$ if ! false; then echo erro; fi
erro
```

> A exclamação também é uma **palavra reservada** e precisa ser separada das outras palavras do comando por um espaço. Sem o espaço antes da palavra seguinte, aliás, haveria uma expansão do histórico!

### Testando a saída de comandos agrupados

Nós também podemos testar o estado de saída de comandos agrupados ou de uma lista:

**Agrupamento com parêntesis...**

```
:~$ if (true; false); then echo sucesso; else echo erro; fi
erro
```

**Agrupamento com chaves...**

```
:~$ if { false; true; } then echo sucesso; else echo erro; fi
sucesso
```

**Uma lista de comandos...**

```
:~$ if false; true; then echo sucesso; else echo erro; fi
sucesso
```

No caso das listas de comandos desagrupados, a negação só tem efeito quando aplicada ao último comando:

```
:~$ if ! true; false; then echo sucesso; else echo erro; fi
erro
:~$ if true; ! false; then echo sucesso; else echo erro; fi
sucesso
```

Já nos agrupamentos, a negação é aplicada ao estado de saída do grupo, que é o estado de saída do último comando:

```
:~$ if ! (true; false); then echo sucesso; else echo erro; fi
sucesso
:~$ if ! { false; true; } then echo sucesso; else echo erro; fi
erro
```

### Por que evitar os blocos `elif`

Quando o teste de `if` resulta em erro e não existe um bloco `else`, nada é executado e a estrutura condicional é terminada. Se outro comando precisar ser testado, isso já caracteriza outro contexto semântico e, portanto, merece ter a sua própria estrutura condicional.

Eventualmente, se o comando testado for uma expressão que avalia, por exemplo, uma comparação de strings, pode ser muito mais interessante recorrer à nossa próxima estrutura condicional.

## 6.8 - O comando composto 'case'

Imagine a situação abaixo:

```
if [[ ${str,,} == banana ]]; then
	echo Não gosto de $str.
elif [[ ${str,,} == laranja ]]; then
	echo Esta $str é muito azeda!
elif [[ ${str,,} == abacate ]]; then
	echo ${str^} é a minha fruta favorita!
else
	echo Não encontrei $str no mercado.
fi
```

Veja que estamos comparando a string em `str` com três outras strings. Aliás, sendo mais exato, esta nem é uma comparação de strings! No comando `[[`, o operador `==` compara uma string com um padrão. Apenas por acaso os padrões são strings literais, mas é isso que o operador faz.

Seria muito mais racional e legível utilizar uma estrutura `case`:

```
case ${str,,} in
    laranja)
        echo Esta $str é muito azeda!
        ;;
    banana)
        echo Não gosto de $str.
        ;;
    abacate)
        echo ${str^} é a minha fruta favorita!
        ;;
    *)
        echo Não encontrei $str no mercado.
        ;;
esac
```

O comando composto `case` compara uma string com vários padrões e executa o bloco de comandos relativo ao padrão onde uma correspondência tiver sido encontrada.

> Os padrões de uma estrutura `case` seguem as mesmas regras da formação de nomes de arquivos e podem ser, inclusive, 'globs estendidos', se a opção `extglob` estiver habilitada, mas isso fica para a aula sobre 'expansões do shell'.

Se precisarmos condicionar um mesmo bloco ao casamento com vários padrões diferentes, nós podemos separá-los com uma barra vertical (`|`), que terá o significado de "ou":

```
case $opt in
    a|A)
        echo Boa escolha!
        ;;
    b|B)
        echo Podia ser melhor...
        ;;
    c|C)
        echo Argh!
        ;;
    *)
        echo Melhor não escolher nada mesmo...
        ;;
esac
```

> O exemplo serve para demonstrar o uso do separador de padrões para permitir o casamentos com os caracteres indicados em caixa alta ou em caixa baixa. Porém, na prática, é muito melhor fazer como nos primeiros exemplos, onde utilizamos uma expansão para tornar minúscula a string na variável de referência.

Observe também que o último padrão (`*`) casa com qualquer valor na variável de referência. Ele está ali para permitir a execução de um bloco de comandos no caso de não haver casamento com os padrões anteriores, mas isso não é obrigatório se não houver uma ação padrão a ser executada.

> É importante saber que a comparação será feita na ordem em que os padrões são apresentados, ou seja, se o padrão `*` aparecer antes das outras opções, seu bloco de comandos sempre será executado.

Para indicar o fim de um bloco de comandos, nós utilizamos o operador de controle `;;`, que encerra o `case` após a execução do bloco de comandos do padrão encontrado. Mas, dependendo da situação, nós podemos optar pelos operadores `;&` ou `;;&`:

| Operador | Descrição|
|---|---|
| `;;` | Delimita o fim de uma lista de comandos em uma cláusula do comando composto `case`, causando o término dos testes comparativos dos padrões (equivale à instrução `break` de outras linguagens). |
| `;&` | Delimita o fim de uma lista de comandos em uma cláusula do comando composto `case`, mas indica que a lista de comandos da cláusula seguinte também deve ser executada incondicionalmente. |
| `;;&` | Delimita o fim de uma lista de comandos em uma cláusula do comando composto `case`, mas indica que os padrões de todas as cláusulas subsequentes também devem ser testados (faz o case do shell se comportar de forma equivale ao padrão de uma estrutura *switch-case* de outras linguagens). |

> O bloco condicionado ao último padrão não precisa ser terminado com um operador de controle, mas é uma boa prática utilizar o `;;` mesmo assim.

Finalmente, como você já deve ter notado, toda estrutura `case` precisa ser  terminada com a palavra reservada `esac` (que é *case* ao contrário).

## 6.9 - Estruturas de repetição

As estruturas de repetição permitem a execução repetida de um bloco de comandos condicionada aos elementos existentes numa lista de palavras, ao estado de saída de comandos, ou à avaliação de uma expressão aritmética ternária.

## 6.10 - O loop 'for'

O comando composto `for` é uma estrutura de repetição geralmente preferida para as iterações com quantidades definidas de ciclos, seja em uma lista de palavras ou na avaliação de uma expressão aritmética ternária.

O uso mais comum do `for` obedece à seguinte sintaxe:

```
for VAR in PALAVRAS; do
	BLOCO DE COMANDOS
done
```

Em uma linha...

```
for VAR in PALAVRAS; do BLOCO DE COMANDOS; done
```

Cada palavra em `PALAVRAS` é atribuída temporariamente à variável `VAR` e causa um ciclo de execução do `BLOCO DE COMANDOS`, que é iniciado pela palavra reservada `do` e terminado pela palavra reservada `done`.

Por exemplo:

```
:~$ for bicho in burro cão gato galo; do echo $bicho; done
burro
cão
gato
galo
```

A lista de palavras também pode ser o resultado de uma expansão do shell:

```
# Expandindo as palavras em uma string...

:~$ palavras='café pão leite'
:~$ for p in $palavras; do echo $p; done
café
pão
leite

# Expandindo as palavras em um vetor...

:~$ frutas=(pera uva 'banana da terra')
:~$ for f in "${frutas[@]}"; do echo $f; done
pera
uva
banana da terra

# Percorrendo as strings geradas por uma expansão de chaves...

for n in {1..5}; do echo $n; done
1
2
3
4
5
```

Quando a lista de palavras corresponde a todos os parâmetros posicionais (aula 7) passados para o script, nós podemos omitir a parte do `in PALAVRAS`. No exemplo, nós utilizaremos o comando `set --` para definir os parâmetros posicionais da sessão corrente do shell:

```
:~$ set -- João Maria 'Luis Carlos'
:~$ for nome; do echo $nome; done
João
Maria
Luis Carlos
```

Isso é equivalente a `for VAR in "$@"`...

Em vez de uma lista de palavras, o `for` também pode ser controlado por uma expressão aritmética ternária, o que é conhecido como “estilo C”. Neste caso, a sintaxe da estrutura muda para:

```
for (( EXP1; EXP2; EXP3 )); do
	BLOCO DE COMANDOS
done
Ou, opcionalmente...
for (( EXP1; EXP2; EXP3 )) {
	BLOCO DE COMANDOS
}
```

Para entender com essa estrutura funciona, vamos pensar em etapas:

**Primeira etapa:**

A expressão `EXP1`, que geralmente é a atribuição de um valor inicial a uma variável de referência, é avaliada (esta avaliação ocorre apenas uma vez, antes do primeiro ciclo).

**Segunda etapa:**

A expressão `EXP2` é avaliada quanto à verdade da sua afirmação. Geralmente, ela é uma comparação entre o valor na variável de referência e um valor inteiro utilizado como limite. Se `EXP2` for avaliada como verdadeira (qualquer valor diferente de `0`), haverá um ciclo de execução do `BLOCO DE COMANDOS`. Caso seja avaliada como falsa (valor 0), nenhum ciclo é executado e o `for` é encerrado.

**Terceira etapa:**

Após executar um ciclo do `BLOCO DE COMANDOS`, a expressão em `EXP3`, que costuma ser alguma alteração no valor da variável de referência, será avaliada.

**Quarta etapa:**

Completadas as etapas anteriores, `EXP2` é novamente avaliada para confirmar se a sua afirmação ainda é verdadeira. Se for, acontece um novo ciclo e uma nova avaliação de `EXP3`. Caso contrário, nenhum novo ciclo é executado e o `for` é encerrado.

Exemplos:

```
# Delimitando o bloco de comandos com ‘do...; done’:

:~$ for (( n=0 ; n < 5; n++)) do echo $n; done
0
1
2
3
4

# Delimitando o bloco de comandos com chaves:

:~$ for (( n=0 ; n < 5; n++)) { echo $n; }
0
1
2
3
4
```

> **Perceba:** a decisão de utilizar a estrutura típica do shell ou o estilo C não tem nada a ver com melhor ou pior, mais difícil ou mais fácil, ou qualquer outro aspecto qualitativo. Ambas as formas são corretas e necessárias para resolver diferentes tipos de problemas. Já o uso das chaves, que é totalmente opcional no estilo C, deve ser **evitado** para não confundir com outros agrupamentos de comandos.

### Loop 'for' infinito

Nós podemos fazer um loop infinito com o `for`:

```
for (( ;1; )); do BLOCO DE COMANDOS; done
```

Como vimos, é a expressão `EXP2` que determina a continuação dos ciclos: se ela receber qualquer valor diferente de `0`, os ciclos continuam. Então, uma expressão literal diferente de zero (como o valor inteiro `1` do exemplo) garantirá a continuidade infinita dos ciclos.

> As expressões aritméticas sempre saem com erro se a avaliação resultar no valor `0` e com estado de sucesso se qualquer outro valor for avaliado.

O mesmo resultado pode ser conseguido omitindo `EXP2`:

```
for (( ; ; )); do BLOCO DE COMANDOS; done
```

Porém, todo loop infinito precisa de um comando que encerre os ciclos em algum momento. Há vários meios de se fazer isso, mas todas elas envolvem alguma forma de execução condicional dos comandos `break` ou `exit` (se for o caso de terminar um script), como veremos mais adiante.

## 6.11 - Os loops 'while' e 'until'

Os loops `while` e `until` condicionam a execução de um bloco de comandos ao estado de saída de um comando testado. A diferença entre eles é que o `while` permite a execução dos ciclos quando o comando testado sai com sucesso, ao passo que o `until` só permite a execução dos ciclos se o estado de saída for erro:

```
while COMANDOS-TESTADOS; do
	BLOCO DE COMANDOS
done

until COMANDOS-TESTADOS; do
	BLOCO DE COMANDOS
done
```

Em uma linha...

```
while COMANDOS-TESTADOS; do BLOCO DE COMANDOS; done
until COMANDOS-TESTADOS; do BLOCO DE COMANDOS; done
```

Aqui, `COMANDOS-TESTADOS` podem ser comandos simples, listas de comandos ou agrupamentos, exatamente como vimos no tópico sobre o comando composto `if`. Mas, de longe, os comandos mais utilizados são os testes de expressões, com `[` ou `[[`, e os comandos nulos (`true`, `false` ou `:`).

Aqui estão alguns exemplos:

```
# Utilizando o 'while' com uma expressão aritmética...

:~$ a=0; while ((a < 5)); do echo $((++a)); done
1
2
3
4
5

# O mesmo efeito com 'until'...

:~$ a=0; until ((a == 5)); do echo $((++a)); done
1
2
3
4
5
```

Como podemos observar, o `while` continuou **enquanto** o comando `((` saiu com sucesso. O loop `until`, por sua vez, só **parou quando** o comando `((` saiu sucesso. Ou seja...

> O estado de saída de **sucesso** determina a continuidade do `while` e a parada do `until`.

### Não temos um loop 'do-while'

Se você vem de alguma linguagem com sintaxes derivadas do C, é provável que esteja se perguntando sobre o loop ***do-while***, mas só se ainda não notou um detalhe importante sobre as duas estruturas existentes no Bash: no `while`, nós especificamos uma condição de **continuidade**: *faça enquanto sucesso*. Já no `until`, nós especificamos uma condição de **parada**: *faça até que sucesso*.

Portanto, semanticamente, nós elaboramos a lógica do loop `while` de modo a que uma condição **impeça a execução de um próximo ciclo**, mas a lógica do loop `until` é elaborada para executar **pelo menos um ciclo** antes de encontrar a condição de parada – exatamente como em um loop *do-while*.

### Loops infinitos com 'while' e 'until'

Não é muito comum criarmos loops infinitos com a estrutura `for`. Até semanticamente, o loop `while` é muito mais apropriado para esta finalidade:

```
while :; do
	BLOCO DE COMANDOS
done
```

> O comando nulo `:` poderia ser trocado pelo comando `true`.


Também podemos utilizar o loop `until`:

```
until false; do
	BLOCO DE COMANDOS
done
```

### Saindo de loops com 'break'

O comando interno `break` provoca a saída imediata de qualquer uma das estruturas de repetição do Bash.

Por exemplo:

```
:~$ a=0; while :; do echo $((++a)); [[ $a -eq 5 ]] && break; done
1
2
3
4
5
```

No caso de loops aninhados, o `break` pode receber um número inteiro como argumento informando qual nível de loop deve ser interrompido. O inteiro `1` corresponde ao loop de nível mais interno (o próprio loop onde o `break` é executado), e os inteiros maiores que 1 são os níveis mais externos.

### Pulando o restante da iteração com ‘continue’

Também utilizado em qualquer uma das estruturas de repetição do Bash, o comando interno `continue` serve para que o restante da iteração por um bloco de comandos seja interrompida e o próximo ciclo seja executado.

Por exemplo:

```
:~$ for c in {A..E}; do [[ $c == C ]] && continue; echo $c; done
A
B
D
E
```

Repare que o comando echo não foi executado quando o valor na variável `c` era o caractere C.

Tal como o `break`, o comando `continue` também pode receber um número inteiro como argumento informando qual nível de iteração deve ser saltado. O inteiro `1` corresponde ao loop de nível mais interno (o próprio loop onde o `continue` é executado), e os inteiros maiores que 1 são os níveis mais externos.

## 6.12 – O menu 'select'

O comando composto `select` é uma estrutura especializada na criação de menus rápidos a partir de listas de palavras. Sua sintaxe é idêntica à do loop `for`:

```
select VAR [in PALAVRAS]; do BLOCO DE COMANDOS; done
```

Neste aspecto, aliás, tudo que se aplica ao loop `for` é aplicável ao menu `select`, o que muda é o funcionamento da estrutura como um todo.

Para começar, o `select` se comporta como um loop infinito que espera a entrada de uma opção numérica do usuário para continuar. A opção digitada é armazenada em duas variáveis: a string correspondente à palavra escolhida vai para a variável `VAR`, e o número da opção escolhida vai para a variável interna `REPLY`.

Deste modo, tudo que temos que fazer é escrever a lógica para tratar as opções digitadas pelo utilizador. Veja no script de exemplo, abaixo:

```
#!/usr/bin/env bash

titulo='CARDÁPIO DE PIZZAS
------------------'

# Usar uma array é uma boa ideia...
menu=(Calabresa Muçarela Vegana Sair)

# Limpa o terminal...
clear

# Exibe o título...
echo "$titulo"

# Monta menu com os elementos da array...
select opt in ${menu[@]}; do

    # Sai do loop se escolher a opção igual ao
    # número de elementos da array 'menu'...
    [[ $REPLY == ${#menu[@]} ]] && break

    # Qualquer opção maior do que o número de
    # elementos da array é inválida...
    [[ $REPLY -gt ${#menu[@]} ]] && {
        # Informa que a opção é inválida...
        echo 'Opção inválida!'
    } || {
        # Exibe o número e a string da escolha...
        echo "Você escolheu $REPLY: ${opt,,}"
    }

    # Aguarda a digitação de um caractere para seguir...
    read -n1 -p 'Tecle algo para continuar...' 

    # Limpa o terminal e exibe o título novamente...
    clear
    echo "$titulo"
done

# Sai do script...
exit 0
```

O script só ficou longo por conta dos comentários, mas ele exemplifica bem o uso típico do menu `select`.

Quando executado, este será o resultado:

```
CARDÁPIO DE PIZZAS
------------------
1) Calabresa
2) Muçarela
3) Vegana
4) Sair
#? _
```

Como você pode ver, o próprio `select` cuidou de numerar as opções, e foi por isso que nós tomamos o cuidado de criar a lista de palavras em um vetor onde o último elemento era a opção `Sair`. Assim, o número associado a ele pelo `select` corresponde ao número total de elementos do vetor, o que nos permite testar facilmente se o utilizador escolheu sair do menu:

```
# Sai do loop se escolher a opção igual ao
# número de elementos da array 'menu'...

[[ $REPLY == ${#menu[@]} ]] && break
```

### O prompt PS3

Os caracteres `#?`, exibidos no prompt do menu `select`, são definidos em uma das 5 variáveis do shell que determinam como os prompts serão exibidos (sim, existem outros prompts):

* A variável `PS0` contém a string que é exibida antes de um comando ser executado no shell interativo.
* A variável `PS1` contém a string do prompt da linha de comandos.
* A variável `PS2` contém o caractere `>`, que indica a continuação da entrada de comandos.
* A variável `PS3` contém o prompt do menu `select`, que é `#?`, por padrão.
* A variável `PS4` contém o caractere `+`, que aparece quando estamos "debugando" a execução do shell (comando `set -x`).

Então, se quisermos um prompt mais amigável no nosso script, basta definir a string do prompt `PS3` em qualquer ponto antes da estrutura do menu:

```
# Define o prompt PS3...
PS3='Escolha a sua opção: '

# Monta menu com os elementos da array...
select opt in ${menu[@]}; do
E o resultado será...
CARDÁPIO DE PIZZAS
------------------
1) Calabresa
2) Muçarela
3) Vegana
4) Sair
Escolha a sua opção: _
```

## 6.13 - Funções

Se estivéssemos falando de uma linguagem de programação, uma função seria basicamente um bloco de instruções agrupadas e identificadas por um nome pelo qual pudesse ser chamado de outras partes no código. Aqui, a ideia básica é a mesma: ainda é um bloco de comandos nomeado, só que, no shell, este bloco de comandos é qualquer um dos comandos compostos que nós vimos nos tópicos anteriores!

Logo, no shell, uma função é...

> Qualquer ‘comando composto’ que recebe um nome pelo qual possa ser chamado de qualquer parte do código.

Nós podemos criar funções com a sintaxe:

```
NOME() COMANDO-COMPOSTO
```

Alternativamente, pode ser utilizada a palavra reservada `function`. Neste caso, os parêntesis são opcionais:

```
function NOME COMANDO-COMPOSTO
```

As funções devem ser definidas no script em um ponto anterior ao ponto onde são chamadas, mas elas só são executadas quando chamadas, logo, elas podem chamar outras funções definidas antes ou depois de suas próprias definições.

### Criando funções com outros comandos compostos

É possível transformar qualquer comando composto em uma função, mas o agrupamento com chaves é a opção mais comum:

```
diga_oi() { 	echo oi; }
```

Aliás, muitos dos conceitos sobre funções que aprendemos por aí são apenas consequências do uso do agrupamento com chaves no corpo da função, como o escopo das variáveis (aula 7) e o fato da lista de comandos ser executada na mesma sessão do shell. Mas, uma função criada com um agrupamento com parêntesis, por exemplo, também é perfeitamente válida, e nós teríamos que lidar com regras totalmente diferentes.

Aqui estão alguns exemplos de uso de outros comandos compostos como funções – alguns são mais úteis do que outros, mas todos são muito interessantes.

### Funções com agrupamentos em parêntesis

Observe...

```
:~$ minha_func() (x=banana; echo $x)
:~$ minha_func
banana
:~$ echo $x

:~$
```

Repare que a função adota o comportamento do comando composto utilizado, e o agrupamento com parêntesis tem a característica de executar a lista de comandos em um subshell. Logo, as variáveis em seu interior estarão sempre limitadas ao escopo do subshell e não poderão ser acessadas pela sessão mãe.

Contudo, ainda podemos passar valores da sessão corrente do shell para os parêntesis: 

```
:~$ minha_func() (echo $x)
:~$ x=elefante
:~$ minha_func
elefante
```

### Testes nomeados

Os testes serão abordados na aula 11, sobre expressões, mas o importante agora é só ter uma ideia do que pode ser feito com funções. Aqui, estamos utilizando o comando composto `[[` para testar uma expressão afirmativa.

```
:~$ teste() [[ ${1,,} == a* ]]
:~$ teste abacate; echo $?
0
:~$ teste melão; echo $?
1
```

No exemplo, nós estamos testando se a string passada como primeiro argumento na chamada da função (`$1`) começa com o caractere `a`, o que é feito utilizando o operador de comparação de strings com padrões (`==`).

> Nós falaremos com mais detalhes sobre isso na aula 7, mas todo argumento passado na chamada de uma função é recebida por ela como um **parâmetro posicional**, no caso `$1`.

### Expressões aritméticas nomeadas

No shell, expressões lógicas e aritméticas podem ser avaliadas com o comando composto `((`, que também será assunto da aula 11:

```
:~$ igual() (($1 == $2))
:~$ igual 3 4 && echo igual || echo desigual
desigual
:~$ igual 4 4 && echo igual || echo desigual
igual
```

Aqui, estamos passando dois argumentos para a função, onde eles serão capturados nos parâmetros posicionais (`$1` e `$2`) e comparados com o operador de igualdade `==` (sim, o significado do operador muda de acordo com o comando). Se forem iguais, o comando composto `((` sairá com sucesso, e este será o estado de saída da função. Se forem diferentes, ela saíra com erro.

### Um loop 'for' nomeado

```
:~$ lista() for n; do echo $n; done
:~$ lista {1..5}
1
2
3
4
5
:~$ lista *txt
alunos.txt
compras.txt
notas.txt
```

Se este não é o exemplo mais útil de uma função com outros comandos compostos, no mínimo, é o mais divertido! Repare que nós omitimos a parte do `in LISTA` da sintaxe do `for` para que ele capturasse todos os argumentos passados para a função.

### Um menu 'select' nomeado

Já pensou em ter uma função geradora de menus no seu script?

```
#!/usr/bin/env bash

menu() select opt in "$@" Sair; do
 [[ $REPLY -eq $(($# + 1)) ]] && break
 [[ $REPLY -gt $(($# + 1)) ]] && {
 echo 'Opção inválida!'
 } || {
 echo $REPLY: $opt
 break
 }
 read -n1 -p 'Tecle algo para continuar...'
 clear
done
```

Para chamar a função no seu código, você faria assim, por exemplo:

```
PS3='Tecle a sua opção: '
menu banana laranja abacate
```

Quando executado, este seria o resultado:

```
1) banana
2) laranja
3) abacate
4) Sair
Tecle a sua opção:
```

Como você pode ver, são muitas as possibilidades! Estes foram apenas alguns exemplos do poder que temos à disposição com as funções no Bash. Mas está na hora de voltarmos à boa e velha função com comandos agrupados com chaves, porque esta é a forma mais comum e mais flexível de se construir uma função.

### Sobre os nomes das funções

As regras para o nome da função são as mesmas que utilizamos para outras linguagens, considerando as boas práticas de:

* Utilizar apenas caracteres minúsculos e o sublinhado.
* Separar nomes compostos com o sublinhado.
* Utilizar o sublinhado como primeiro caractere do nome quando houver risco de confusão entre a chamada de uma função e um comando do shell.

Boas práticas são recomendações para a escrita de códigos legíveis sem o sacrifício do uso dos recursos que a linguagem nos oferece, mas elas não são absolutas e nem mandatórias.

Exemplos:

```
# Separando nomes compostos com sublinhado...
minha_func() { echo Olá, mundo!; }

# Iniciando o nome com o sublinhado...
_exit() { exit $1; }
```

Além disso, para facilitar a leitura do código, é conveniente manter um padrão ao longo de todo o script: se uma função tiver seu nome iniciado por um caractere sublinhado, todos os outros nomes também deverão ser iniciados com um sublinhado.

### Passagem de argumentos para funções

O shell substitui temporariamente o conteúdo dos parâmetros posicionais da sessão pelos valores dos argumentos passados na chamada de uma função, de modo que, no interior da função, nós podemos utilizar qualquer uma de suas expansões (próxima aula). Quando a execução da função termina, os parâmetros posicionais da sessão são restaurados.

Acompanhe este exemplo com atenção e analise o que acontece:

```
:~$ set -- banana laranja
:~$ echo $1 $2
banana laranja
:~$ soma() { echo $1 + $2 = $(($1 + $2)); }
:~$ soma 10 15
10 + 15 = 25
:~$ echo $1 $2
banana laranja
```

Reparou que `$1` e `$2` continuaram expandindo seus valores originais na sessão do shell, mas assumiram outros valores durante a execução da função?

Esse é o ponto mais importante, porque, como todas variáveis têm escopo global numa mesma sessão do shell, o esperado seria que os parâmetros posicionais tivessem o mesmo valor da sessão mãe, mas este é um comportamento exclusivo de funções. Se fosse um simples agrupamento de comandos entre chaves, por exemplo, isso não aconteceria:

```
:~$ set -- banana laranja
:~$ { echo $1 + $2; }
banana + laranja
```

Os parâmetros especiais `*`, `@` e `#` também passam a fazer referência aos parâmetros posicionais passados para a função, mas o parâmetro especial `0` continua contendo o nome do shell ou do script.

### Escopo de variáveis em funções

Fora o caso especial dos parâmetros posicionais, o escopo das variáveis dependerá da estrutura utilizada na formação da função. Ou seja, se o comando composto não abrir um subshell (como um agrupamento com parêntesis, por exemplo), nós podemos generalizar e dizer que a função tem acesso a todos os parâmetros da sessão do shell e a sessão enxerga todas as variáveis na função -– mas, isso pode ser mudado!

Eventualmente, pode não ser interessante criar situações em que uma variável na função possa ser acessada fora dela. Nesses casos, nós podemos contar com o comando interno `local`, que torna restrito ao contexto da função os escopos das variáveis que forem definidas com ele.

Observe e analise:

```
:~$ a=10; b=20
:~$ soma() { echo $((a + b)); }
:~$ soma
30
:~$ soma() { local a=15 b=35; echo $((a + b)); }
:~$ soma
50
:~$ echo $a $b
10 20
```

O comando interno `local`, que só funciona no corpo de funções, recebe nomes de variáveis e as suas respectivas atribuições como argumentos.

### Destruindo funções

A definição de uma função pode ser desfeita com a opção `-f` do comando interno `unset`:

```
:~$ soma() { echo $1 + $2 = $(( $1 + $2 )); }
:~$ soma 15 30
15 + 30 = 45
:~$ unset -f soma
:~$ soma 10 20
bash: soma: comando não encontrado
```
### Retorno de funções no shell

Um dos pontos mais polêmicos sobre o como as funções funcionam no shell, é o fato de que elas não possuem um conceito de **retorno** tal como nas outras linguagens de programação. A parte de *"devolver o controle da execução para o fluxo principal do programa"*, que consta desses outros conceitos, ainda está lá, mas as dificuldades começam quando tentamos descobrir formas de obter valores através do retorno de funções.

Na verdade, a solução é simples: funções do shell não retornam valores!
 
Todos os dados que as funções processam, ou estão em variáveis globais, ou estão na saída padrão, ou em algum descritor de arquivos se as saídas dos comandos no corpo da função forem redirecionadas (aula 9), o que sempre nos levará, ou a uma expansão, ou ao acesso posterior desses dados.

Por exemplo:

```
:~$ quadrado() { echo $(($1**2)); }
:~$ echo O quadrado de 8 é $(quadrado 8)
O quadrado de 8 é 64
```

Para aumentar a confusão, existe ainda o comando interno `return`, que é quase um `exit`, só que para funções. Quando utilizado, seu papel é interromper a função e devolver o controle de execução para o comando seguinte no script. Opcionalmente, nós podemos passar um valor inteiro entre 0 e 255 como argumento para especificar o estado de saída da função. Se não for passado nenhum valor, a função sairá com o mesmo estado de saída do último comando executado.

### Obtendo o nome da função

Quando o shell "empresta" os parâmetros posicionais para uma função, o único que não muda é o parâmetro especial `0`, que continua contendo o nome do executável do shell ou o nome do script. Mas existe uma variável interna que armazena o nome de todas as funções em execução –- o vetor `FUNCNAME`.

Sempre que uma função é executada, seu nome é armazenado no elemento de índice `0` de `FUNCNAME`. Os outros elementos contém os nomes das demais funções em execução em ordem inversa de chamada. Sendo assim, o maior índice de FUNCNAME em um script sempre terá o nome da função `main`, o que podemos comprovar com o pequeno script abaixo:

```
#!/usr/bin/env bash

teste() {  echo ${FUNCNAME[@]}; }

teste
```

Quando executado...

```
:~$ ./funcname.sh
teste main
```

### Variáveis, aliases e funções

Na customização da linha de comandos, é muito comum nós criarmos apelidos para abreviar a digitação ou padronizar as opções de um comando. Isso pode ser feito tanto com o comando `alias` quanto com variáveis. Na prática, além do fato dos apelidos serem processados antes das expansões de variáveis, a única diferença de uso é que um apelido pode se executado sem um `$` na frente.

Repare:

```
# Criando o alias ‘hh’
:~$ alias hh='help -d'
:~$ hh test
test - Evaluate conditional expression.

# Destruindo o alias ‘hh’...
:~$ unalias hh

# Criando a variável ‘hh’...
:~$ hh='help -d'
:~$ $hh test
test - Evaluate conditional expression.

# Destruindo a variável ‘hh’...
:~$ unset hh
```

Como você pode ver, tanto o apelido quanto a variável expandem para a string do comando, mas ambos os métodos carecem de uma coisa que pode ser bem mais útil numa linha de comandos: a capacidade de receber argumentos, e isso só é possível com scripts ou funções.

Como não vale a pena passar por todo o processo de criação de scripts e gerenciamento de arquivos para fazer essas pequenas customizações, nós acabamos recorrendo às funções.

Então, se eu quiser um comando para calcular o quadrado de um inteiro qualquer, basta criar uma função `quadrado` e salvá-la no final do meu arquivo `~/.bashrc`. Assim, ela estará sempre disponível para quando eu precisar.

No arquivo `~/.bashrc`:

```
quadrado() { echo $(($1**2)); }
```

Executando (depois do shell recarregado)...

```
:~$ quadrado 6
36
```

