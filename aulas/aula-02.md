# Aula 2 - O que acontece antes do 'Enter'

- [Vídeo desta aula](https://youtu.be/xSJzJmDVi_U)
- [Playlist completa](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)

## 2.1 - O shell é um programa como outro qualquer

Como vimos, um sistema operacional *unix-like* geralmente é composto por três tipos de programas:

* Um *kernel*
* Um shell
* Um conjunto de utilitários e aplicativos

Isso quer dizer que, apesar do papel especial que desempenha no sistema operacional, o shell não deixa de ser um programa como outro qualquer. Como qualquer programa, ele terá que ser iniciado de alguma forma e, eventualmente, poderá ser terminado. Outra decorrência deste fato, é que o shell pode ser mudado temporária ou permanentemente de acordo com o nosso gosto ou a nossa necessidade. Na verdade, nós podemos escolher até o shell que será utilizado para executar um único comando!

Contudo, fica difícil enxergar o shell como um programa qualquer quando, ao iniciarmos um terminal, por exemplo, ele já está lá, esperando pelos nossos comandos. Portanto, o nosso objetivo nesta aula é descobrir como funcionam os terminais e como eles se relacionam com o shell.

## 2.2 - Terminais e consoles

Se o seu sistema não tem interface gráfica, assim que você faz o seu *login*, um console é iniciado e, com ele, também tem início uma **sessão do shell**, e o mesmo acontece quando abrimos um terminal na nossa interface gráfica. Em nenhuma das duas situações, nós chegamos a digitar o nome do *binário executável* do shell -- até porque precisaríamos de um shell para fazer isso. O fato é que a execução de um shell está na própria razão de existirem os terminais e consoles.

## 2.3 - Teletipos

No tempo dos computadores que ocupavam salas inteiras, os terminais eram um conjunto de equipamentos físicos conectados por cabos aos dispositivos eletrônicos responsáveis por ler o que era digitado pelos usuários e gerar e exibir caracteres (*tipos*) em uma impressora ou em um monitor de vídeo. Esses equipamentos eram chamados de **teletipos** ou, abreviadamente, **TTY** (de *TeleTYpe*, em inglês).

> Também era muito comum chamar os TTY de **terminais burros**, já que eles não processavam nada, eram apenas máquinas para enviar e exibir caracteres.

Veja como era antes da emulação de terminais:

![](https://blauaraujo.com/wp-content/uploads/2021/01/tty-01.png)

> **UART:** *"Universal Asynchronous Receiver and Transmitter"*, um hardware que era utilizado para interfacear transmissões e recepções de dados seriais onde a sincronização era feita por software.

Tudo que o usuário digitava no terminal era transmitido e recebido, via *UART*, por cabos físicos para o computador onde havia um sistema operacional em execução. O *kernel* desse sistema operacional possuía um módulo responsável pelo tratamento dos dados que trafegavam através da UART e que seriam passados diretamente para o processo de algum programa em execução.

Para lidar com alguns problemas que essa abordagem direta poderia causar, além de oferecer mais controle sobre a sessão, foram desenvolvidos vários mecanismos que, em conjunto com os drivers do *kernel*, iriam compor o chamado *subsistema do terminal*.

## 2.4 - Disciplina de linha

O subsistema do terminal envolve tarefas como o gerenciamento de sessão e a disciplina de linha. A disciplina de linha (**LDISC**) , que é o que nos interessa no momento, implementa as regras de comunicação entre o módulo do *kernel* que lida com as entradas e saídas de dados para o terminal e o dispositivo TTY, fornecido pelo *kernel* sob a forma de um **arquivo caractere** (ou **arquivo de dispositivo**) no sistema de arquivos.

Entre outras coisas, a disciplina de linha contém a lógica para o processamento de caracteres especiais relacionados com os sinais de controle e edição de linhas de texto, como o caractere de interrupção (geralmente `Ctrl`+`C`, associado ao sinal SIGINT) e os caracteres para ações simples, mas fundamentais, como deletar caracteres digitados e quebrar linhas, por exemplo.

## 2.5 - Emuladores de terminal

Com o passar do tempo, os computadores ficaram menores e mais compactos. Com isso, os terminais deixaram de ser equipamentos físicos ligados a um computador para tornarem-se programas que fazem parte do próprio *kernel* (chamados de **módulos**). Deste modo, o que temos hoje são **emulações de terminais físicos** feitas pelo *kernel* que, por questões históricas, ainda são chamados de **TTY**.

> A emulação do TTY é tão fiel ao passado histórico, que os terminais de hoje ainda são "burros".

A diferença, como podemos ver na imagem abaixo, é que agora nós temos teclados e monitores de vídeo conversando diretamente com o kernel (através de seus respectivos módulos, evidentemente):

![](https://blauaraujo.com/wp-content/uploads/2021/01/tty-02.png)

### Consoles

Outra analogia com raízes no passado é o termo *"console"*, que era como se chamavam os móveis das estações de trabalho que abrigavam os componentes físicos de um terminal. Hoje, nós utilizamos essa palavra para nos referirmos a cada terminal emulado disponibilizado pelo kernel.

No sistema operacional GNU/Linux, nós temos acesso a um console TTY quando fazemos login no "modo texto" (como se diz, popularmente) ou quando pressionamos as teclas `Ctrl`+`Alt`+`F1` a `F6` (geralmente, a combinação `Ctrl`+`Alt`+`F7` nos leva a uma sessão gráfica).

## 2.6 - Espaço do usuário

Quando falamos da Filosofia UNIX como requisito de projeto, um dos pontos citados foi a separação das interfaces *usuário-sistema* e *programas-kernel*. Na prática, isso delimita dois espaços de execução de instruções na memória: um para o usuário e outro para o kernel:

* **Espaço do kernel**: reservado para a execução de instruções privilegiadas do kernel, seus módulos e *drivers* de dispositivos.

* **Espaço do usuário**: reservado para a execução de instruções não privilegiadas.

De forma resumida, podemos dizer que, no **espaço do kernel**, são executadas instruções com acesso direto ao hardware, enquanto que, no **espaço do usuário**, os programas têm acesso apenas ao *kernel* através da sua API (do inglês, *Application Programming Interface*). Por isso, era de se imaginar que, em algum momento, seria interessante desenvolver uma forma de utilização do terminal a partir do espaço do usuário -- e assim surgiram os **pseudoterminais**.

## 2.7 - Pseudoterminais

Com os pseudoterminais, também chamados de "pseudo TTY" ou **PTY**, a emulação do terminal é feita no espaço do usuário. Nos bastidores, é assim que tudo funciona:

![](https://blauaraujo.com/wp-content/uploads/2021/01/pts-01.png)

Aqui, o programa emulador de terminal (o Xterm ou o Terminal, por exemplo) solicita ao kernel um par de arquivos de dispositivos: os arquivos PTY (*pseudo TTY*) e PTS (*pseudo TTY secundário*). No lado do PTY, nós temos o processo do terminal (*Xterm*, *URXVT*, *Terminal*, etc...); na ponta do PTS, nós temos **um shell**!

Vejamos como as coisas acontecem quando você executa o programa de um emulador de terminal:

1. O programa que emula o terminal é iniciado;
2. Ele desenha uma interface e solicita um PTY ao sistema operacional;
3. O shell é iniciado como um subprocesso do terminal;
4. Os fluxos de entrada (*stdin*) e saída (*stdout*) de dados e o fluxo da saída de erros (*stderr*) são conectados ao PTY secundário (PTS);
5. O processo do terminal monitora os eventos do teclado para enviá-los ao PTY mestre (PTM);
6. A disciplina de linha captura os caracteres e os armazena em um *buffer* até que o usuário tecle `Enter`;
7. Quando o `Enter` acontece, os caracteres são copiados para o PTS, onde o shell está aguardado por um comando;
8. É neste ponto que o shell incia a sua rotina de interpretar uma sequência de caracteres e "decidir" o que fazer;
9. O comando é executado como um *fork* do processo do shell e a sua saída, se houver, é escrita na saída padrão associada ao PTS;
10. O driver TTY copia os caracteres no PTY mestre sem interferência da disciplina de linha;

Na continuação, o shell volta a entrar no estado de prontidão, o terminal redesenha a interface gráfica e o ciclo continuará até que o terminal seja fechado.

## 2.8 - Tudo são arquivos

Aqui está uma frase que será muito repetida neste curso:

> Em sistemas 'unix-like', tudo é um arquivo, inclusive o hardware.

Então, se um programa está manipulando um PTY, isso significa que  ele está executando operações de escrita e leitura em dois arquivos: o pseudoterminal mestre e o pseudoterminal secundário. Em sistemas GNU/Linux, o arquivo utilizado para gerar este par é o `/dev/ptmx` (multiplexador de pseudoterminal).

Quando um processo abre `/dev/ptmx`, ele recebe um descritor de arquivos (*file handler*, "ponteiro para arquivo", "recurso de arquivo", etc...) para o dispositivo mestre (PTM) e um dispositivo secundário (PTS) é criado no diretório `/dev/pts/`.

### Experimente

Abra um terminal na sua interface gráfica e execute a linha de comando abaixo:

```
:~$ tty
```

O utilitário `tty` exibe o nome do arquivo do dispositivo de terminal conectado ao fluxo de entrada de dados em uso (a **entrada padrão** ou *stdin*). Em outras palavras, ele exibe o caminho e o nome do arquivo do pseudoterminal secundário gerado pelo acesso ao arquivo `/dev/ptmx`. Se não houver nenhum outro terminal aberto, você verá o seguinte:

```
:~$ tty
/dev/pts/0
```

Onde `0` é o nome do arquivo do pseudoterminal secundário (PTS) disponibilizado para a instância do programa do seu terminal gráfico.

### Demonstração

Para demonstrar como funciona essa relação entre dispositivos e arquivos, nós podemos fazer um experimento bem interessante!

Se você ainda não fechou o terminal, abra um segundo terminal gráfico e execute `tty` novamente. Aqui, os meus dois terminais exibiram os seguintes caminhos:

**Terminal 1**

```
:~$ tty
/dev/pts/0
```

**Terminal 2**

```
:~$ tty
/dev/pts/1
```

Agora, no **Terminal 1**, eu vou executar o seguinte comando:

**Terminal 1**

```
:~$ echo 'Olá, mundo!' > /dev/pts/1
```

O operador de redirecionamento `>` é um recurso do shell que faz com que a saída de um comando seja escrita em um arquivo. No caso, o arquivo é `/dev/pts/1`, que é o arquivo de dispositivo associado ao segundo terminal aberto.

Aparentemente, nada aconteceu no próprio **Terminal 1**, mas dê uma olhada no outro terminal...

**Terminal 2**

```
:~$ tty
/dev/pts/1
:~$ Olá, mundo!

```

Como dissemos na descrição do utilitário `tty`, o arquivo exibido em `/dev/pts/N` está conectado à **entrada padrão** do pseudoterminal `N`. A entrada padrão é o fluxo de dados pelo qual os bits e bytes de tudo que você digita no seu teclado chegam ao terminal. Logo, escrever no arquivo `/dev/pts/1` é basicamente o mesmo que digitar alguma coisa no prompt do **Terminal 2**, do nosso exemplo.

### Por que nós conseguimos escrever em '/dev/pts/1'?

Você deve estar se perguntando sobre como foi possível escrever em um arquivo que está num ramo da raiz do sistema de arquivos, e você mesmo pode encontrar a resposta com o comando abaixo:

```
:~$ ls -l /dev/pts
total 0
crw--w----  1 blau tty  136, 0 jan  3 18:34 0
crw--w----  1 blau tty  136, 1 jan  3 18:35 1
c---------  1 root root   5, 2 jan  1 15:50 ptmx
```

Repare que os arquivos `0` e `1` pertencem a mim (usuário `blau`). Isso aconteceu porque, como vimos, a execução do terminal cria um pseudoterminal no **espaço do usuário**, e também é por isso que não precisamos informar uma senha administrativa (senha de *root*) toda vez que abrimos um terminal gráfico.

Além disso, na primeira coluna da saída do comando, nós podemos ver a seguinte informação:

```
crw--w----
```

O primeiro caractere, a letra `c`, me diz que o arquivo é do tipo *caractere*, o que será assunto para outras aulas. Depois dele, nós temos os caracteres que informam as permissões do arquivo. Para nós, o que importa no momento são os três primeiros: `rw-`. Eles me dizem que o dono do arquivo pode ler (`r`), escrever (`w`), mas não pode executar (`-`) o seu arquivo.

Para encerrar essa demonstração, observe que existe um arquivo caractere `/dev/pts/ptmx` que pertence ao usuário `root`. Ele é um clone de `/dev/ptmx` e está lá porque o kernel Linux implementa o suporte aos pseudoterminais montando o sistema de arquivos dos dispositivos PTS (*devpts*) em `/dev/pts/`.

## Notas e curiosidades

### 1 - Escrita em '/dev/ttyN'

Também é possível fazer a experiência de escrita no dispositivo do terminal se você estiver utilizando um console tty. O procedimento é quase o mesmo, mas você abre e alterna entre os consoles com os atalhos `Ctrl`+`Alt`+`F[1...6]`.

Listando os arquivos dos respectivos consoles abertos, é possível ver que eles possuem os mesmos atributos dos dispositivos `pts`, por exemplo:

```
:~$ ls -l /dev/tty2
crw-------  1 blau tty  4, 2 jan  7 10:41 /dev/tty2
```

### 2 - Sessões de SSH e de multiplexadores (Tmux)

Os programas que iniciam sessões remotas de um shell (SSH, FTP, etc...) e os multiplexadores de terminais (Tmux, Screen, etc...) também trabalham com pseudoterminais, mesmo quando executados em consoles tty.

```
Debian GNU/Linux 10 acer tty2

acer login: blau
Password:
...
:~$ tty
/dev/tty2
...
:~$ ssh blau@192.168.10.16
blau@192.168.10.16's password: 
...
:~$ tty
/dev/pts/0
```