# Aula 5 - Depois do 'Enter'

- [Vídeo desta aula](https://youtu.be/kPeMKkPB4KQ)
- [Playlist completa](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)

## 5.1 - Etapas de processamento de comandos

O shell pode receber comandos de três entradas:

* Da entrada padrão, que são os comandos digitados pelo usuário no terminal.
* De um arquivo de script.
* Ou da string de um comando passada como argumento de uma invocação a partir de seu nome de executável (`bash -c 'string do comando'`, por exemplo).

A partir daí, o shell inciará um procedimento de análise e interpretação antes de, finalmente, proceder a execução da linha do comando (ou **de cada** linha de comando, se houver mais de uma num script, por exemplo). Esse procedimento pode ser descrito em cinco etapas gerais:

### Etapa 1 - Decomposição da linha do comando

A primeira coisa que o shell faz é percorrer os caracteres da linha para localizar e identificar, de acordo com as **regras de citação**, a ocorrência dos dois componentes léxicos fundamentais de um comando do shell: as **palavras** e os **operadores**. Neste momento, também acontece a **expansão dos apelidos** (*aliases*), se houver algum.

> **Componentes léxicos**, também chamados de ***tokens***, são qualquer sequência de caracteres que possua algum significado no contexto de uma linguagem de programação ou, no nosso caso, do shell.

### Etapa 2 - Comandos simples e compostos

Aqui, o shell identifica e agrupa os componentes encontrados em comandos simples e compostos, que são estruturas capazes de lidar com expressões, com agrupamentos e com o controle do fluxo de execução de blocos de comandos.

### Etapa 3 - Expansões

Neste ponto, o shell já é capaz de realizar diversas **expansões**, quebrando os componentes expandidos em listas de arquivos, comandos e argumentos.


### Etapa 4 - Redirecionamentos

É aqui que o shell executa os redirecionamentos e remove os **operadores de redirecionamento** e seus operandos da lista de argumentos.

### Etapa 5 - Execução

Só então, o shell executa a linha do comando, podendo ou não esperar o seu término para coletar um estado de saída.


## 5.2 - Palavras e operadores (tokens)

Como vimos, a primeira coisa que o shell faz é percorrer os caracteres da linha para localizar e identificar, de acordo com as **regras de citação**, a ocorrência de **palavras** e **operadores**. Neste tópico, nos vamos entender um pouco melhor os conceitos relacionados com o que acontece na primeira etapa de processamento de uma linha de comando.

> Nesta aula, e não apenas neste tópico, a ideia é apresentar os conceitos para que você seja capaz de saber do que eu estou falando, mas muitos deles serão abordados com profundidade em futuros tópicos e aulas. 

### Operadores

Os operadores são sequências de um ou mais caracteres (também chamados de *"metacaracteres"*) que possuem a função especial de separar as *palavras* e estabelecer uma relação entre elas conforme dois contextos:

* **Contexto de comandos:** operadores de controle e operadores de redirecionamento.

* **Contexto de expressões:** operadores de atribuição, aritméticos, lógicos, de comparação, etc...

### Palavras

Uma palavra é qualquer sequência de caracteres que não contenha operadores. De acordo com a sua função semântica, as palavras podem ser comandos internos, palavras-chaves (*keywords*), nomes de executáveis ou argumentos. Do ponto de vista do shell, porém, todas as palavras de uma mesma linha de comando são seus **parâmetros**, mas nós voltaremos a isso mais adiante.

## 5.3 - Regras de citação (quoting)

No processo de análise da linha do comando, a ocorrência de alguns caracteres determinará se as palavras e os operadores deverão, ou não, ser tratados de acordo com seu significado especial para o shell. De forma análoga ao que acontece na redação, esses mecanismos recebem o nome de **regras de citação**.

> Em inglês, o termo utilizado é ***quoting*** (citação), o que frequentemente é traduzido como *"aspas"* - o que não está de todo errado, já que as aspas são o principal mecanismo de citação, ao lado do escape. Contudo, as regras de citação vão muito além disso.

### Escape de caracteres

A ocorrência do caractere `\` remove um eventual significado especial do caractere seguinte. Por exemplo:

```
:~$ echo $fruta
laranja
:~$ echo \$fruta
$fruta
```

No segundo comando, a barra invertida fez com que o caractere `$`, usado para indicar a expansão de uma variável, perdesse o seu significado especial e fosse tratado como um caractere literal qualquer.

### Aspas simples

Todos os caracteres entre aspas simples serão tratados literalmente, ou seja, sem significado especial, inclusive o caractere de escape (`\`):

```
:~$ echo \$fruta
$fruta
:~$ echo '\$fruta'
\$fruta
```

Por conta disso, é impossível escapar aspas simples se elas aparecerem entre aspas simples:

```
:~$ echo '$fruta 'madura'
>  <---- O shell não entende o comando como completo!
```

Tentando escapar...

```
:~$ echo '$fruta \'madura'
>  <---- Nada mudou!
```

### Aspas duplas

As aspas duplas removem os poderes especiais de todos os caracteres que aparecerem entre elas, menos:

* Do cifrão (`$`)
* Do acento grave (**`**)
* Da barra invertida (`\`)
* E da exclamação (`!`)

### Uma observação sobre os espaços

O caractere de escape (`\`) e as aspas também removem o poder especial do caractere espaço. Pode não parecer, mas o espaço também é um **operador de controle** e, portanto, tem o "poder especial" de separar as palavras em uma linha de comando. Sendo assim, espaços entre aspas (duplas ou simples) ou após uma barra invertida serão apenas espaços que **fazem parte de uma palavra**.

Muita gente diz que utiliza aspas para "proteger os espaços" ou "preservar os espaços", mas isso passa uma ideia totalmente errada sobre o que está acontecendo! Observe:

```
:~$ echo lindo    dia
lindo dia
```

Neste comando, sem aspas, todos os espaços são operadores de controle separando palavras:

```
  palavras encontradas
           |
      +----+-------+
      |    |       |
:~$ echo lindo    dia
        |     ||||
        +-+---++++
          |
  operadores separando palavras
```

 Logo, o comando `echo` exibirá apenas as palavras encontradas que serão utilizadas como seus argumentos, no caso: `lindo` e `dia`. Por padrão, o `echo` separa os argumentos recebidos com um espaço, o que resulta na string `lindo dia`.


Nos comandos abaixo, com os parâmetros do `echo` entre aspas, todos os caracteres, inclusive os espaços, estão formando uma única palavra:

```
:~$ echo 'lindo    dia'
lindo    dia
:~$ echo "lindo    dia"
lindo    dia
```

Tudo que o `echo` faz é exibir este único argumento:

```
  palavras encontradas
           |
      +----+----+
      |         |
:~$ echo "lindo    dia"
        |
        |
  operador separando palavras
```

### Citação de caracteres ANSI-C

Existe uma situação especial que ocorre quando as aspas simples são precedidas do cifrão (`$'string'`). Neste caso, se a string contiver caracteres de controle do padrão ANSI-C, eles serão decodificados.

Os caracteres de controle ANSI-C são conjuntos formados pela barra invertida (`\`) seguida de um caractere ou de um número inteiro correspondente ao valor de um caractere na tabela ASCII. Quando decodificados, eles podem resultar em ações geralmente associadas com o controle de impressão dos antigos terminais de teletipo, como a quebra de linha (`\n`) e a tabulação (`\t`), entre outros.

De certa forma, nós podemos dizer que o significado dos caracteres de controle ANSI-C é o que está sendo tratado literalmente por esta regra de citação. Observe o exemplo:

```
:~$ echo banana\nlaranja
banananlaranja
```

Aqui, a barra invertida apenas escapou o caractere `n`, que não possui um significado especial para o shell. Contudo, `\n` é um caractere de controle ANSI-C. Aplicando a regra para obter o seu significado literal...

```
:~$ echo $'banana\nlaranja'
banana
laranja
```

Outro exemplo interessante, mostra o movimento do cursor (antigamente, seria "do carro") para trás com o caractere `\b`:

```
:~$ echo $'banana\b\blidade'
banalidade
```

A lista de caracteres ANSI-C que o shell pode decodificar [está aqui](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#ANSI_002dC-Quoting). Nela, nós encontramos um caractere que pode nos ajudar a representar aspas simples literalmente entre aspas simples.

Lembra deste exemplo?

```
:~$ echo '$fruta 'madura'
>  <---- O shell não entende o comando como completo!
```

Nós não podemos escapar aspas simples dentro de aspas simples, mas podemos fazer isso:

```
:~$ echo $'$fruta \'madura'
$fruta 'madura
```

Aqui, `\'` é o caractere ANSI-C `'`, não uma aspa simples escapada, mas existe um outro detalhe sobre a citação de aspas simples que deve ser considerado: eles não são decodificados na primeira etapa do processamento da linha de um comando, mas na terceira etapa, junto com as expansões do shell.

Se o caractere fosse decodificado na primeira etapa, os comando acima seria visto da seguinte forma pelo shell:

```
       Caractere decodificado
                 |
                 V
:~$ echo '$fruta 'madura'
>  <---- Daria na mesma...
```

Mas, se podemos deixar o caractere `'` para ser decodificado na terceira etapa, nós também poderíamos utilizar uma expansão:

```
:~$ as="'"
:~$ echo $as
'
:~$ echo '$fruta '$as'madura'
$fruta 'madura
```

Se quiser comprovar o que estou dizendo, tente repetir este último comando trocando `$as` pelo caractere `'`:

```
:~$ echo '$fruta '''madura'
>  <---- Não deu certo...
```

### Comentários - a citação máxima

Nós não costumamos pensar nos comentários dessa forma, mas eles são o mecanismo de citação mais radical que podemos encontrar no shell, pois eles citam o usuário literalmente, e nada do que estiver comentado será tratado como um comando.

Na linha de um comando, quando uma palavra é precedida de uma cerquilha (`#`), tudo, a partir dela, é simplesmente descartado como parte do comando. 

```
:~$ echo olá # mundo
olá
:~$ # echo olá mundo
:~$
```

Mas atenção com as aspas:

```
:~$ echo 'olá # mundo'
olá # mundo
```

Aqui, a cerquilha perdeu seus "poderes especiais", mas nem é só por conta das aspas simples -- neste caso, ela não está precedendo uma palavra, mas fazendo parte dela!

É raro, mas pode acontecer dos comentários estarem desabilitados para o modo interativo, e os exemplos acima podem não funcionar. Se for o seu caso, você pode habilitá-los com o comando `shopt`:

```
:~$ shopt -s interactive_comments
```

Para desabilitar essa opção do shell:

```
:~$ shopt -u interactive_comments
```

Para o modo não-interativo (os nossos scripts) os comentários sempre estão habilitados. Portanto, você não tem nenhuma desculpa para não comentar o seu código!

## 5.4 - Expansão de apelidos (aliases)

Um "apelido" (ou *alias*, em inglês) é uma palavra à qual foi atribuída uma string que corresponde a um comando. No shell, isso é feito com o comando interno `alias` que, além de fazer a atribuição, registra o novo apelido numa **lista de apelidos** que ficará disponível durante toda a sessão do shell.

Repare que eu disse *"lista de apelidos"*, ou seja, não é uma lista qualquer! É importante deixar isso claro porque, na primeira etapa de processamento da linha de um comando, o shell precisará saber quais são os apelidos definidos para providenciar a sua expansão. Para entender como isso é feito, vejamos como o comando `alias` funciona.

### Listando os 'aliases' da sessão corrente

Sem argumentos, o comando `alias` exibe a lista de apelidos definidos para a sessão corrente, por exemplo, estes são meus aliases:

```
:~$ alias
alias ..='cd ..'
alias cd..='cd ..'
alias crontab='crontab -i'
alias dir='dir $LS_OPTIONS'
alias egrep='egrep $GREP_OPTIONS'
alias fgrep='fgrep $GREP_OPTIONS'
alias gitc='git clone'
alias grep='grep $GREP_OPTIONS'
alias i3conf='nano ~/.config/i3/config'
alias l='ls -Nhl'
alias l.='l -d .*'
alias la='l && l.'
alias ll='l -C'
alias ls='ls $LS_OPTIONS'
alias mocp='mocp --theme=transparent-background'
alias neofetch='neofetch --block_range 0 15'
alias up='sudo apt update && upinfo'
alias upg='sudo apt upgrade'
alias upl='echo -e "$(apt list --upgradable 2>/dev/null)" | most'
alias yt='youtube-dl --add-metadata -ic'
alias yta='youtube-dl --add-metadata -xic'
alias ~='cd ~'
```

> É nesta lista que o shell procura os apelidos que deverá expandir na primeira etapa de processamento dos comandos.

Para exibir a definição de apenas um apelido...

```
:~$ alias grep
alias grep='grep $GREP_OPTIONS'
```

### Criando novos apelidos

Cada um dos apelidos da lista acima foi definido com o comando `alias`. Por exemplo, vamos criar um novo apelido:

```
:~$ alias zzz='echo Está dormindo?'
:~$ zzz
Está dormindo?
:~$ 
```

Agora, o apelido `zzz` também deve aparecer na listagem:

```
:~$ alias
alias ..='cd ..'
alias cd..='cd ..'
alias crontab='crontab -i'
...
alias zzz='echo Está dormindo?'
alias ~='cd ~'
```

Pelo tamanho da minha lista de apelidos, deve ser óbvio que eu não digitei nada do que está lá na linha de comandos! Mas, como os apelidos dizem respeito apenas à sessão corrente do shell, o comando `alias` tem que ser executado de alguma forma, e isso é feito incluindo esses comandos no arquivo `~/.bashrc` (no caso do Bash), que é um script que o Bash executa automaticamente quando inicia.

### Criando apelidos contendo variáveis

Na minha lista de apelidos, você deve ter visto que alguns deles contêm a expansão de uma variável, por exemplo:

```
:~$ alias grep
alias grep='grep $GREP_OPTIONS'
```

Onde `GREP_OPTIONS` é uma variável que eu também defini no meu `~/.bashrc`:

```
GREP_OPTIONS='--color=auto'  
```

Como vimos nas primeiras aulas, o `$` antes do nome de uma variável significa que queremos que o nome da variável seja substituído pelo seu valor, através de um mecanismo chamado: **expansão de parâmetros**.

O problema é que, segundo a ordem das etapas de processamento de comandos, a expansão dos apelidos acontece antes (etapa 1) da expansão dos parâmetros (etapa 3)! Então, como isso pode estar certo?

A resposta é simples: estamos falando da **criação de um apelido**, que é um comando como outro qualquer, logo, as expansões de parâmetros serão realizadas (ou não) na execução do comando de criação do apelido.

> Os comandos de criação de apelidos no arquivo `~/.bashrc` são executados quando o Bash é iniciado.

Veja, neste exemplo, como as coisas acontecem:

```
:~$ fruta=banana
:~$ alias teste1='echo $fruta'
:~$ alias teste2="echo $fruta"
:~$ alias | grep 'alias teste'
alias teste1='echo $fruta'
alias teste2='echo banana'
```

Na primeira linha, eu defini a variável `fruta` com o valor `banana`. Em seguida, eu criei dois *aliases*. No primeiro, eu envolvi a string do comando com aspas simples. Como vimos, as *aspas simples* impedem a expansão de de parâmetros na string, por isso o apelido registrado na lista foi uma *versão literal* do comando atribuído a `teste1`:

```
:~$ alias teste1
alias teste1='echo $fruta'
```

Já no segundo comando, eu utilizei *aspas duplas*, que permitem a expansão de parâmetros, o que resultou no seguinte registro na lista de apelidos:

```
:~$ alias teste2
alias teste2='echo banana'
```

No fim das contas, a diferença entre os dois apelidos é que `teste1`, ao ser expandido na primeira etapa do processamento de uma linha de comando, resultará em uma string contendo o acesso ao valor de uma variável (`$fruta`), mas essa expansão só acontecerá na terceira etapa, com um resultado que dependerá da existência da variável `fruta` e de seu valor. Por sua vez, o apelido `teste2` sempre será expandido, logo na primeira etapa do processamento, para `echo banana`.

Observe o que acontece se eu mudar o valor de `fruta`:

```
:~$ fruta=laranja
:~$ teste1
laranja
:~$ teste2
banana
:~$ 
```

### Removendo apelidos

A remoção de apelidos é feita com o comando interno `unalias`:

```
unalias NOME     # Remove o apelido 'NOME'.
unalias -a       # Remove todos os apelidos da lista.
```

Antes, você pode conferir se o apelido está na lista:

```
:~$ alias zzz
alias zzz='echo Está dormindo?'
```

Se estiver, você pode removê-lo:

```
:~$ unalias zzz
```

E depois, conferir novamente se a operação foi bem-sucedida:

```
:~$ alias zzz
bash: alias: zzz: não encontrado
```

Se quiser remover todos os apelidos, é só utilizar a opção `-a`:

```
:~$ alias
alias ..='cd ..'
alias cd..='cd ..'
alias crontab='crontab -i'
alias dir='dir $LS_OPTIONS'
alias egrep='egrep $GREP_OPTIONS'
alias fgrep='fgrep $GREP_OPTIONS'
alias gitc='git clone'
alias grep='grep $GREP_OPTIONS'
alias i3conf='nano ~/.config/i3/config'
alias l='ls -Nhl'
alias l.='l -d .*'
alias la='l && l.'
alias ll='l -C'
alias ls='ls $LS_OPTIONS'
alias mocp='mocp --theme=transparent-background'
alias neofetch='neofetch --block_range 0 15'
alias teste1='echo $fruta'
alias teste2='echo banana'
alias up='sudo apt update && upinfo'
alias upg='sudo apt upgrade'
alias upl='echo -e "$(apt list --upgradable 2>/dev/null)" | most'
alias yt='youtube-dl --add-metadata -ic'
alias yta='youtube-dl --add-metadata -xic'
alias ~='cd ~'
:~$ unalias -a
:~$ alias
:~$
```

Se você se arrepender, basta abrir outro terminal ou reiniciar a execução do arquivo `~/.bashrc` com o comando interno `source`:

```
:~$ source ~/.bashrc
:~$ alias
alias ..='cd ..'
alias cd..='cd ..'
alias crontab='crontab -i'
alias dir='dir $LS_OPTIONS'
alias egrep='egrep $GREP_OPTIONS'
alias fgrep='fgrep $GREP_OPTIONS'
alias gitc='git clone'
alias grep='grep $GREP_OPTIONS'
alias i3conf='nano ~/.config/i3/config'
alias l='ls -Nhl'
alias l.='l -d .*'
alias la='l && l.'
alias ll='l -C'
alias ls='ls $LS_OPTIONS'
alias mocp='mocp --theme=transparent-background'
alias neofetch='neofetch --block_range 0 15'
alias up='sudo apt update && upinfo'
alias upg='sudo apt upgrade'
alias upl='echo -e "$(apt list --upgradable 2>/dev/null)" | most'
alias yt='youtube-dl --add-metadata -ic'
alias yta='youtube-dl --add-metadata -xic'
alias ~='cd ~'
```
Repare que isso só restaurou os apelidos que estavam no `~/.bashrc`. Todos os apelidos criados na linha de comandos da sessão corrente do shell serão perdidos.

> O comando `source` serve para executar os comandos de um arquivo no shell em execução. Ele também pode ser invocado com um ponto (`.`) em vez de seu nome: `. ~/.bashrc`, por exemplo.

### O arquivo '.bash_aliases'

Muitas distribuições GNU/Linux incluem este código no `~/.bashrc`:

```
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
```

Eu escreveria diferente, mas, deixando os detalhes desse tipo de estrutura para um momento mais adequado, o que o código faz é testar se o arquivo `~/.bash_aliases` existe...

```
[ -f ~/.bash_aliases ]
```

Caso exista, seu conteúdo será carregado com um `source` (o ponto é o comando `source`)...


```
. ~/.bash_aliases
```

O motivo da existência dessa opção é que, caso seja necessário recarregar as definições de apelidos, em vez de executar novamente todos os comandos no arquivo `~/.bashrc`, o que pode incluir coisas que nós não queremos que sejam recarregadas, basta fazer um `source` dos comandos em `~/.bash_aliases`.

> É sempre uma boa ideia utilizar o arquivo `~/.bash_aliases` para os apelidos e funções que você mesmo criar.


## 5.5 - Estados de saída

Nas próximas aulas nós falaremos de coisas que acontecem nas etapas intermediárias do processamento da linha de um comando, mas este é o melhor momento para vermos algo que acontece no final da última etapa: os estados de saída.

Nem todo comando exibirá uma mensagem em caso de erro, mas o shell registra o estado de saída de todos os comandos executados numa variável especial de nome `?` -- se o comando terminar com **sucesso**, o valor em `?` será zero (`0`), se terminar com **erro**, o valor pode ser qualquer inteiro entre `1` e `255`.

Quando falamos do arquivo `~/.bash_aliases`, nós vimos a seguinte estrutura:

```
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
```

Todos os comandos entre `if` e `fi` formam aquilo que nós chamamos de **comando composto**. Aqui, o que o `if` está fazendo é aguardar o término do comando interno `test`, executado em uma de suas sintaxes:

```
[ -f ~/.bash_aliases ]
```

Isso é o mesmo que:

```
test -f ~/.bash_aliases
```

Neste caso, o comando `test` está fazendo uma afirmação: "o arquivo `~/.bash_aliases` existe e é um arquivo comum" -- é isso que o operador `-f` faz. Se essa afirmação for verdadeira, o comando terminará com estado de saída de sucesso e o valor `0` será registrado na variável especial `?`. Se a afirmação for falsa, o comando será terminado com erro e um valor diferente de zero será registrado em `?`.

Com base no estado de saída do comando `test`, a estrutura `if` decidirá se executará ou não a carga do arquivo `~/.bash_aliases`.

Vamos experimentar sem o comando composto `if`:

```
:~$ test -f ~/.bash_aliases
:~$ echo $?
0
```

Como o estado de saída registrado em `?` foi `0`, sucesso, eu posso afirmar que tenho o arquivo `~/.bash_aliases` no meu sistema.

Vejamos o que acontece se eu testar um arquivo que eu sei que não existe:

```
:~$ test -f ~/banana.txt
:~$ echo $?
1
```

Agora, a afirmação é falsa e o estado de saída registrado em `?` foi `1`, o que representa um erro.

Vamos observar a saída de outro comando:

```
:~$ alias zzz
bash: alias: zzz: não encontrado
```

Sim, nós removemos o apelido `zzz`, daí a mensagem de erro. Mas, qual será o estado de saída da linha do comando?

```
:~$ alias zzz
bash: alias: zzz: não encontrado
:~$ echo $?
1
```

Novamente, o valor registrado em `?` foi `1`, erro. Mas, conferindo a saída de um apelido que existe...

```
:~$ alias ls
alias ls='ls $LS_OPTIONS'
:~$ echo $?
0
```

Sucesso!

### Operadores de encadeamento condicional

Adiantando um pouco do assunto do próximo tópico, existem dois operadores de controle utilizados para condicionar a execução de um comando ao estado de saída do comando anterior, são os operadores de encadeamento condicional: `&&` e `||`.

Voltando ao exemplo do arquivo `~/.bash_aliases`, nós poderíamos testar sua existência desta forma:

```
:~$ [ -f ~/.bash_aliases ] && echo existe || echo não existe
existe
```

Aqui, nós temos três comandos:

```
Comando 1: [ -f ~/.bash_aliases ]
Comando 2: echo existe
Comando 3: echo não existe
```

Entre os comandos `1` e `2`, existe o operador `&&`, que só permitirá a execução do comando `2` se o comando `1` terminar com sucesso -- como aconteceu no exemplo.

Entre os comandos `2` e `3`, existe o operador `||`, que só permite a execução do comando `3` se o comando `2` terminar com erro. No exemplo, o comando `2` terminou com sucesso, logo, o comando `3` não foi executado.

Mas, como ficaria no caso de um arquivo que não existe?

```
:~$ [ -f ~/banana.txt ] && echo existe || echo não existe
não existe
```

Desta vez, o comando `1` terminou com erro e o comando `2` sequer chegou a ser executado. Sendo assim, o último estado de saída registrado foi o do comando `1` (erro), o que permitiu a execução do comando `3`.

### Comandos 'true', ':' e 'false'

Toda a lógica do shell se baseia nos estados de saída dos comandos. Não existem constantes para *verdadeiro* e *falso* nem tipos de dados booleanos, apenas estados de saída de sucesso ou erro. Mas existem três comandos que não fazem nada além de sair com estados `0` (sucesso) e `1` (erro):

* Comando `true` - sempre sai com estado `0`;
* Comando `:` - (chamado de *comando nulo*) sempre sai com estado `0`;
* Comando `false`: sempre sai com estado `1`.

Você pode utilizá-los, inclusive, para aprender a utilizar os operadores de encadeamento condicional:

```
:~$ true && echo sucesso || echo erro
sucesso
:~$ : && echo sucesso || echo erro
sucesso
:~$ false && echo sucesso || echo erro
erro
```

### Saída de erros (stderr)

Além do estado de saída, registrado na variável `?`, o comando pode gerar dados na saída padrão de erros (*stderr*). Em sistemas *unix-like*, este fluxo de dados geralmente está conectado com a saída padrão (*stdout*), e é por isso que nós vemos as mensagens no terminal.

Nós teremos uma aula só sobre redirecionamentos, mas podemos utilizar dois conceitos vistos nas aulas anteriores para demonstrar algo que pode ser muito útil desde já: o operador de redirecionamento para arquivos (`>`) e o dispositivo nulo (`/dev/null`).

Por padrão, o operador de redirecionamento para arquivos enviará para um arquivo, claro, o conteúdo do fluxo de dados na saída padrão (*stdout*), que corresponde ao descritor de arquivos `1`. Mas, nós podemos especificar o redirecionamento de outros descritores de arquivos, por exemplo, o descritor de arquivos `2`, que é o fluxo de dados da saída de erros (*stderr*). Para isso, basta informar o número `2` antes do operador `>`.

Por exemplo...

Sem redirecionamento da saída de erros:

```
:~$ alias zzz
bash: alias: zzz: não encontrado
```

Com o redirecionamento da saída de erros para `/dev/null`:

```
:~$ alias zzz 2> /dev/null
:~$
```

Isso não altera o estado de saída, que continuará sendo `1` (erro), mas possibilita a criação de lógicas de execução e mensagens de erro customizadas. Por exemplo:

```
:~$ alias zzz 2> /dev/null || echo 'Alias não existe!'
:~$ Alias não existe!
```

Aqui, a definição do apelido `zzz` só será exibida se o comando `alias zzz` terminar com sucesso. Caso contrário, a minha mensagem customizada é que será exibida. Vamos testar com um apelido existente:

```
:~$ alias ls 2> /dev/null || echo 'Alias não existe!'
alias ls='ls $LS_OPTIONS'
```

Mas, se a ideia for redirecionar ambas as saídas (`1` e `2`), basta acrescentar o caractere `&` antes do operador `>`:

```
:~$ alias ls &> /dev/null && echo existe || echo não existe
existe
```


## 5.6 - Operadores de controle

Existem dois tipos de operadores envolvidos no processamento de uma linha de comando:

* Operadores de controle
* Operadores de redirecionamento

Independente de seu tipo, todo operador no contexto dos comandos é formado de um ou mais **metacaracteres**, que são os caracteres que o Bash utiliza para separar as palavras de um comando. Eles podem ser um espaço, uma tabulação, uma quebra de linha ou combinações dos caracteres `|`, `&`, `;`, `(`, `)`, `<` e `>`.

Todos esses operadores possuem uma característica em comum: eles são separadores de palavras e, portanto, a menos que possam ser confundidos com as palavras que eles separam, não existe a obrigatoriedade de se deixar um espaço antes ou depois deles.
 
Por exemplo:

```
:~$ ps -e | grep bash
```

Poderia ser escrito...

```
:~$ ps -e|grep bash
```

> **Só para reforçar:** espaços, tabulações e quebras de linha também são 'metacaracteres' e, portanto, 'separadores de palavras'.

### Tabela dos operadores de controle

Nesta aula, nós nos limitaremos a apresentar os operadores para que você seja capaz de identificá-los numa linha de comando. No futuro, haverá uma aula exclusiva para tratar dos operadores de redirecionamento e outra para os *pipes*, e muitos dos operadores de controle desta tabela serão vistos dentro de seus respectivos contextos de uso.

| Operador | Função |
|---|---|
| `&&` | Encadeamento condicional de comandos. Executa o comando seguinte se o anterior terminar com estado de "sucesso" (saída `0`). |
| `||` | Encadeamento condicional de comandos. Executa o comando seguinte se o anterior terminar com estado de "erro" (saída diferente de `0`). |
| `|` | Cria uma *pipeline*, onde cada comando envia a sua saída para a entrada do comando seguinte. |
| `|&` | Um pipe que envia a saída padrão e a saída de erros de um comando para o comando seguinte. |
| `;` | Indica o fim de cada comando ou *pipeline* em uma lista de comandos escrita em uma mesma linha. Quando a lista é escrita em várias linhas, o `;` é dispensável, tendo sua função substituída pela ocorrência de uma ou mais quebras de linha. |
| `&` | Executa comandos em segundo plano. Quando aparece delimitando o fim de um comando, faz com ele seja executado assincronamente em um *subshell*. |
| `( )` | Agrupa uma lista de comandos. Com os parêntesis, toda a lista será executada em um *subshell* e será tratada como se fosse apenas um comando para efeito de redirecionamentos e de captura do estado de saída. |
| `;;` | Delimita o fim de uma lista de comandos em uma cláusula do comando composto `case`, causando o término dos testes comparativos dos padrões. |
| `;&` | Delimita o fim de uma lista de comandos em uma cláusula do comando composto `case`, mas indica que a lista de comandos da cláusula seguinte também deve ser executada incondicionalmente. |
| `;;&` | Delimita o fim de uma lista de comandos em uma cláusula do comando composto `case`, mas indica que os padrões de todas as cláusulas subsequentes também devem ser testados. |
