# Aula 7 - Expansões de parâmetros

- [Vídeo - parte 1](https://youtu.be/lDVfQF_e1tk)
- [Vídeo - parte 2](https://youtu.be/mM9V1DfEP_8)
- [Vídeo - parte 3](https://youtu.be/Lj2HaY8Nebs)
- [Vídeo - parte 4](https://youtu.be/zfyjXulZFfY)
- [Playlist completa](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)

## 7.1 – Argumentos e parâmetros

Quando falamos da primeira etapa de execução de comandos, nós dissemos que as sequências de caracteres serão separadas em palavras. Geralmente, uma dessas palavras será um comando interno do shell ou o nome de um executável, e as demais palavras serão os argumentos desse comando ou desse programa:

```
:~$ comando arg1 arg2 ... argN
:~$ programa arg1 arg2 ... argN
```

Ainda de modo geral, os **argumentos** são valores que o programa utilizará na execução de suas tarefas. Internamente, no código, o programador precisa reservar um espaço na memória para que os valores dos argumentos sejam recebidos. Contudo, ele só sabe que o programa deve esperar um valor, não o valor em si, porque isso é imprevisível -- ou seja, o valor pode **variar**. Sendo assim, tudo que ele pode fazer é dar um nome para esse espaço reservado para o valor, e é isso que nós chamamos de **variável**.

As variáveis podem ser criadas em um programa para diversas finalidades, mas nós estamos falando especificamente da recepção de valores que poderão determinar como o programa funcionará ou, eventualmente, os dados que ele irá processar, e isso recebe o nome de **argumentos formais** ou, simplesmente, **parâmetros**. Portanto, **nesse contexto**, variáveis e parâmetros são sinônimos, já que o programador define os parâmetros de que o programa precisa para ser executado utilizando variáveis. 

> No contexto interno de programas, o conceito de parâmetro também se aplica a qualquer variável que identifica o espaço reservado para a recepção de valores em uma função.

Voltando às linhas de comandos do shell, nós temos uma situação inusitada: o shell já está em execução e está aguardando comandos. Os comandos, como vimos, são essencialmente palavras e operadores, mas não há como o shell saber de antemão o que vamos digitar na linha de comando, então, internamente, ele também reserva espaços para a recepção dos valores que irão determinar o que ele fará em seguida, ou seja, ele espera receber **parâmetros**. Logo, tudo que nós digitamos na linha de um comando são os parâmetros que o shell utilizará para interpretar e executar os comandos.

### Resumindo os conceitos

* **Argumentos:** são os valores passados para um programa na linha de um comando.

* **Parâmetros:** são a forma como o espaço reservado para a recepção de um argumento é representado internamente no programa.

* **Variáveis:** são a forma como o espaço reservado para a atribuição de um valor é representado em um programa -- se forem utilizadas para representar os espaços reservados para a recepção de argumentos, elas também poderão ser chamadas de parâmetros.


### Parâmetros posicionais

Na maioria das plataformas, sempre que um programa recebe o controle da execução (entra no chamado *"tempo de execução"*), o sistema operacional passa para ele dois parâmetros: o contador de argumentos (`argc`), que contém o número de argumentos passados para o programa, e o vetor de argumentos (`argv`), que é uma array (matriz) contendo cada um dos argumentos passados para ele na linha de comando.

Em `argv`, cada argumento é indexado conforme a sua ordem de aparição na linha do comando, começando com o índice `0`, que será o nome do arquivo executável do programa. Por exemplo, para o programa `cat`, executado da forma abaixo, receberia os parâmetros:

```
:~$ cat teste.txt
     ↑       ↑
   argv[0] argv[1] argc=2
```

No shell não é diferente, e nós podemos acessar o elemento em `argv[0]`, que ele recebeu quando passou a ser executado, com o nosso já conhecido parâmetro especial `0`:

```
:~$ echo $0
bash
```

Mas perceba: o parâmetro especial `0` é uma variável que o shell disponibiliza para que nós tenhamos acesso indireto (e, portanto, sujeito a processamento) ao que ele recebeu internamente no parâmetro `argv[0]` quando foi executado. Tanto que podem haver diferenças no valor do parâmetro especial `0` dependendo do modo de execução do shell.

Quando o Bash é executado a partir de um login (no console, por exemplo, ou por SSH), o valor do parâmetro `0` será precedido de um traço:

```
:~$ echo $0
-bash
```

Já se for executado por um script, o valor em `0` será o nome do script. Observe como isso acontece no script `teste.sh`, abaixo:

```
#!/bin/bash

echo $0
```

Executando...

```
:~$ ./teste.sh
./teste.sh
```

O parâmetro especial `0` faz parte de um conjunto de parâmetros que o shell disponibiliza para que nós tenhamos acesso a todos os valores que ele mesmo recebeu internamente no parâmetro `argv`. Como esses valores são indexados conforme a posição em que aparecem na linha do comando, no shell, eles são chamados de **parâmetros posicionais**.

Vamos alterar um pouco o script `teste.sh`:

```
#!/bin/bash

echo $0 $1 $2 $3
```

Agora, nós queremos ter acesso a outros três argumentos da linha do comando em que o script for executado, por exemplo:

```
:~$ ./teste.sh banana laranja abacate
./teste.sh banana laranja abacate
```

Os parâmetros posicionais seguem as mesmas regras de separação de palavras e de citação aplicadas no processamento de uma linha de comando. Por exemplo:

```
:~$ ./teste.sh banana 'laranja abacate' pera
./teste.sh banana laranja abacate pera
```

Repare que o nosso script só exibe os quatro primeiros parâmetros posicionais, mas cinco palavras foram exibidas na saída, e isso aconteceu porque `'laranja abacate'` está entre aspas simples e, portanto, é apenas uma palavra.

### Total de argumentos

O parâmetro `argc` também possui um equivalente no shell: é o parâmetro especial `#`. Contudo, diferente de `argc`, que inclui o nome do executável na contagem, o valor em `#` considera apenas os valores nos parâmetros especiais a partir do `1`. Vamos alterar o script `teste.sh` novamente:

```
#!/bin/bash

echo $0 $1 $2 $3
echo Total de argumentos recebidos: $# 
```

Executando de várias formas (tente explicar as diferentes saídas):

```
:~$ ./teste.sh banana
./teste.sh banana
Total de argumentos: 1

:~$ ./teste.sh banana laranja
./teste.sh banana laranja
Total de argumentos: 2

:~$ ./teste.sh banana 'laranja abacate'
./teste.sh banana laranja abacate
Total de argumentos: 2

:~$ ./teste.sh banana 'laranja abacate' pera
./teste.sh banana laranja abacate pera
Total de argumentos: 3

:~$ ./teste.sh banana 'laranja abacate' pera uva
./teste.sh banana laranja abacate pera
Total de argumentos: 4

:~$ ./teste.sh banana laranja abacate pera uva
./teste.sh banana laranja abacate
Total de argumentos: 5
```


### Acessando todos os argumentos

O shell oferece dois parâmetros especiais para o acesso a todos os argumentos passados na linha de comando (menos o nome do executável): `*` e `@`. na maioria das vezes, não há diferenças entre eles, mas o parâmetro especial `@` tem o diferençal de respeitar as regras de citação na separação de palavras se for expandido entre aspas duplas.

Vamos alterar o script `teste.sh`:

```
#!/bin/bash

echo $0 $1 $2 $3
echo Total de argumentos recebidos: $# 
echo 'Argumentos recebidos (*):' $*
echo 'Argumentos recebidos (@):' $@
```

Executando, note que não há uma diferença visível na saída:

```  
:~$ ./teste.sh banana 'laranja abacate' pera
./teste.sh banana laranja abacate pera
Total de argumentos: 3
Argumentos recebidos (*): banana laranja abacate pera
Argumentos recebidos (@): banana laranja abacate pera
```

A diferença só será notada quando o `@` for expandido entre aspas duplas (`"$@"`) e as palavras que ele contiver forem acessadas individualmente, por exemplo (script `teste2.sh`):

```
#!/bin/bash

echo Percorrendo palavras com '"$@"'
for p in "$@"; do echo $p; done

echo

echo Percorrendo palavras com '$*'
for p in $*; do echo $p; done
```


Executando...

```
:~$ ./teste2.sh banana 'laranja abacate' pera
Percorrendo palavras com "$@"
banana
laranja abacate
pera

Percorrendo palavras com $*
banana
laranja
abacate
pera
```

### Para o shell, toda variável é um parâmetro

Como vimos, do ponto de vista do shell, tudo que escrevemos numa linha de comando, inclusive nas linhas de comandos escritas nos scripts, será recebido por ele como parâmetros. Alguns parâmetros são passados diretamente através de seus valores literais (uma string, um número, um caminho, etc...), mas outros podem ser nomes que reservam espaços na linha do comando para que sejam substituídos pelos valores que eles identificam no momento do processamento do comando.

Nesses casos, em vez do valor, nós temos um identificador que representa um parâmetro, e esse é o conceito de **variável** no contexto do shell - nomes que identificam valores de parâmetros ou, como eu costumo dizer para diferenciar dos parâmetros formais de um programa ou de uma função: **parâmetros nomeados**.

### Parâmetros especiais

A tabela abaixo contém uma lista de parâmetros especiais do Bash que todos nós deveríamos tatuar na ponta da língua:

| Parâmetro |O que expande... |
|---|---|
| `0` a `n` | Parâmetros posicionais passados para um script ou uma sessão do shell. O parâmetro `0` é o nome do shell ou do script em execução. |
| `@` | Todos os parâmetros posicionais (menos o parâmetro `0`). Quando entre aspas duplas (`"$@"`), faz com que o shell trate parâmetros com espaços escapados como uma única palavra. |
| `*` | Todos os parâmetros posicionais (menos o parâmetro `0`). |
| `#` | Número total de parâmetros posicionais (desconta o parâmetro posicional `0`). |
| `?` | Estado de saída do último comando executado. |
| `$` | PID da sessão corrente do shell. |
| `!` | Identificação do processo mais recente colocado para ser executado em segundo plano. |
| `-` | A lista dos flags de configuração do shell. |


### O que realmente é expandido

Pode parecer um preciosismo, mas eu acho importante destacar a diferença conceitual entre variáveis e parâmetros quando estudamos o shell. Isso ajuda a reforçar o fato de que, mesmo nos scripts, tudo que escrevemos são linhas de comandos e será interpretado e processado como linhas de comandos, onde cada palavra, mesmo que só seja expandida ao longo do processamento, será um parâmetro recebido pelo shell.

Antes desta aula, eu só usei a expressão "expansão de variáveis" para dar tempo de você chegar à explicação do conceito de parâmetros. Sendo mais exato agora, o termo "variável", quando falamos do shell, diz respeito diretamente ao identificador de um valor que será usado posteriormente como parâmetro em uma linha de comando. O processo de substituir um identificador pelo seu valor, e com isso transformá-lo em um parâmetro da linha do comando, é o que chamamos de **expansão de um parâmetro**, e é isso que acontece quando o shell encontra um identificador precedido por um cifrão (`$`).

> Por analogia com outros conceitos da computação, nós podemos (e iremos) continuar chamando os 'parâmetros nomeados' de 'variáveis'. O objetivo aqui não é propor uma correção na forma de nos referimos ao conceito, mas esclarecer qual é, de fato, o conceito. Por isso mesmo, o próximo tópico será...

## 7.2 - Variáveis no shell

Na linha de um comando, uma variável é basicamente um **nome** (ou *identificador*) que aponta para uma determinada informação a que chamamos de **valor**. No Bash, como em qualquer outra linguagem, nós podemos criar, alterar e eliminar variáveis. 

A forma mais direta de criação de uma variável é através de um tipo de operação chamada de **atribuição**. Numa atribuição, nós definimos um nome, e atribuímos a ele um valor utilizando o **operador de atribuição** `=`.

Veja alguns exemplos:

```
:~$ fruta=banana
:~$ nota=10
:~$ nomes=(João Maria José)
:~$ retorno=$(whoami)
:~$ soma=$((a + 10))
```

> Atenção! Não pode haver espaços antes ou depois do operador de atribuição!

Além da operação de atribuição, nós também podemos criar variáveis através de alguns comandos, como o comando interno `read`, por exemplo, que lê um dado que o usuário digita no terminal, ou que venha de um arquivo, e atribui o valor lido a um nome:

```
:~$ read -p 'Digite seu nome: ' nome
Digite seu nome: Renata
:~$ echo $nome
Renata
```

O comando interno `declare` é outra forma de criação de variáveis. Com ele, entre outras coisas, nós também podemos especificar se o valor armazenado será de um tipo diferente do padrão, que é o tipo **indeterminado**.

Por exemplo:

```
:~$ declare -i soma
```

Mas, tenha sempre em mente que as variáveis no Bash não são declaradas. Uma declaração, a rigor, envolveria a reserva de um certo espaço em bytes na memória de acordo com o tipo do dado que será armazenado na variável, e não é bem assim que o Bash funciona. Para começar, o `declare` é um comando cuja função básica é definir e listar **atributos** de variáveis. Então, no exemplo acima, tudo que ele faz é dizer ao interpretador que os valores recebidos na variável `soma` deverão ser tratados como valores numéricos inteiros ou expressões que resultem em inteiros, e não como strings, como seria feito por padrão.

Tente analisar o exemplo abaixo:

```
:~$ declare -i soma; a=15; b=10; c=$a+$b
:~$ echo $c
15+10
:~$ soma=$c
:~$ echo $soma
25
```

Repare que a variável `c` contém uma string que apenas se parece com uma soma, por isso ela é expandida como `15+10`. Por outro lado, a variável `soma`, que teve o atributo de inteiro definido pela opção `-i` do comando `declare`, permite a interpretação da string como uma *expressão aritmética* e, portanto, é expandida para o valor `25`.

Uma das consequências das variáveis não serem declaradas no Bash, é que este é um comando totalmente válido e não gera um estado de saída de erro:

```
:~$ echo $fruta; echo $?

0
```

Para o shell, o nome `fruta` não possui um valor associado, por isso a expansão é feita para uma string vazia (a quebra de linha do exemplo veio do comando `echo`) e nenhum erro é gerado. 

### Nomeando variáveis

Os nomes das variáveis que nós mesmos criamos em nossos códigos e na linha de comandos podem conter apenas:

* Letras maiúsculas e minúsculas da tabela ASCII;
* Dígitos (caracteres entre `0` e `9`);
* O caractere sublinhado (_).

Todas as combinações desses caracteres são válidas, menos uma: **um número nunca pode aparecer como o primeiro caractere do nome**.

Aqui estão alguns exemplos de nomes válidos:

```
fruta
Fruta
_fruta
fruta1
frutaVermelha
fruta_vermelha
```

Já estes nomes são inválidos:

```
:~$ maçã=fruta
bash: maçã=fruta: comando não encontrado

:~$ 2a=10
bash: 2a=10: comando não encontrado
```

## 7.3 – Vetores (arrays)

Quando um nome de variável aponta para um único valor, nós dizemos que essa variável é **escalar**. Mas, também podemos criar variáveis que apontam para uma **coleção de valores** – são os chamados **vetores**, também conhecidos como *arrays* ou matrizes.

No Bash, um vetor pode ser criado atribuindo uma lista de valores entre parêntesis a um nome:

```
:~$ alunos=(João Maria Renata)
```

Alternativamente, nós podemos criar o mesmo vetor do exemplo acima pela atribuição individual de valores a cada um de seus elementos:

```
:~$ alunos[0]=João; alunos[1]=Maria; alunos[2]=Renata
```

Repare que o nome da variável é `alunos`, e os números que aparecem entre os colchetes são seus **índices**, também chamados de **subscritos**, daí eles receberem o nome de **vetores indexados** (ou *arrays* indexadas). No Bash, este é o tipo padrão de vetor, mas também podemos criar vetores cujos índices são strings – são os chamados **vetores associativos**. O detalhe, porém, é que os vetores associativos dependem da definição de um atributo especial através do comando `declare`:

```
:~$ declare -A carro
:~$ carro[vw]=fusca
:~$ carro[lindão]=jipe
:~$ carro[de boi]=carroça
```

Também podemos criar vetores indexados e associativos especificando seus índices quando a atribuição é feita com uma lista de valores entre parêntesis:

```
:~$ declare -A carro
:~$ carro=([vw]=fusca [fiat]=palio [ford]=k)
:~$ alunos=([3]=João [1]=Maria [5]=José)
```

No caso específico dos vetores indexados, se os índices não forem especificados, o shell atribuirá um número inteiro sequencial a partir de `0` ao índice de cada elemento, o que será feito de acordo com a sua ordem de aparição na lista.

## 7.4 - Expansão de valores em variáveis

Toda ocorrência do nome de uma variável iniciada com o caractere `$` (cifrão) na linha de um comando fará o shell **expandir** o valor armazenado na variável na terceira etapa de processamento de um comando (aula 5).

A forma padrão de se fazer referência ao nome de uma variável para que a expansão ocorra é `${NOME}`, mas as chaves são dispensáveis quando se trata de uma variável escalar. Então, as duas formas abaixo são válidas:

```
:~$ a=banana; b=laranja
:~$ echo Fui ao mercado comprar $a e ${b}.
Fui ao mercado comprar banana e laranja.
```

Porém, se eu quisesse incluir um caractere `s` após a expansão da variável `a`, veja o que aconteceria:

```
:~$ a=banana; b=laranja
:~$ echo Fui ao mercado comprar $as e ${b}s.
Fui ao mercado comprar e laranjas.
```

Neste caso, a variável `a` não foi expandida. Na verdade, ela nem sequer existe no comando! Repare que, sob qualquer ponto de vista, nosso ou do shell, nós tentamos expandir uma variável não definida com nome `as`. A variável `b`, por sua vez, não passou por esse tipo de problema porque nós utilizamos as chaves para separar seu nome do restante da string.

Corrigindo...

```
:~$ a=banana; b=laranja
:~$ echo Fui ao mercado comprar ${a}s e ${b}s.
Fui ao mercado comprar bananas e laranjas.
```

> Muitos programadores em Bash chamam isso de "proteger a variável". Embora faça algum sentido, eu evito utilizar e recomendar essa terminologia, porque não se trata de uma proteção, e sim da sintaxe padrão para a expansão de variáveis.

Quando a variável é **vetorial** (indexada ou associativa), o uso das chaves e do subscrito tornam-se **obrigatórios**:

```
:~$ alunos=(João Maria José)
:~$ echo ${alunos[1]}
Maria
```

Sem o subscrito, com ou sem as chaves, o shell expande o elemento de índice `0` de um vetor indexado:

```
:~$ echo ${alunos}
João
```

Para expandir todos os valores no vetor, nós utilizamos os caracteres `@` ou `*` dentro dos colchetes: 

```
:~$ alunos=(João Maria 'Luis Carlos')
:~$ echo ${alunos[*]}
João Maria Luis Carlos
```

A diferença entre o `*` e o `@` está na forma como as palavras contendo espaços em branco são tratados na expansão (reveja as *regras de citação* na aula 5). Se o vetor for acessado entre aspas e com o `@` no subscrito, os espaços serão preservados como parte da palavra expandida, o que fica mais fácil de comprovar nas iterações de um loop `for`.

Utilizando o `*`:

```
:~$ alunos=(João Maria 'Luis Carlos')
:~$ for nome in ${alunos[*]}; do echo $nome; done
João
Maria
Luis
Carlos
```

Utilizando o `@`:

```
:~$ for nome in "${alunos[@]}"; do echo $nome; done
João
Maria
Luis Carlos
```

## 7.5 – Indireções

Com `*` ou `@` no subscrito, é possível expandir todos os índices de um vetor em vez de seus elementos. Isso é feito com uma exclamação (`!`) antes do nome:

```
:~$ alunos=(João Maria 'Luis Carlos')
:~$ echo ${!alunos[@]}
0 1 2
```

> Repare que, para utilizar símbolos que modificam as expansões de variáveis, o uso das chaves torna-se obrigatório. 

Quando utilizada em **variáveis escalares**, a exclamação faz com que seja expandida uma **indireção**, que é a expansão de uma variável cujo nome corresponde ao valor armazenado em outra variável.

Por exemplo:

```
:~$ animal=cavalo; cavalo=equino
:~$ echo $animal
cavalo
:~$ echo ${!animal}
equino
```

## 7.6 – Número de elementos e de caracteres

As expansões de variáveis também podem ser modificadas para que seja retornado o número de caracteres de uma variável escalar ou o número de elementos de um vetor:

```
:~$ fruta=banana
:~$ animais=(zebra leão gnu)
:~$ echo ${#fruta}
6
:~$ echo ${#animais[@]}
3
```

Mas, se o índice de um elemento da array for especificado, nós teremos a quantidade de caracteres deste elemento:

```
:~$ echo ${#animais[1]}
4
```

## 7.7 – Escopo de variáveis

O escopo é a disponibilidade de uma variável em uma sessão do shell, em partes diferentes do código de um script, ou em sessões derivadas da sessão corrente do shell (como sessões filhas e subshells). No Bash, a menos que se defina o contrário, uma variável sempre estará disponível em qualquer parte da sessão corrente do shell, ou seja: em princípio, todas as variáveis são globais.

Todavia, quando falamos da disponibilidade das variáveis entre sessões diferentes do shell, existem pelo menos três situações que precisamos considerar:

* As sessões filhas
* As variáveis de ambiente
* E os subshells

### Sessões filhas

Uma sessão filha é uma sessão do shell iniciada a partir de uma sessão já em execução (sessão mãe), e é o que acontece, por exemplo, quando executamos um script ou o próprio executável do Bash na linha de comandos. Sendo mais exato, uma sessão filha é um processo filho do processo do shell que é o líder da sessão (aula 4), como podemos observar com o utilitário `ps` e a variável especial `$`, que expande o PID do shell corrente.

```
:~$ echo $$
21335
:~$ bash
:~$ echo $$
22524
:~$ ps t
  PID TTY      STAT   TIME COMMAND
21335 pts/0    Ss     0:00 bash
22524 pts/0    S      0:00 bash
22599 pts/0    R+     0:00 ps t
```

Desta vez, nós utilizamos o `ps` com a opção `t`, da notação BSD, para que a saída ficasse limitada à exibição aos processos em execução no terminal e também para termos as informações da coluna `STAT`. Isso nos permite ver o estado do processo seguindo de caracteres adicionais, como o `s` que aparece na linha do processo 21335, indicando que este é o processo líder da sessão (mais sobre isso na aula 4).

> Nem sempre a sessão do shell iniciada junto com uma nova instância de um terminal é um processo líder de sessão – isso depende das configurações do sistema operacional e do ambiente gráfico em uso.

Voltando aos escopos, as variáveis criadas numa sessão mãe **não estão disponíveis nas sessões filhas**, e é por isso que se diz que **todas as variáveis de uma sessão do shell são, em princípio, locais em relação às demais sessões**. 

Observe o exemplo:

```
:~$ fruta=banana
:~$ echo $fruta
banana
:~$ bash
:~$ echo $fruta

:~$
```

Na sessão filha, iniciada com a invocação do executável `bash`, a variável `fruta` não estava definida e nada foi expandido. Porém, é possível copiar uma variável para as sessões filhas, e uma das formas de se fazer isso é com o comando interno `export`:


```
:~$ fruta=banana
:~$ echo $fruta
banana
:~$ export fruta
:~$ bash
:~$ echo $fruta
banana
```

Um detalhe importante do comando `export`, é que ele altera um dos atributos da variável – o atributo de exportação. Quando ligado, este atributo faz com que a variável seja copiada para cada nova sessão criada a partir da sessão mãe original. Isso significa que `fruta` estaria disponível, não apenas para a sessão filha que nós inciamos, mas também para as sessões netas, bisnetas e assim por diante.

Outra forma de manipular o atributo de exportação de uma variável é através do comando `declare` com a opção `-x`:

```
:~$ declare -x fruta=banana
:~$ echo $fruta
banana
:~$ bash
:~$ echo $fruta
banana
```

### Variáveis de ambiente

Uma variável de ambiente é uma variável que teve, em algum momento, o seu **atributo de exportação** ligado e, portanto, está disponível para as suas sessões filhas. Isso significa que, quando um novo processo do shell é iniciado, um ou mais scripts de configuração deverão ser executados para que as variáveis de ambiente sejam recriadas.

Para listar todas as variáveis com atributo de exportação, inclusive as que nós mesmos criarmos durante uma sessão, basta executar:

```
:~$ export -p
```

A lista retornada é longa demais para ser transcrita aqui, mas, se você testou os exemplos anteriores, provavelmente a variável `fruta` aparecerá desta forma em uma das últimas linhas exibidas:

```
declare -x fruta="banana"
```

Como estamos falando de **sessões** e **ambientes**, esta é uma ótima oportunidade para conhecermos a variável de ambiente `SHLVL` (que deve ter aparecido na listagem do comando acima, se você testou). Sua função é armazenar o nível relativo da sessão corrente do shell.

O resultado do próximo exemplo pode variar de acordo com a sua distribuição GNU ou o seu ambiente gráfico, mas é bem provável que, se você abrir um terminal e expandir a variável `SHLVL`, o nível da sessão inicial do shell também seja `1`, o que será incrementado a cada subnível do shell:

```
:~$ echo $SHLVL
1
:~$ bash
:~$ echo $SHLVL
2
:~$ bash
:~$ echo $SHLVL
3
```

A variável `SHLVL` é definida pelo próprio shell no momento da sua execução (por isso é chamada de "variável do shell") e recebe o atributo de exportação para que, na criação de uma sessão filha, seu valor possa ser consultado e incrementado. Como as alterações feitas em variáveis exportadas na sessão filha não afetam a sessão mãe, elas só serão propagadas para as sessões filhas da sessão filha.

### Subshells

Como vimos na última aula, um subshell, que também é um processo e tem o seu próprio PID, não requer a reconfiguração do shell para restaurar as variáveis definidas, já que ele herda uma cópia de todas as variáveis em uso na sessão mãe, inclusive as variáveis `$` (PID da sessão corrente) e `SHLVL` (nível da sessão corrente).

Após este primeiro momento, porém, um subshell tem o mesmo comportamento de um processo normal do shell, e todas as variáveis criadas e alteradas nele terão escopo local em relação às demais sessões do shell em execução.

## 7.8 - Exportando variáveis apenas para um novo processo

Como vimos, nós podemos ativar o atributo de exportação de uma variável com os comandos `declare -x` e `export`. Contudo, às vezes pode ser útil definir uma variável para que seja utilizada apenas no ambiente de um processo filho do shell, o que inclui outro shell ou qualquer outro programa que faça uso desses dados. Isso pode ser feito com uma atribuição antes da invocação do nome do executável em questão, por exemplo:

```
:~$ fruta=banana bash -c 'echo $fruta; declare -p fruta'
banana
declare -x fruta="banana"
:~$ echo $fruta

:~$ 
```

Aqui, nós definimos a variável `fruta` no mesmo comando em que invocamos o executável do Bash. Deste modo, a variável foi exportada para o novo processo, onde pôde ser expandida com o `echo`. Mas, além da expansão, nós ainda verificamos as propriedades da variável com o comando `declare -p`, o que nos mostrou que seu atributo de exportação (`-x`) estava ativado. 

Agora, o fato mais interessante do exemplo é que a atribuição só teve efeito para o novo processo, e `fruta` não estava definida para a sessão corrente do shell.


## 7.9 – Variáveis inalteráveis (read-only)

Normalmente, nós podemos criar variáveis com um determinado valor e mudá-lo à vontade quando necessário...

```
:~$ fruta=banana
:~$ echo $fruta
banana
:~$ fruta=laranja
:~$ echo $fruta
laranja
```

Porém, existem casos onde precisamos garantir que um determinado valor não seja mudado. Com o comando interno `readonly`, o shell retornará um erro caso ocorra uma tentativa de alterar o valor da variável:

```
:~$ fruta=banana
:~$ readonly fruta
:~$ fruta=laranja
bash: fruta: a variável permite somente leitura
```

Outra forma de tornar uma variável inalterável, é com a opção `-r` do comando interno `declare`:

```
:~$ declare -r animal=gato
:~$ animal=zebra
bash: animal: a variável permite somente leitura
```

> Variáveis inalteráveis só podem ser destruídas com o fim da sessão em que foram criadas.

## 7.10 – Destruindo variáveis

Internamente, quando criamos uma variável, ela entra para um tipo de tabela na memória que o shell utiliza para rastrear seu nome, seus atributos e as alterações do seu valor. Cada sessão do shell tem a sua própria tabela para controlar variáveis, logo, com o fim da sessão, todas as variáveis relacionadas com aquele processo deixam de existir.

Como as variáveis não são declaradas no Bash, não existe diferença prática entre uma variável vazia e uma variável não definida.

Veja no exemplo:

```
:~$ fruta=
:~$ echo Eu quero uma $fruta para o meu $animal
Eu quero uma para o meu
```

Aqui, a variável `fruta` foi definida e iniciada com o valor de uma string vazia, mas a variável `animal` sequer foi definida. Em ambos os casos, a expansão resultou numa string vazia, mas por motivos totalmente diferentes: `fruta` existia e estava vazia, enquanto `animal` não existia e, portanto, não havia o que expandir.

Isso pode ser confirmado com a opção `-p` do comando `declare`, utilizada para exibir os atributos de uma variável:

```
:~$ fruta=
:~$ declare -p fruta
declare -- fruta=""
:~$ declare -p animal
bash: declare: animal: não encontrado
```

Como dissemos, **na prática** não há diferença e, a menos que o uso de recursos do sistema seja uma preocupação, nós podemos atribuir uma string vazia à variável sempre que quisermos torná-la nula. Mas, se nós realmente quisermos destruí-la, será preciso utilizar o comando interno `unset`:

```
:~$ fruta=banana
:~$ declare -p fruta
declare -- fruta="banana"
:~$ unset fruta
:~$ declare -p fruta
bash: declare: fruta: não encontrado
```

## 7.11 - Outras expansões de parâmetros

Uma das coisas mais surpreendentes do shell, é que nós podemos transformar de diversas maneiras o valor de uma variável durante o processo de expansão. Assim, os dados identificados por uma mesma variável podem ser utilizados de diversas formas diferentes sem que seu valor seja alterado -- tudo acontece apenas na expansão do parâmetro.

Nos próximos tópicos nós veremos como essas outras expansões funcionam e como podem nos ajudar em situações que, de outra forma, exigiriam códigos e lógicas bem mais complexas para termos resultados semelhantes.

Mas não espere dominá-las todas de uma vez! Encare os próximos tópicos como a apresentação de um novo amigo -- você só irá conhecê-lo realmente com o tempo e com a convivência. Aqui é mais ou menos a mesma coisa. A ideia é que você saiba que esses recursos existem e estão à sua disposição, mas é só com o uso em diversos contextos que virá o domínio.


## 7.12 – Substrings

Não é raro nós precisarmos de uma parte de uma string em vez de todos os seus caracteres, e o shell oferece duas formas de fazermos isso com expansões: pela posição dos caracteres iniciais e finais de uma parte da string, ou através do casamento de padrões. Neste tópico, nós veremos apenas a primeira forma, que é indicando as posições dos caracteres da faixa desejada.

Para facilitar, vamos considerar a string na variável abaixo:

```
:~$ str=abcdefghij
```

A expansão de uma substring pode ser feita da esquerda para a direita (de `a` para `j`) ou na direção contrária (de `j` para `a`). O que determina a direção da contagem é o sinal do número que indica a posição do caractere – números positivos são contados a partir do início da string e negativos são contados a partir do final.

A contagem sempre é feita a partir do zero (`0`) e deve ser imaginada como um ponto **antes** dos caracteres:

```
  .a  .b  .c  .d  .e  .f  .g  .h  .i  .j
  0   1   2   3   4   5   6   7   8   9
-10  -9  -8  -7  -6  -5  -4  -3  -2  -1  
```

A expansão da substring é feita com a definição da posição inicial e da quantidade de caracteres a serem expandidos, ambas antecedidas por dois pontos (`:`).

Mas, vamos com calma, vejamos como ela funciona apenas com a definição do ponto inicial:

```
# Posições iniciais positivas...
:~$ echo ${str:0}
abcdefghij
:~$ echo ${str:1}
bcdefghij
:~$ echo ${str:2}
cdefghij

# Posições iniciais negativas (o espaço é obrigatório!)...
:~$ echo ${str: -1}
j
:~$ echo ${str: -2}
ij
:~$ echo ${str: -3}
hij
```

Como podemos ver, o número da posição inicial determina o ponto a partir do qual a string começará a ser expandida.

O segundo modificador, se for positivo, determina a quantidade de caracteres que será expandido a partir do ponto inicial:

```
:~$ echo ${str:0:3}
abc
:~$ echo ${str:4:2}
ef
:~$ echo ${str:2:5}
cdefg
:~$ echo ${str: -3:2}
hi
:~$ echo ${str: -4:3}
ghi
:~$ echo ${str: -6:4}
efgh
```

Se o segundo modificador for negativo, ele indicará o ponto final da expansão da substring:

```
:~$ echo ${str:0:-5}
abcde
:~$ echo ${str:4:-3}
efg
:~$ echo ${str: -6:-3}
efg
:~$ echo ${str: -3:-1}
hi
:~$ echo ${str: -10:-5}
abcde
```

Importante! A posição final não pode ser um ponto anterior à posição inicial! 

Da mesma forma que a posição final, quando omitida, é assumida como "até o fim", quando omitimos a posição inicial, ela será assumida como "do início":

```
:~$ echo ${str::3}
abc
:~$ echo ${str::-3}
abcdefg
:~$ echo ${str::5}
abcde
:~$ echo ${str::-5}
abcde
:~$ echo ${str::1}
a
:~$ echo ${str::-1}
abcdefghi
```

Algumas dicas que você mesmo poderia deduzir de tudo que vimos:

* Remoção do último caractere: `${nome::-1}`
* Remoção do primeiro caractere: `${nome:1}`
* Remoção dos "n" últimos caracteres: `${nome::-n}`
* Remoção dos "n" primeiros caracteres: `${nome:n}`

Dica extra! Como todas as expansões de parâmetros acontecem na mesma etapa da avaliação da linha do comando, elas podem ser utilizadas umas dentro das outras (aninhadas):

```
:~$ final=5
:~$ echo ${str:3:$final}
defgh
```


## 7.13 - Faixas de elementos de vetores

Agora, vamos supor que a nossa variável seja um vetor (uma array):

```
:~$ compras=(banana laranja leite ovos presunto queijo)
```

Com um vetor criado desta forma, o shell atribuirá a cada elemento um índice numérico inteiro a partir de zero (`0`). Isso permite a expansão de um elemento a partir da indicação de seu índice, mas nós estamos interessados em expandir uma faixa de elementos. Portanto, teremos que pensar em termos de posições, assim como fizemos com as expansões de substrings:

```
 .banana .laranja .leite .ovos .presunto .queijo
 0       1        2      3     4         5
-6      -5       -4     -3    -2        -1
```

Entendido esse preceito básico, tudo que vimos sobre a expansão de substrings é diretamente aplicável aos elementos de um vetor.

> Lembre-se: para expandir todos os elementos de um vetor nós utilizamos o `@` ou o `*` no subscrito!

```
# Expandindo o primeiro elemento...
:~$ echo ${compras[@]::1}
banana

# Expandindo o último elemento...
:~$ echo ${compras[@]: -1}
queijo

# Expandindo do terceiro ao quarto elemento...
:~$ echo ${compras[@]:2:3}
leite ovos presunto
```

Mas, atenção! Se o vetor for do tipo associativo, aquele que tem strings como índices, não existe uma definição de como será feita a ordenação dos elementos, por exemplo:

```
# Definindo o atributo de ‘vetor associativo’...
:~$ declare -A carros

# Populando o vetor...
:~$ carros[vw]=fusca
:~$ carros[fiat]=147
:~$ carros[ford]=K
:~$ carros[willis]=jeep

# Expandindo os valores dos elementos...
:~$ echo ${carros[@]}
jeep fusca 147 K
```

Então, apesar de ainda podermos expandir faixas de elementos, nós não podemos contar com pressupostos como: "o primeiro elemento foi o primeiro a entrar no vetor" ou "o último foi o último a ser definido". Isso não é verdade e precisa ser lembrado, principalmente, quando o vetor associativo é populado de forma não-interativa, por exemplo:

```
# Definindo o vetor 'arr' como associativo...
declare -A arr

# Populando seus elementos em um loop 'for'...
for i in {a..e}; do
	arr[$i]=$i
done
```

Aqui, ao fim dos ciclos, os elementos de `arr` serão:

```
1º ciclo: arr[a] → a
2º ciclo: arr[b] → b
3º ciclo: arr[c] → c
4º ciclo: arr[d] → d
5º ciclo: arr[e] → e
```

Mas sua ordem interna pode ser:

```
:~$ echo ${arr[@]}
e d c b a
```

Se tentarmos expandir o último elemento do vetor imaginando que ele é o mesmo que foi definido no último ciclo, teremos problemas:

```
:~$ echo ${arr[@]: -1}
a
```

Tirando esse detalhe, não há nenhuma diferença na expansão de faixas de elementos de vetores indexados e associativos.

Os parâmetros especiais `@` e `*`, que expandem todos os parâmetros posicionais, comportam-se exatamente como os vetores.

## 7.14 - Remoções e substituições a partir de padrões

Se quisermos expandir uma substring a partir do início ou do fim de um parâmetro, nós podemos utilizar **padrões** em vez das posições de seus caracteres. Neste caso, os padrões seguem os mesmos princípios da formação de nomes de arquivos, e nós só temos que definir se queremos casá-lo a partir do início ou do fim da string:

* `${nome#padrão}` → Casa o padrão a partir do início.
* `${nome%padrão}` → Casa o padrão a partir do fim.


Por exemplo, digamos que a string seja `banana da terra` e o padrão seja `?a` (um caractere qualquer seguido de `a`). Se o casamento for feito com o começo da string, a correspondência será `ba`, mas, se for com o final, será `ra`. Em ambos os casos, o padrão será removido da string na expansão:

```
# Definindo a string...
:~$ fruta='banana da terra'

# Expandindo tudo menos o padrão casado no início...
:~$ echo ${fruta#?a}
nana da terra

# Expandindo tudo menos o padrão casado no final...
:~$ echo ${fruta%?a}
banana da ter
```

> **Dica:** pense no padrão como aquilo que você quer 'remover' da string!

Neste tipo de expansão, o casamento do padrão é feito com **a menor correspondência possível**. Por exemplo, se o padrão fosse `*n` (qualquer quantidade de caracteres seguida de um `n`) e nós quiséssemos casá-lo com o início da string, veja o que aconteceria:

```
# String...
:~$ fruta='banana da terra'

# Expandindo tudo menos o padrão casado no início...
:~$ echo ${fruta#*n}
ana da terra
```

Repare que `*n` casou apenas com os primeiros caracteres antes de um dos caracteres `n` existentes na string, e a sequência `ban` foi removida.

O mesmo aconteceria se o padrão fosse `n*` (n seguido de qualquer quantidade de caracteres) e o casamento fosse com o final da string:

```
# String...
:~$ fruta='banana da terra'

# Expandindo tudo menos o padrão casado no final...
:~$ echo ${fruta%n*}
bana
```

Como podemos ver nos dois exemplos, ainda havia um `n` que poderia ter sido casado.

Para que o casamento aconteça com a maior quantidade possível de caracteres, nós dobramos os símbolos: `##`, para remover o padrão do início, e `%%`, para remover do final.

> **Outra dica:** nunca se esqueça de que o padrão deve representar a sequência de caracteres que você quer 'remover' da string original!

```
# String...
:~$ fruta='banana da terra'

# Casamento máximo no início...
:~$ echo ${fruta##*n}
a da terra

# Casamento máximo no final...
:~$ echo ${fruta%%n*}
ba
```

Para que você entenda um detalhe importante sobre as expansões que removem e substituem padrões em strings, não se esqueça de que os parâmetros posicionais, obviamente, também são parâmetros. Portanto, tudo que vimos sobre expansões de parâmetros também se aplicam aos parâmetros especiais, inclusive os parâmetros `@` e `*`. Ou seja, se o seu script receber como parâmetros `banana` e `laranja`, nós podemos expandi-los, todos de uma vez, com `$@` ou `$*` e aplicar os conceitos que estamos vendo nesta aula. Mas, como podemos exemplificar isso sem criarmos um script?

É aí que entra o comando interno set!

### Uma pausa para o comando 'set'

O comando set é utilizado para definir ou remover os valores das opções de execução do shell, mas também pode definir e modificar valores de parâmetros posicionais – inclusive no modo interativo!

> O comando `help set` pode fornecer informações adicionais, caso você esteja curioso, mas nós vamos nos concentrar apenas na sua capacidade de manipular os parâmetros posicionais.

Um dos argumentos do comando `set` é o `--` (dois traços). Se algo estiver antes dele, será tratado como uma lista de argumentos do próprio `set`. Mas, tudo que vier depois será tratado como parâmetro posicional da sessão corrente do shell. Se não houver nada após o `--`, os parâmetros posicionais da sessão corrente do shell serão destruídos.

Por exemplo:

```
# Eliminando qualquer parâmetro posicional anterior...
:~$ set --
:~$ echo $@

:~$

# Definindo dois parâmetros posicionais...
:~$ set -- banana laranja
:~$ echo $1
banana
:~$ echo $2
laranja
echo $@
banana laranja

# Alterando os parâmetros posicionais...
:~$ set -- abacate
:~$ echo $@
abacate
```

Era exatamente disso que nós precisávamos para os exemplos dos próximos detalhes sobre as expansões de parâmetros!

### Voltando às remoções e substituições

Quando uma expansão de remoção ou substituição com base em padrões é feita com os nomes dos parâmetros especiais `@` e `*`, ela é aplicada individualmente a cada um dos parâmetros posicionais:

```
# Definindo os parâmetros posicionais da sessão corrente...
:~$ set -- abano banana dezena
:~$ echo $@
abano banana dezena

# Removendo o padrão ‘*n’ da expansão de cada parâmetro...
echo ${@#*n}
o ana a
```

Os padrões também podem ser utilizados para substituir caracteres na expansão de parâmetros, e este é o modificador utilizado:

```
${nome/padrão/string}
```

Por exemplo:

```
:~$ str=banana
:~$ echo ${str/na/nda}
bandana
```

Repare que apenas o primeiro `na` foi substituído, apesar de haver dois em `banana`.

Se o padrão for precedido de `/`, todas as ocorrências encontradas serão substituídas:

```
:~$ str=banana
:~$ echo ${str//na/ta}
batata
```

Mas, observe que aqui prevalece o maior casamento possível:

```
:~$ str=banana
:~$ echo ${str//n*/ta}
bata
```

Se o padrão for precedido da cerquilha (`#`), ele tem que casar com o início do valor expandido. É como na expansão com remoção utilizando duas cerquilhas (`##`), só que, em vez de ser removido, o casamento será substituído:

```
:~$ str=banana
:~$ echo ${str/#b??/cab}
cabana
```

Para fazer o mesmo com o final do valor expandido, nós utilizamos o `%`:

```
:~$ str=banana
:~$ echo ${str/%a??/ido}
banido → banana[ido]
```

> **Para reforçar:** se o parâmetro for `@` ou `*`, as expansões de remoção e substituição serão aplicadas a cada um dos parâmetros posicionais, se for um vetor com `@` ou `*` no subscrito, elas serão aplicadas a cada um dos elementos.

## 7.15 – Expandindo valores condicionalmente

O shell realiza quatro tipos de expansões condicionadas ao estado de definição do parâmetro (se ele está ou não definido). É com elas que nós podemos dizer ao shell o que fazer caso uma variável esteja indefinida ou tenha um valor nulo (string vazia).

O primeiro tipo de expansão condicional (e o mais conhecido) é aquele com que nós definimos o **valor padrão** de um parâmetro, ou seja, caso o parâmetro seja nulo ou não esteja definido, o Bash fará a expansão de um valor padrão:

```
# Expande 'valor' se 'nome' for nulo ou não definido.
${nome:-valor}

# Expande 'valor' apenas se 'nome' for não definido.
${nome-valor}
```

Por exemplo, se o parâmetro `var` for **não-definido**, ambas as expansões resultarão na string padrão:

```
:~$ unset var
:~$ echo ${var:-padrão}
padrão
:~$ echo ${var-padrão}
padrão
```

Mas, estando o parâmetro definido, se o seu valor for nulo, somente a expansão com `:-` resultará na string padrão:

```
:~$ var=
:~$ echo ${var:-padrão}
padrão
:~$ echo ${var-padrão}

:~$
```

Note que esta expansão não altera o valor original do parâmetro!
Se o parâmetro contiver algum valor, este é que será expandido (com ou sem os dois pontos):

```
:~$ var=banana
:~$ echo ${var:-padrão}
banana
:~$ echo ${var-padrão}
banana
```

Opcionalmente, em vez de apenas expandirmos, nós podemos **atribuir** o valor padrão ao parâmetro não-definido ou com valor nulo.

```
# Expande 'valor' e o atribui a 'nome'
# se nome for nulo ou não definido.
${nome:=valor}

# Expande 'valor' e o atribui a 'nome'
# apenas se 'nome' for não-definido.
${nome=valor}
```

Observe os exemplos comentados abaixo:

```
:~$ unset var           # Tornando variável não definida.
:~$ echo $var           # Conferindo...
                        # Não expande nada.

:~$ echo ${var=banana}  # Sem dois pontos...
banana                  # Expandiu!
:~$ echo $var
banana                  # Atribuiu!

:~$ unset var           # Tornando não definida novamente.

:~$ echo ${var:=banana} # Com dois pontos...
banana                  # Expandiu!
:~$ echo $var
banana                  # Atribuiu!

:~$ unset var           # Tornando não definida novamente.
:~$ var=                # Atribuindo um valor nulo.

:~$ echo ${var=banana}  # Sem dois pontos...
                        # Não expandiu!
:~$ echo $var
                        # Não atribuiu!
:~$

:~$ echo ${var:=banana} # Com dois pontos...
banana                  # Expandiu!
:~$ echo $var
banana                  # Atribuiu!
```

> Note que esta expansão **altera** o valor original (nulo ou não-definido) do parâmetro!

Uma terceira expansão pode ser condicionada ao valor da variável, estando ela definida, ser nulo ou não.

```
# Expande 'valor' apenas se 'nome' *não for nulo*.
${nome:+valor}

# Expande 'valor' se 'nome' estiver definido (sendo seu valor nulo ou não).
${nome+valor}
```

Observe que agora a opção com `:+` não expandirá um valor padrão se o parâmetro contiver um valor nulo.

Observe nos exemplos comentados:

```
:~$ unset var           # Parâmetro 'var' não definido.

:~$ echo ${var:+banana} # Com dois pontos.
                        # Não expande.
:~$ echo ${var+banana}  # Sem dois pontos.
                        # Não expande.

:~$ var=                # Atribuindo um valor nulo.
:~$ echo ${var:+banana} # Com dois pontos.
                        # Não expande.
:~$ echo ${var+banana}  # Sem dois pontos.
banana                  # Expande!

:~$ var=laranja         # Atribuindo um valor não-nulo.
:~$ echo ${var:+banana} # Com dois pontos.
banana                  # Expande!
:~$ echo ${var+banana}  # Sem dois pontos.
banana                  # Expande!
```

A última expansão condicional nos permite gerar um erro se o valor do parâmetro for nulo ou não-definido.

```
# Envia 'mensagem' para a saída de erros
# se 'nome' for nulo ou não-definido.
${nome:?mensagem}

# Envia 'mensagem' para a saída de erros
# apenas se 'nome' for não-definido.
${nome?mensagem}
```

Observe:

```
:~$ unset var             # Parâmetro 'var' não definido.

:~$ echo ${var?deu erro}  # Sem dois pontos.
bash: var: deu erro       # Mensagem de erro!
:~$ echo ${var:?deu erro} # Com dois pontos.
bash: var: deu erro       # Mensagem de erro!

:~$ var=                  # Parâmetro 'var' agora é nulo.

:~$ echo ${var?deu erro}  # Sem dois pontos.
                          # Nenhum erro!
:~$ echo ${var:?deu erro} # Com dois pontos.
bash: var: deu erro       # Mensagem de erro!
```

As expansões condicionais (como todas as expansões) são um grande exemplo de como o Bash pode nos ajudar a transformar várias linhas de código e estruturas lógicas complicadas em expansões e comandos simples e objetivos.

Há quem ache que esse tipo de recurso reduz a legibilidade do código. Mas, como eu sempre digo: ***legibilidade é para quem sabe ler a linguagem em que o código foi escrito***.

## 7.16 – Alterando a caixa do texto

Nós também podemos expandir parâmetros alterando a caixa de texto de seus caracteres, tornando-os maiúsculos ou minúsculos – o que pode ser particularmente útil, por exemplo, quando temos que comparar valores recebidos do usuário com um padrão, um caractere ou uma string.

```
# Converte primeiro caractere para caixa alta.
${nome^}

# Converte todos os caracteres para caixa alta.
${nome^^}

# Converte primeiro caractere para caixa baixa.
${nome,}

# Converte todos os caracteres para caixa baixa.
${nome,,}

# Inverte a caixa do primeiro caractere.
${nome~}  

# Inverte a caixa de todos os caracteres.
${nome~~} 
```

O uso dessas expansões é muito simples, veja nos exemplos:

```
# Expandindo em caixa alta...
:~$ bicho=zebra
:~$ echo ${bicho^}
Zebra
:~$ echo ${bicho^^}
ZEBRA

# Expandindo em caixa baixa...
:~$ fruta=BANANA
:~$ echo ${fruta,}
bANANA
:~$ echo ${fruta,,}
banana

# Expandindo com caixa invertida
:~$ nome=MaRiA
:~$ echo ${nome~}
maRiA
:~$ echo ${nome~~}
mArIa
```

Opcionalmente, nós podemos definir um padrão que corresponda aos caracteres que queremos modificar, mas isso só tem efeito prático com os modificadores dobrados, que são os que afetam toda a string:

```
# Alterando a caixa de caracteres segundo um padrão.
:~$ str='casa da sogra'
:~$ echo ${str^^[cs]}
CaSa da Sogra

:~$ str='LÍNGUA DE TRAPO'
:~$ echo ${str,,[LDT]}
lÍNGUA dE tRAPO
```

Um detalhe que não tem a ver diretamente com as expansões, mas é interessante mencionar, é que existem formas de alterar a caixa de texto do valor de um parâmetro no momento da atribuição com o comando `declare`:

```
:~$ declare -l str  # Define que 'str' terá caixa baixa.
:~$ str=BOTECO
:~$ echo $str
boteco

:~$ declare -u str  # Define que ‘str’ terá caixa alta.
:~$ str=música
:~$ echo $str
MÚSICA
```

> Como sempre, se o parâmetro for `@` ou `*`, as expansões serão aplicadas a cada um dos parâmetros posicionais, se for um vetor com `@` ou `*` no subscrito, serão aplicadas a cada um dos elementos.

## 7.17 – Mais expansões de parâmetros

Para não deixarmos pontas soltas neste assunto, vamos fechar o tópico falando de algumas expansões de parâmetros um pouco menos conhecidas, mas que também merecem o nosso carinho.

### Expansão de prefixos

```
${!prefixo*} ou ${!prefixo@}
```

Expande todos os nomes das variáveis que começam com `prefixo` separados pelo primeiro caractere definido como separador padrão de palavras em `IFS`. A diferença entre o `*` e o `@` está na forma como as palavras expandidas serão tratadas: se for utilizado o `@` e a expansão estiver entre aspas duplas, cada nome expandido será tratado como uma palavra.

Exemplo:

```
:~$ echo ${!P*}
PATH PIPESTATUS PPID PROMPT_COMMAND PS1 PS2 PS4 PWD
```

### Expansão do operador 'Q' (aspas)

```
${nome@Q}
```

Expande o valor do parâmetro entre aspas.

Exemplo:

```
:~$ var=carroça
:~$ echo ${var@Q}
'carroça'
```

### Expansão do operador 'E' (escape)

```
${nome@E}
```

Tem o mesmo efeito da expansão de caracteres ANSI-C (`$'…'`).

Exemplo:

```
:~$ nl='\n'
:~$ echo "banana${nl}laranja"
banana\nlaranja
:~$ echo "banana${nl@E}laranja"
banana
laranja
```

### Expansão do operador 'P' (prompt)

```
${nome@P}
```

Expande os caracteres de comando do prompt.

Exemplo:

```
:~$ prompt='\u@\h:\w\$ '
:~$ echo ${prompt@P}
blau@enterprise:~$
:~$
```

### Expansão do operador 'a' (atributos)

```
${nome@a}
```

Expande os atributos de um parâmetro.

Exemplo:

```
:~$ declare -i num  # Define o atributo de inteiro.
:~$ echo ${num@a}
i
```

### Expansão do operador 'A' (atribuição)

```
${nome@A}
```

Expande uma string na forma de uma atribuição que, se executada, recria o parâmetro com seu valor e seus atributos.

Exemplo:

```
:~$ fruta=laranja
:~$ echo ${fruta@A}
fruta='laranja'
```
