# Aula 10 - Pipes

- [Vídeo desta aula](https://youtu.be/jRVy5PDI5qM)
- [Playlist completa](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)


Os *pipes* são a implementação mais característica da *Filosofia Unix*. Diferente dos redirecionamentos, que estabelecem um fluxo de dados entre processos e arquivos, os pipes são o mecanismo pelo qual os dados irão fluir apenas entre processos.

```
REDIRECIONAMENTOS

    PROCESSO ← fluxo de dados → ARQUIVO

PIPES

    PROCESSO1 → fluxo de dados → PROCESSO2
```

## 10.1 - Revendo as chamadas 'fork()' e 'exec()'

Todo processo inicia como uma cópia (um *fork*) de um processo pai na memória, mas precisamos destacar uma diferença entre os *forks* para um subshell e aqueles destinados aos programas externos ao shell.

No caso dos **subshells**, os subprocessos continuam a ser executados na cópia do processo pai, enquanto os comandos externos ainda causam uma chamada de sistema `exec()`, que substitui todos os recursos do processo pai pelas especificações do programa, menos no que diz respeito ao **ambiente**. Isso faz com que, ao menos em tese, cada processo encadeado por pipes seja executado em seu próprio subprocesso do shell (em subshells). Contudo, isso pode não ser verdade para todas as implementações dos pipes nos vários shells existentes.

## 10.2 - Definição e implementações

Do ponto de vista do shell, o pipe (do inglês: *cano*) é um **operador de encadeamento de fluxo de dados entre processos**. Esse encadeamento deve possibilitar que os dados gerados na saída por um processo estejam disponíveis na entrada padrão do processo seguinte. Segundo as normas POSIX, cada comando em uma *pipeline* (um encadeamento de pelo menos dois comandos por pipes) deve ser executado no ambiente de um subshell, sendo permitido, contraditoriamente, que um ou todos os comandos sejam executados no ambiente do shell corrente. Essa ambiguidade causou a implementação dos pipes de formas diferentes em vários shells.

Observe os exemplos:

```
# Shell ksh
$ echo banana | read fruta; echo $fruta
banana

# Shell bash
:~$ echo banana | read fruta; echo $fruta

:~$
```

Na implementação dos pipes no **ksh** (Korn Shell), o último comando é executado no ambiente da sessão corrente, enquanto que **o Bash executa todos os comandos em subshells, inclusive o primeiro:**

```
:~$ echo $BASHPID; echo $BASHPID | cat
             ↑              ↑
      sessão corrente   subshell
```

A saída deste exemplo mostrará que o PID exibido pelo primeiro comando `echo $BASHPID` (que não está na pipeline) é diferente do que resulta do segundo `echo`, que é o primeiro comando da pipeline: ou seja, são processos diferentes do Bash. Confira no resultado:

```
:~$ echo $BASHPID; echo $BASHPID | cat
29189 ← PID do shell do primeiro `echo'
29448 ← PID do shell do segundo `echo'
```

> **Nota:** nós precisamos fazer o teste acima com a variável `BASHPID` porque `$$` expandiria o PID do shell do processo pai, recebido durante a cópia do ambiente feita na criação do subshell.

Normalmente, quando trabalhamos com shells diferentes, a informação mais importante sobre os pipes é se o último comando é executado ou não na mesma sessão do processo pai: no caso do Bash, não é.

### Canalizando a saída de erros (stderr)

Eventualmente, além da saída padrão (*stdout*), pode ser necessário transportar os dados na saida padrão de erros (*stderr*) para o comando seguinte em uma pipeline. Para isso, o Bash nos oferece o operador `|&`. Observe como funciona:

```
:~$ ls xxxx | cat > teste.txt
ls: não foi possível acessar 'xxxx': Arquivo ou diretório inexistente
:~$ cat teste.txt 
:~$ 
```

Observe que a mensagem de erro foi exibida no terminal. Isso indica que nada foi passado para o comando `cat` ou redirecionado para o arquivo `teste.txt`. Por outro lado, utilizando o operador `|&`:

```
:~$ ls xxxx |& cat > teste.txt
:~$ cat teste.txt 
ls: não foi possível acessar 'xxxx': Arquivo ou diretório inexistente
```

## 10.3 - O sistema de arquivos 'pipefs'

Para gerenciar os fluxos de dados em pipelines, o kernel disponibiliza um sistema de arquivos virtual, montado apenas na memória (logo, indisponível no espaço do usuário), chamado **pipefs** (*Pipe File System*). Este mecanismo é totalmente gerenciado pelo kernel. Então, o que o shell faz é solicitar um arquivo temporário no **pipefs**, através da chamada de sistema `pipe()`, para que possa criar dois redirecionamentos:

```
PROCESSO1[FD:1] > pipe:[ARQUIVO_PIPE]
PROCESSO2[FD:0] < pipe:[ARQUIVO_PIPE]
```

Observe o exemplo:

```
:~$ ls -l /proc/self/fd | cat
total 0
lr-x------ 1 blau blau 64 nov 22 15:17 3 -> /proc/30300/fd
lrwx------ 1 blau blau 64 nov 22 15:17 0 -> /dev/pts/0
l-wx------ 1 blau blau 64 nov 22 15:17 1 -> pipe:[208757]
lrwx------ 1 blau blau 64 nov 22 15:17 2 -> /dev/pts/0
```

Aqui, `pipe:[208757]` é justamente o arquivo teporário que o kernel disponibilizou para que a canalização do fluxo de dados pudesse ser feita.

> **Nota:** A representação `pipe:` cumpre exatamente o mesmo papel da barra (`/`) como símbolo da raiz do sistema de arquivos. A diferença é que **pipefs** é um sistema de arquivos monobloco, ou seja, não pode ser dividido em subdiretórios.

## 10.4 - Dispositivos FIFO

**FIFO** é uma sigla em inglês que significa: *"First In First Out"* (*o primeiro a entrar é o primeiro a sair*) e é utilizada como referência às características de um tipo de dispositivo acumulador de dados (*buffer*). Um arquivo especial do tipo FIFO é idêntico a um arquivo do pipefs, com a diferença de ser criado e acessado no sistema de arquivos no espaço do usuário. Sua principal finalidade também é canalizar fluxos de dados entre processos: razão pela qual são chamados de **pipes nomeados**. 

Quando dois processos estão trocando dados através de um FIFO, o kernel passa esses dados internamente, pela memória, sem que nada seja escrito no sistema de arquivos. Isso quer dizer que **um FIFO não possui conteúdo no sistema de arquivos** e serve apenas como um "ponto de referência", um nome que será utilizado para a troca de dados entre os processos.

### Criando FIFOs (pipes nomeados)

Existem duas ferramentas do *GNU coreutils* capazes de criar FIFOS: `mknod` e `mkfifo`, sendo esta última a mais conhecida e utilizada (e é nela que vamos nos concentrar), até mesmo pela possibilidade de definir as permissões do dispositivo no momento da sua criação.

Para criar um arquivo FIFO, nós geralmente utilizamos a seguinte sintaxe:

```
mkfifo NOME_DO_ARQUIVO
```

Por exemplo:

```
:~$ mkfifo meu-pipe
```

É simples assim!

Listando o arquivo criado, nós podemos observar que ele será identificado com a letra `p` na coluna das permissões, indicando que trata-se de um pipe (neste caso, nomeado):

```
:~$ ls -l meu-pipe
prw-r--r-- 1 blau blau 0 nov 23 13:53 meu-pipe
↑
+--- 'p' de 'pipe'!
```

Existe, inclusive, um operador do comando `test` (e do comando composto `[[`) para afirmar que um arquivo é um pipe nomeado:

```
:~$ [[ -p ~/meu-pipe ]]; echo $?
0 ← sucesso!
```

### Utilizando pipes nomeados

Quando criado com a sintaxe que vimos, um pipe nomeado pressupõe a presença de dois processos conectados a ele:

```
Um processo que escreve no FIFO:

	PROCESSO1[ESCRITA] > PIPE_NOMEADO

E um processo que lê o FIFO:

	PROCESSO2[LEITURA] < PIPE_NOMEADO
```

Sendo assim, um processo que inicie a escrita em um pipe nomeado **terá o seu término suspenso até que outro processo leia o conteúdo do arquivo FIFO**. Porém, como não acontece a criação de subprocessos, os processos 1 e 2 precisam estar em sessões de shells diferentes: tal como terminais diferentes ou planos de execução diferentes (*jobs*, execução assíncrona).

Por exemplo, colocando o `echo`, abaixo, para executar em segundo plano (observe o operador de execução assíncrona `&`), nós podemos fazer o pipe nomeado `meu-pipe` ser lido pelo `cat`:

```
:~$ echo 'Olá, pipe!' > meu-pipe & cat meu-pipe
[1] 73337
Olá, pipe!
[1]+  Concluído       echo 'Olá, pipe!' > meu-pipe
~ $ 
```

Ou ainda, lembra desse exemplo com pipes "anônimos"?

```
:~$ echo banana | read fruta; echo $fruta
        ←---- Cadê o valor de `fruta'?!
:~$
```

Veja o que aconteceria se fosse com o nosso pipe nomeado:

```
echo banana > meu-pipe & read fruta < meu-pipe; echo $fruta
[1] 73650
banana  ←---- Agora o valor de `fruta' está disponível na sessão corrente!
[1]+  Concluído              echo banana > meu-pipe
```

### Excluindo pipes nomeados

Os pipes nomeados, apesar de seu comportamento especial, são tratados como arquivos normais no que diz respeito ao seu gerenciamento. Por isso, enquanto os pipes anônimos são excluídos pelo próprio kernel em determinado momento, os pipes nomeados existirão até que sejam removidos manualmente (`rm NOME_DO_FIFO`). Mas existe um modo de tornar temporários os nossos pipes nomeados: utilizando o diretório `/tmp`.

O diretório `/tmp` é um local do sistema de arquivos destinado a arquivos que serão necessários apenas durante uma sessão de execução do sistema ou de um programa. Mesmo no caso de um programa não providenciar a remoção de seus arquivos temporários do diretório `/tmp`, o próprio sistema fará a limpeza desses arquivos no próximo boot.

Em termos práticos, a criação de FIFOs no diretório `/tmp` é muito comum e, geralmente, é feito desta forma:

```
tmp_pipe=$(mktemp -u)
mkfifo $tmp_pipe
```

Aqui, nós utilizamos uma substituição de comandos para registrar a saída do comando `mktemp -u` na variável `tmp_pipe`, que será utilizada para expandir o caminho e o nome do nosso pipe nomeado. Com essa técnica, nós podemos brincar à vontade com pipes nomeados sem nos preocuparmos com a limpeza da bagunça no nosso diretório pessoal ou em qualquer outro diretório do sistema de arquivos.

> Quando utilizado com a opção `-u`, o comando `mktemp`, do GNU coreutils, gera um caminho e um nome aleatório para um arquivo temporário, mas não cria o arquivo (como faria sem a opção `-u`).

## 10.5 - Here strings e here docs

Existem outros mecanismos do shell que utilizam os pipes para cumprir seu papel: os redirecionamentos do tipo *here string* e *here doc*. Nos dois casos, o shell passa um bloco de texto para a entrada padrão de um processo utilizando um pipe (do pipefs) para canalizar o fluxo de dados, exatamente como se estivessem fazendo um:

```
echo BLOCO_DE_TEXTO | PROCESSO_DESTINO
```

Existe uma diferença, porém: não acontece a criação de subshells. Sendo assim, isso é perfeitamente possível:

```
:~$ read var <<< "teste"
:~$ echo $var
teste
```

Antes de mostrarmos como as *here strings* e *here docs* funcionam, vamos observar como o shell utiliza um pipe do pipefs para canalizar o fluxo de dados:

```
:~$ while read; do ls -l /proc/$$/fd; done <<< 'Olá!'
lr-x------ 1 blau blau 64 nov 22 15:16 0 -> 'pipe:[220588]'
lrwx------ 1 blau blau 64 nov 24 07:48 1 -> /dev/pts/0
lrwx------ 1 blau blau 64 nov 24 07:48 10 -> /dev/pts/0
lrwx------ 1 blau blau 64 nov 24 07:48 2 -> /dev/pts/0
lrwx------ 1 blau blau 64 nov 24 07:48 255 -> /dev/pts/0

```


Aqui, nós vemos a abertura de um recurso para a leitura do pipe pela entrada padrão (descritor de arquivos 0, *stdin*) do próprio processo do shell. Quem está "lendo" o pipe são todos os comandos participantes do comando composto `while`. Como a operação com a here string acontece em duas etapas (escrita do bloco de texto e subsequente leitura pela entrada padrão), não é possível observar a ligação da saída padrão do shell com o pipe utilizado.

> O descritor de arquivos `10` foi criado para auxiliar processos internos do shell durante esse tipo de operação, provavelmente ocupando o papel do descritor de arquivos `0`, conectado ao pipe.

### Here docs

Com o operador `<<`, chamado de *here document*, ou apenas *here doc*, nós podemos fazer exatamente a mesma coisa que fizemos com o redirecionamento da entrada na aula 9, só que, em vez de um arquivo,será utilizada uma string com várias linhas como fonte de dados -- tudo que precisamos fazer é informar onde começam e onde terminam as linhas com uma palavra delimitadora. 

Esta é a sintaxe geral de uso de um here doc:

```
COMANDO [FD]<< DELIMITADOR
linha 1
linha 2
...
DELIMITADOR
```

O espaço depois do operador `<<` é opcional, assim como o número do descritor de arquivos `FD`. Se `FD` não for informado, será utilizado o descritor de arquivos `0` (entrada padrão) de `COMANDO`. 

> Importante! Não pode haver espaços antes do delimitador que faz o fechamento do bloco de texto!

O delimitador inicial pode ser qualquer palavra com ou sem aspas, e isso é o que vai determinar se haverá ou não expansões no texto do documento.

Sem aspas no delimitador:

```
cat << AQUI
linha 1
$USER
AQUI
```

Neste caso, a saída será:

```
linha 1
blau
```

Com aspas no delimitador (simples ou duplas, tanto faz):

```
cat << "AQUI"
linha 1
$USER
AQUI
```

Agora, a saída será:

```
linha 1
$USER
```

Isso pode ser muito útil para inserir códigos em shell em arquivos, porque nada será expandido ou executado:

```
:~$ cat << 'CODIGO' > novo-script.sh
> #!/usr//bin/env bash
> a=15
> b=30
> echo $((a + b))
>exit
> CODIGO
:~$
```

Lendo o arquivo criado:

```
:~$ cat novo-script.sh
#!/usr/bin/env bash
a=15
b=30
echo $((a + b))
exit
```


Para facilitar a leitura do código em scripts (e apenas em scripts), nós podemos utilizar a variante `<<-` do operador, que remove tabulações reais (`\t`) do início das linhas do bloco de texto.

No script:

```
cat <<- AQUI
	linha 1
	$USER
AQUI
```

A saída ainda seria:

```
linha 1
blau
```

### Here strings

O mais interessante de uma *here string*, é que ela manda qualquer bloco de texto para a entrada padrão de um comando sem a necessidade delimitadores, sem regras especiais para as aspas (além das conhecidas), e com quantas linhas forem necessárias.

A sintaxe geral é:

```
COMANDO [FD]<<< string
```

Simples assim! A string pode ser uma string literal ou o resultado de uma expansão. Isso quer dizer que podemos armazenar o "documento" numa variável e utilizar uma *here string* para mandá-lo para a entrada padrão de um comando:

```
:~$ cat <<< moleza
moleza
```

Enviando a expansão de uma string:

```
:~$ var="banana
> laranja
> abacate"
:~$ cat <<< $var
banana
laranja
abacate
```