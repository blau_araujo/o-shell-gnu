# Aula 9 - Redirecionamentos

- [Vídeo - parte 1](https://youtu.be/zZMGJfB-fXw)
- [Vídeo - parte 2](https://youtu.be/O2wmIbMTSOs)
- [Vídeo - parte 3](https://youtu.be/THLoti8G4dc)
- [Playlist completa](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)


É muito interessante pensar na quantidade de coisas que o shell processa antes de, efetivamente, executar um comando. Até aqui, o processamento do comando já passou pelas etapas de:

* Separação da linha do comando em palavras e operadores segundo as regras de citação;
* Expansão dos apelidos (*aliases*);
* Identificação de comandos simples, complexos e compostos;
* Expansões de chaves;
* Expansão do til;
* Expansões de parâmetros em variáveis;
* Substituições de comandos;
* Expansões aritméticas;
* Substituições de processos;
* Separação das palavras expandidas;
* Expansões de nomes de arquivos e, finalmente...
* Remoção de aspas!

Das 5 etapas que nós propusemos para o processamento dos comandos, nós vimos apenas 3 delas! Ainda falta uma etapa antes que o comando seja efetivamente executado: que é quando o shell verifica se a linha do comando, através da ocorrência de operadores especiais, envolve algum tipo de manipulação dos fluxos de dados -- estamos falando dos **redirecionamentos**.

Antes de entrarmos neste assunto, porém, precisamos entender alguns conceitos, especificamente o conceito de **descritores de arquivos** (FD) e de **fluxos de entrada e saída**.

## 9.1 – Descritores de arquivos

Indo um pouco além do contexto do shell, seja qual for o sistema operacional, nenhum programa tem acesso direto à leitura ou à escrita nos arquivos. Quando isso é necessário, o programa realiza uma solicitação de acesso indicando o arquivo com que pretende trabalhar, informa se quer realizar uma leitura ou uma escrita e, em retorno, recebe do sistema operacional um valor numérico que servirá de identificador para uma área especial **na memória** (não da mídia persistente) onde os dados serão efetivamente trabalhados. A este identificador nós damos o nome de *"file handle"* ("manipulador de arquivo", em português).

> Eu traduziria *"handle"* como "alça", mas nunca encontrei este termo utilizado na literatura técnica em português.

Tecnicamente, os manipuladores de arquivos são uma camada de abstração entre os programas que solicitam acesso aos arquivos e a sua representação na interface que um kernel POSIX implementa para gerenciar este acesso: a ** tabela de descritores de arquivos**.

Os **descritores de arquivos**, portanto, estão associados a toda uma estrutura de informações que o sistema operacional utiliza para gerenciar o acesso aos arquivos, como:

* A localização do arquivo no disco;
* O tipo de operação a ser realizada;
* Permissões;
* Atributos;
* Indicadores de estado do arquivo...

Entre outras informações.

Neste aspecto, o conceito de descritores de arquivos é parecido com o conceito de **processos**: assim como processos não são programas em execução, os descritores de arquivos não são os arquivos gravados em mídias persistentes (discos, por exemplo).

Portanto...

> Descritores de arquivos são identificadores únicos e estruturas de informações para o controle do acesso aos arquivos e a recursos e dispositivos de entrada e saída.

Os nomes dos descritores de arquivos (também chamados de "FD", de *file descriptors*) são números inteiros não-negativos (números positivos mais o zero) criados no diretório virtual `/proc/PID_DO_PROCESSO/fd/`. Note, porém, que o que torna único um FD não é o número que ele recebe como nome, mas todo o caminho representado a partir do diretório `/proc`. Logo, dois ou mais processos poderão ter descritores de arquivos com o mesmo número, mas cada um deles estará associado ao caminho do seu próprio processo.

## 9.2 - Fluxos de entrada e saída de dados

De forma geral, entradas e saídas são operações de leitura e escrita de informações em arquivos, o que se dá, como vimos, através da referência aos seus respectivos descritores.

* **Entrada:** qualquer informação que o programa ou o comando **leia** de arquivos ou de qualquer outro recurso que possa ser identificado por um descritor de arquivos.

* **Saída:** qualquer informação escrita por programas e comandos em arquivos ou em qualquer outro recurso que possa ser identificado por um descritor de arquivos.

Repare que, em ambos os conceitos, nós estamos diferenciando *"arquivos"* de *"qualquer outro recurso identificado por um descritor de arquivos"*. Isso é necessário porque, embora os descritores de arquivos estejam envolvidos nas duas situações, nem todo recurso apontado por um descritor de arquivos é um arquivo gravado em disco.

A ideia de **fluxo de dados** surge quando, através desses recursos apontados por descritores de arquivos, os comandos e programas são capazes de trocar dados entre si.

## 9.3 - Descritores de arquivos padrão

Como vimos nas quatro primeiras aulas, todo novo processo inicia, por padrão, com acesso a 3 dispositivos identificados pelos descritores de arquivos de número `0`, `1` e `2`:

| FD  | Dispositivo   | Descrição             |
| --- | ------------- | --------------------- |
| `0` | `/dev/stdin`  | Entrada padrão.       |
| `1` | `/dev/stdout` | Saída padrão.         |
| `2` | `/dev/stderr` | Saída padrão de erros |

### Stdin

Quando digitamos comandos ou respondemos a alguma solicitação de entrada de dados exibida no terminal, todos os caracteres que teclamos é lido pelo shell no dispositivo `stdin` (FD `0`), a **entrada padrão**.

### Stdout

Quando um comando ou programa produz uma saída de dados no terminal, isso é o resultado da escrita no dispositivo `stdout` (FD `1`), a **saída padrão**.

### Stderr

Se o programa precisar emitir alguma mensagem de erro, ele pode escrever no dispositivo `stderr` (FD `2`), a **saída padrão de erros**.

> Ainda por padrão, a saída de erros (FD `2`) está conectada à saída padrão (FD '1'), o que permite a exibição das mensagens de erro no terminal.

No sistema de arquivos, esses três dispositivos são links simbólicos para os descritores de arquivos em `/proc/self/fd/`:

```
:~$ ls -l /dev/std*
lrwxrwxrwx ... /dev/stderr -> /proc/self/fd/2
lrwxrwxrwx ... /dev/stdin -> /proc/self/fd/0
lrwxrwxrwx ... /dev/stdout -> /proc/self/fd/1

```

> Nesta aula, para encurtar saídas muito longas, nós utilizaremos reticências (`...`) para ocultar informações irrelevantes.

Mas, veja o que acontece se tentarmos seguir esses links:

```  
:~$ ls -l /proc/self/fd
total 0
lr-x------ ... 3 -> /proc/165890/fd
lrwx------ ... 0 -> /dev/pts/7
lrwx------ ... 1 -> /dev/pts/7
lrwx------ ... 2 -> /dev/pts/7

```

Aqui, o que nos interessam são os FDs `0`, `1` e `2` (vamos ignorar, por enquanto o FD `3`), e repare que eles também são links para outro dispositivo: o pseudoterminal `/dev/pts/7` (`7`, no meu caso). Isso demonstra o que vimos a partir do tópico 2.7 (Pseudoterminais) do nosso curso: *em dado ponto, após o início do emulador, o shell é iniciado e os fluxos de entrada, saída e saída de erros são conectados ao pseudoterminal secundário (pts)*. Ou seja, os descritores de arquivos padrão são abstrações para os fluxos de dados gerenciados pelas instâncias dos pseudoterminais. É por isso que, generalizando, nós dizemos que **`stdin`, `stdout` e `stderr`, bem como seus respectivos descritores, são fluxos de dados!**

### Uma pausa para falar de '/proc/self'

Se você executar o comando abaixo...

```
:~$ ls -d /proc/sel*
/proc/self
```

Você verá que existe um diretório chamado `/proc/self`. Mas, observe o que acontece se pedirmos uma listagem mais detalhada (acrescentando a opção `-l`):

```
:~$ ls -dl /proc/sel*
lrwxrwxrwx ... /proc/self -> 188521
```

Isso demonstra que `/proc/self` é um link para o diretório de um processo (identificado aqui pelo PID `188521`), mas de quem é esse PID?

Vamos executar novamente:

```
:~$ ls -dl /proc/sel*
lrwxrwxrwx ... /proc/self -> 188984
```

Agora o PID é `188984`! A que se deve essa mudança?

A resposta está na tradução da palavra *self*, que é algo como *"si mesmo"*, *"eu mesmo"*. Quer dizer, quando acessamos o diretório `/proc/self`, **ele sempre será um link para o processo do programa utilizado para fazer este acesso**. Nos exemplos acima, nós utilizamos o programa `ls`, logo, os dois PIDs exibidos referem-se ao próprio programa `ls`.

### É possível criar outros descritores de arquivos

Voltando ao exemplo da listagem dos fluxos de dados padrão...

```
:~$ ls -l /dev/std*
lrwxrwxrwx ... /dev/stderr -> /proc/self/fd/2
lrwxrwxrwx ... /dev/stdin -> /proc/self/fd/0
lrwxrwxrwx ... /dev/stdout -> /proc/self/fd/1
```

Note que eles são links para `/proc/self/fd/...`, isso porque os dispositivos `stdin`, `stdout` e `stderr` sempre estarão relacionados com o processo do programa que estiver acessando esses fluxos de dados. Isso ficou demonstrado quando nós tentamos seguir os links simbólicos:

```  
:~$ ls -l /proc/self/fd
total 0
lr-x------ ... 3 -> /proc/165890/fd
lrwx------ ... 0 -> /dev/pts/7
lrwx------ ... 1 -> /dev/pts/7
lrwx------ ... 2 -> /dev/pts/7
```

Aqui, além dos descritores `0`, `1` e `2`, nós vemos surgir um quarto descritor de arquivos com o nome `3`, desta vez, como um link apontando para o diretório `/proc/165890/fd`. Isso esclarece um outro detalhe: o fato de que programas e comandos podem criar descritores de arquivos para uso próprio.

Observe:

```
:~$ ls -l /proc/$$/fd /proc/self/fd
/proc/163150/fd:
total 0
lrwx------ ... 0 -> /dev/pts/7
lrwx------ ... 1 -> /dev/pts/7
lrwx------ ... 2 -> /dev/pts/7
lrwx------ ... 255 -> /dev/pts/7

/proc/self/fd:
total 0
lr-x------ ... 3 -> /proc/189380/fd
lrwx------ ... 0 -> /dev/pts/7
lrwx------ ... 1 -> /dev/pts/7
lrwx------ ... 2 -> /dev/pts/7
```

Aqui, nós pedimos a listagem de dois diretórios:

**`/proc/$$/fd`:** descritores de arquivos do processo da sessão corrente do shell.

**`/proc/self/fd`:** descritores de arquivos do processo da execução do comando `ls -l`.

Observe que ambos os programas (`ls` e `bash`) criam descritores de arquivos para lidar com seus procedimentos internos: O Bash utiliza o FD `255` para lidar com o assunto desta aula, os redirecionamentos, enquanto o `ls` cria o FD `3` para lidar com a exibição dos descritores de arquivos relativos ao seu próprio PID, definido dinamicamente através do diretório `/proc/self`.

Outro exemplo da criação de novos descritores de arquivos, são as substituições de processos que nós vimos na aula anterior:

```  
:~$ echo <(:) >(:) <(:) >(:)
/dev/fd/63 /dev/fd/62 /dev/fd/61 /dev/fd/60
```

Nas expansões de substituições de comandos (exclusivas do Bash), o shell cria descritores de arquivos temporários para que a saída dos comandos sejam passadas para os programas que precisarem ler arquivos.

### O acesso a arquivos "em disco"

Mas, como fica o acesso aos arquivos nas mídias persistentes?

Para investigar, vamos tomar como exemplo o arquivo `teste.txt`, cujo conteúdo é apenas:

```
uma linha
```

A ideia é listar o diretório `/proc/PID_DO_SHELL/fd` enquanto o comando `read` lê as linhas do arquivo através do fluxo de dados na entrada padrão (`stdin`). Sendo um comando interno, o PID que nós monitoraremos será o da própria sessão do shell (variável `$`).

Para que a listagem dos arquivos seja possível, ainda na execução do comando `read`, nós utilizaremos o comando composto `while`:

```
while read; do ls -l /proc/$$/fd;  done < teste.txt
```

Lembre-se de que um comando composto compartilha e mantém abertos os fluxos de dados utilizados até que o último dos comandos tenha sido finalizado. Isso nos permite listar `/proc/$$/fd` enquanto os recursos ainda estão abertos.

Portanto...

```
:~$ while read; do ls -l /proc/$$/fd;  done < teste.txt
total 0
lr-x------ ... 0 -> /home/blau/teste.txt
lrwx------ ... 1 -> /dev/pts/7
lrwx------ ... 10 -> /dev/pts/7
lrwx------ ... 2 -> /dev/pts/7
lrwx------ ... 255 -> /dev/pts/7
```

Na listagem, o FD `255`, como já vimos, é um recurso auxiliar do próprio Bash. Contudo, um segundo descritor de arquivos auxiliar, o FD `10`, é criado para a execução do comando interno `read`. O fato mais interessante, porém, é que o FD `0` (`stdin`) passou a ser um link para o arquivo `/home/blau/teste.txt`:

```
lr-x------ ... 0 -> /home/blau/teste.txt
 ↑
 |
 +--- Observe: é apenas para leitura!
```

Disso, nós podemos depreender que a presença do **operador de redirecionamento** `<` causou a delegação do papel de leitura da entrada padrão, originalmente atribuída ao teclado, para o conteúdo do arquivo.

> Sem argumentos adicionais, o comando `read` lê apenas uma linha de caracteres por vez, e quem delimita a linha é o caractere de fim de linha ou o sinal de fim do arquivo (EOF).

Mas, como um programa faria para ler o conteúdo de um arquivo sem recorrer a um redirecionamento do tipo `programa < arquivo`?

Por exemplo, o programa `awk`, uma linguagem de processamento de dados em texto, é capaz de ler arquivos diretamente, sem redirecionamentos na linha do comando, como podemos ver abaixo:

```
:~$ awk '{print}' teste.txt
uma linha
```

Se pudermos monitorar os descritores de arquivos associados ao seu processo (PID) durante a sua execução, certamente veríamos os recursos utilizados para a leitura do arquivo -- felizmente, podemos!

Observe:

```
:~$ awk '{system("ls -l /proc/$(pidof awk)/fd")}' teste.txt 
total 0
lrwx------ ... 0 -> /dev/pts/7
lrwx------ ... 1 -> /dev/pts/7
lrwx------ ... 2 -> /dev/pts/7
lr-x------ ... 3 -> /home/blau/teste.txt
```

> A função `system()` permite que o `awk` execute comandos do shell. Para obter o PID atribuído à sua execução, nós expandimos a saída do comando `pidof awk`.

Como podemos ver, a leitura do arquivo foi feita através do descritor de arquivos `3`. O número do descritor de arquivos não é o que importa neste caso, mas o fato de que descritores de arquivos podem ser criados: seja por atribuição do sistema operacional, por uma instrução explícita na linha do comando ou até por uma instrução do programa.

---

<iframe id="lbry-iframe" width="100%" height="385" src="https://odysee.com/$/embed/csg-a09-2/b92d08481208db992b24439dbf633a44e5230f55?r=AKLxZYnKp7xZJjRjSKUkuGA5AE8uXZm3" allowfullscreen></iframe>

## 9.4 - Operadores de redirecionamento

O teclado (`stdin`) e os dados exibidos no terminal (`stdout`) são, respectivamente, a origem e o destino padrão dos fluxos de dados que estabelecem a comunicação entre o shell e o utilizador. Através dos **operadores de redirecionamento**, nós podemos alterar a origem e o destino dos fluxos de dados.

### Redirecionamento de saídas ('stdout/stderr' → arquivos)

Quando queremos desviar fluxos de dados para um arquivo, nós utilizamos os operadores `>` e `>>`. Por padrão, os dois operadores capturam os dados destinados originalmente à saída padrão (`stdout`) para um arquivo, com uma diferença:

* `>` → Trunca (zera) o conteúdo do arquivo antes de proceder a escrita.
* `>>` → Escreve os dados recebidos ao final do arquivo.

A tabela abaixo mostra a sintaxe, a finalidade e o comportamento desses operadores:

| Operador         | Descrição |
| ---------------- | --------- |
| `[FD]> arquivo`  | Redireciona o fluxo de dados destinado ao descritor de arquivos `FD` para `arquivo`. O conteúdo do arquivo é truncado antes da escrita. Quando `FD` é omitido, o shell entende que se trata do descritor de arquivos `1` (`stdout`). |
| `[FD]>> arquivo`  | Redireciona o fluxo de dados destinado ao descritor de arquivos `FD` para `arquivo`. O conteúdo original é preservado e os novos dados são escritos no final do arquivo. Quando `FD` é omitido, o shell entende que se trata do descritor de arquivos `1` (`stdout`). |

> Em ambos os casos, se o arquivo não existir, ele será criado.

A explicitação do descritor de arquivos é opcional **apenas** quando se trata da saída padrão (FD `1`). Se quisermos, por exemplo, redirecionar a saída padrão de erros (FD `2`), nós devemos utilizar `2>`. A propósito, como os descritores `1` e `2` representam fluxos de saída padrão, existe outra notação para quando queremos redirecionar as duas saídas para um arquivo:

```
&> arquivo  # Cria ou trunca 'arquivo' para escrever
            # as saídas '1' e '2'.
            
&>> arquivo # Cria ou posiciona ponteiro interno no final de
            # 'arquivo' para escrever as saídas '1' e '2'.
```

Eventualmente, você pode encontrar uma das formas abaixo sendo utilizadas com o mesmo propósito:

```
comando >& arquivo
comando > arquivo 2>&1
```

Mas a primeira forma (`&>`) ainda é a preferível, tanto por legibilidade quanto por compatibilidade.

### Alterando o comportamento do operador '>'

O operador `>`, como dissemos, trunca o conteúdo do arquivo de destino se ele existir. O Bash oferece uma opção configurável pelo comando `shopt -o` para suprimir este comportamento: a opção `noclobber`.

Por padrão, ela vem desligada:

```  
:~$ shopt -o noclobber
noclobber      	off
```

Para habilitar:

```
:~$ shopt -s -o noclobber
:~$ shopt -o noclobber
noclobber      	on
```

Para desabilitar:

```
:~$ shopt -u -o noclobber
:~$ shopt -o noclobber
noclobber      	off
```

Contudo, mesmo que `noclobber` esteja habilitada, é possível reproduzir o comportamento padrão com o operador `>|`:

```
:~$ shopt -s -o noclobber

:~$ echo banana > teste.txt 
bash: teste.txt: impossível sobrescrever arquivo existente
:~$ cat teste.txt
uma linha

~ $ echo banana >| teste.txt
~ $ cat teste.txt 
banana
```

### Redirecionamento para a entrada (arquivos → 'stdin')

No sentido oposto, quando queremos que o conteúdo de um arquivo seja a fonte dos dados lidos por um arquivo, nós utilizamos o operador de redirecionamento para a entrada:

| Operador         | Descrição |
| ---------------- | --------- |
| `[FD]< arquivo`  | Redireciona para o descritor de arquivos `FD` o conteúdo de `arquivo`. Quando `FD` é omitido, o shell entende que se trata do descritor de arquivos `0` (`stdin`). |

Como vimos no tópico anterior, neste redirecionamento, o descritor de arquivos passa a ser um link simbólico para o arquivo que será lido:

```
:~$ while read; do ls -l /proc/$$/fd;  done < teste.txt
total 0
lr-x------ ... 0 -> /home/blau/teste.txt
...
```

No caso do comando interno `read` (portanto, do próprio shell), o descritor de arquivos é `0`, o fluxo de dados de entrada padrão (`stdin`). Já quando experimentamos com o programa `awk`, a leitura foi implementada através da criação do descritor de arquivos `3`:

```
:~$ awk '{system("ls -l /proc/$(pidof awk)/fd")}' teste.txt 
total 0
...
lr-x------ ... 3 -> /home/blau/teste.txt
```

Nos dois casos, porém, é interessante notar que o conteúdo do arquivo `teste.txt` parece substituir a função de um teclado ligado ao terminal por onde nós digitaríamos os caracteres. Ou seja, os descritores de arquivos podem até ser listados como se fossem arquivos normais (ou links), mas eles precisam ser vistos como mecanismos do sistema operacional que representam, de fato, **fluxos de dados**.

## 9.5 - Criando descritores a arquivos para escrita

Os descritores de arquivos `0`, `1` e `2` são reservados para os fluxos de dados padrão. No entanto, o Bash nos permite designar descritores de arquivos para arquivos de entrada e/ou de saída através de um mecanismo chamado: **descritores de arquivos definidos pelo usuário**.

O procedimento de escrita através de descritores de arquivos segue três etapas:

* Associar o arquivo de saída a um descritor de arquivos;
* Executar a escrita de dados no arquivo de saída;
* Fechar o descritor de arquivos.

Isso equivale ao que se faz em outras linguagens:

* Obter um manipulador de arquivos para escrita;
* Escrever no arquivo através deste manipulador;
* Fechar o manipulador de arquivos.

Para associar um arquivo de saída a um descritor de arquivos, nós utilizamos o comando `exec` seguido de um operador de redirecionamento de saída e do nome do arquivo, por exemplo:

```
:~$ exec 3> arquivo.txt
```

O comando `exec` substitui o shell corrente na execução de comandos (sim, como na chamada de sistema `exec()`, de quado falamos dos processos), mas também pode processar um redirecionamento caso nenhum comando seja especificado.

No exemplo acima, nós associamos o descritor de arquivos `3` para a escrita em `arquivo.txt`. Como resultado, um novo link é criado no processo do shell:

```
:~$ exec 3> arquivo.txt
:~$ ls -l /proc/$$/fd
total 0
lrwx------ ... 0 -> /dev/pts/0
lrwx------ ... 1 -> /dev/pts/0
lrwx------ ... 2 -> /dev/pts/0
lrwx------ ... 255 -> /dev/pts/0
l-wx------ ... 3 -> /home/blau/arquivo.txt
  ↑
  |
  +--- Observe: é apenas escrita!
```

### Escrevendo no arquivo de saída pelo descritor de arquivos

Para escrever no arquivo de saída através do descritor de arquivos criado, nós utilizamos o operador:

```  
>&n    # Escreve no descritor de arquivos 'n'.
```

Por exemplo:

```
:~$ echo 'Maria tinha um carneirinho' >&3
:~$ cat arquivo.txt 
Maria tinha um carneirinho
```

Interessante, mas você deve estar se perguntando: "qual a vantagem?"

A questão aqui, é que o descritor de arquivos `3` continuará aberto para escrita enquanto nós não o fecharmos explicitamente ou a sessão do shell não terminar!

Observe:

```
:~$ echo 'Sua lã era branca como a neve' >&3
:~$ cat arquivo.txt 
Maria tinha um carneirinho
Sua lã era branca como a neve
```

Ou seja, este mecanismo dá mais performance e flexibilidade, porque o recurso de escrita é aberto apenas uma vez e pode ser utilizado várias vezes numa mesma sessão do shell.

> Observe que os descritores de arquivos (definidos pelo usuário ou padrão) sempre estão associados ao processo de um shell. Isso é particularmente importante de notar quando estivermos trabalhando com subshells!

### Fechando descritores de arquivos

Com o fim do processo do shell onde o descritor tiver sido criado, ele também deixa de existir, mas não é uma boa prática deixar o fechamento por conta do fim da sessão.

A forma mais adequada para fecharmos um descritor de arquivos é executando:

```
exec n>&-    # Fecha o descritor de arquivos 'n'.
```

Fechando o arquivo do nosso exemplo:

```
:~$ exec 3>&-
:~$ echo 'Aonde quer que a menina fosse' >&3
bash: 3: Descritor de arquivo inválido  
```

Para confirmar...

```
:~$ ls -l /proc/$$/fd
total 0
lrwx------ ... 0 -> /dev/pts/0
lrwx------ ... 1 -> /dev/pts/0
lrwx------ ... 2 -> /dev/pts/0
lrwx------ ... 255 -> /dev/pts/0
```

Como esperado, a ligação simbólica `3` desapareceu.

### Adicionando linhas em um arquivo existente

Quando utilizamos o operador `>` na criação de um descritor de arquivos, o arquivo associado a ele será truncado (zerado), exatamente como no redirecionamento da saída padrão:

```
:~$ exec 3> arquivo.txt
:~$ cat arquivo.txt
:~$
```

Sendo assim, se quisermos continuar a escrever no arquivo dos exemplos anteriores, nós podemos abri-lo novamente para escrita com o operador `>>` (*append*):

```
:~$ exec 3>> arquivo.txt

:~$ cat arquivo.txt
Maria tinha um carneirinho
Sua lã era branca como a neve

:~$ echo 'Aonde quer que a menina fosse' >&3

:~$ cat arquivo.txt
Maria tinha um carneirinho
Sua lã era branca como a neve
Aonde quer que a menina fosse

:~$ exec 3>&-
```

## 9.6 - Criando descritores de arquivos para leitura

O mesmo ganho de performance e flexibilidade pode ser conseguido para a leitura de arquivos através da criação de descritores. O procedimento é semelhante ao da escrita:

* Associar um descritor a um arquivo de entrada
* Ler o conteúdo do arquivo
* Fechar o descritor de arquivos

Também, da mesma forma que antes, nós utilizamos o comando `exec` seguido do redirecionamento, por exemplo:

```
:~$ exec 3< arquivo.txt
```

Vamos verificar o processo do shell?

```
:~$ ls -l /proc/$$/fd
total 0
lrwx------ ... 0 -> /dev/pts/0
lrwx------ ... 1 -> /dev/pts/0
lrwx------ ... 2 -> /dev/pts/0
lrwx------ ... 255 -> /dev/pts/0
lr-x------ ... 3 -> /home/blau/arquivo.txt
 ↑
 |
 +--- Observe: somente leitura!
```

Tudo como antes, mas, desta vez, nós só podemos ler o arquivo. Se tentarmos escrever nele...

```
:~$ echo 'O carneirinho ia atrás' >&3
bash: echo: erro de escrita: Descritor de arquivo inválido
```

Para ler o conteúdo do arquivo através do seu descritor, nós utilizamos o operador:

```
<&n    # Lê o descritor de arquivos 'n'
```

Por exemplo:

```
:~$ cat <&3
Maria tinha um carneirinho
Sua lã era branca como a neve
Aonde quer que a menina fosse
```

Contudo, quando se trata da leitura de arquivos, existe algo mais a ser levado em conta. Veja por você mesmo:

```
# Criando o descritor de arquivos para leitura...
:~$ exec 3< arquivo.txt

# Exibindo o conteúdo do arquivo...
:~$ cat <&3
Maria tinha um carneirinho
Sua lã era branca como a neve
Aonde quer que a menina fosse

# Tentando novamente...
:~$ cat <&3
:~$ 

# Nada!
```

Confuso, mas vamos conferir novamente o processo do shell:

```
:~$ ls -l /proc/$$/fd
total 0
lrwx------ ... 0 -> /dev/pts/0
lrwx------ ... 1 -> /dev/pts/0
lrwx------ ... 2 -> /dev/pts/0
lrwx------ ... 255 -> /dev/pts/0
lr-x------ ... 3 -> /home/blau/arquivo.txt
```

Nada mudou! Nosso redirecionamento continua ativo, mas não conseguimos ler o arquivo novamente e nem recebemos qualquer tipo de erro!

Vamos fechar o descritor de arquivos e tentar outra vez de outra forma:

```
# Fechando o descritor de arquivos anterior...
:~$ exec 3>&-

# Criando o descritor de arquivos para leitura...
:~$ exec 3< arquivo.txt

# Lendo o conteúdo do arquivo com o comando 'read'...
:~$ read <&3; echo $REPLY
Maria tinha um carneirinho

# Outra vez...
:~$ read <&3; echo $REPLY
Sua lã era branca como a neve

# De novo...
:~$ read <&3; echo $REPLY
Aonde quer que a menina fosse

Mais uma vez...
:~$ read <&3; echo $REPLY

:~$
```

Reparou? O comando `read`, diferente do `cat`, lê apenas uma linha do arquivo por vez. A cada leitura, **o ponteiro interno do arquivo** se posiciona no início da linha seguinte até chegar ao fim do arquivo (EOF), e é por isso que não conseguimos fazer outra leitura do mesmo arquivo através do mesmo descritor.

Então, apesar de conseguirmos o mesmo ganho de performance e flexibilidade criando descritores para a leitura de arquivos -- afinal, o arquivo não precisará ser fechado para a leitura das linhas subsequentes em outros pontos de um script ou da sessão do shell após a leitura da primeira linha --, a única forma de reposicionar o ponteiro interno do arquivo será fechando o descritor de arquivos atual e criando outro para uma nova leitura.

## 9.7 - Criando descritores para leitura e escrita

Uma outra possibilidade que o Bash oferece, é a criação de descritores de arquivos para leitura e escrita, o que é feito com o comando `exec` e o operador `n<>`, por exemplo:

```
:~$ exec 3<> arquivo.txt
```

Vejamos o que aconteceu em `/proc`:

```  
:~$ ls -l /proc/$$/fd
total 0
lrwx------ ... 0 -> /dev/pts/0
lrwx------ ... 1 -> /dev/pts/0
lrwx------ ... 2 -> /dev/pts/0
lrwx------ ... 255 -> /dev/pts/0
lrwx------ ... 3 -> /home/blau/arquivo.txt
 ↑
 |
 +--- Agora podemos ler e escrever!
```

Apesar de parecer mais simples criar um descritor de arquivos para leitura e escrita, nós precisamos considerar vários pontos bastante críticos:

* Quando abrimos um arquivo **para escrita**, o ponteiro interno é posicionado **no fim do arquivo**, de forma que toda inserção de dados acontece após a última linha.

* Por outro lado, quando abrimos um arquivo **para leitura**, o ponteiro interno é posicionado **no início do arquivo**.

* Um arquivo aberto **para leitura e escrita**, por sua vez, pressupõe a possibilidade de uma operação de leitura, logo, o ponteiro interno também é posicionado **no início do arquivo**!

* Somando a isso o fato de que uma operação de escrita sempre sobrescreve os dados a partir da posição do ponteiro interno do arquivo, não é difícil imaginar que podemos estar diante de um desastre anunciado!

Para que você tenha uma noção do perigo, veja o que acontece quando não levamos esses pontos em consideração:

```
# O conteúdo original...
:~$ cat arquivo.txt 
Maria tinha um carneirinho
Sua lã era branca como a neve
Aonde quer que a menina fosse

# Criando o descritor para leitura e escrita...
:~$ exec 3<> arquivo.txt 

# Lendo a primeira linha do arquivo...
:~$ read <&3

# Escrevendo na linha seguinte...
:~$ echo 'O carneirinho ia atrás' >&3

# Fechando o descritor de arquivos...
:~$ exec 3>&-

# Conferindo o resultado...
:~$ cat arquivo.txt
Maria tinha um carneirinho
O carneirinho ia atrás
a neve
Aonde quer que a menina fosse
```

Para entender o que aconteceu, compare os comprimentos da linha escrita com o da linha original:

```
Sua lã era branca como a neve  # Original
O carneirinho ia atrás         # Novo texto
                      ↑
                      |
                      +--- Quebra de linha do 'echo'
```

Resultado:

```
O carneirinho ia atrás    # Novo texto + quebra de linha
a neve                    # O que sobrou do original
```

Mas vejamos como isso funciona em uma aplicação mais útil, como substituir um caractere em uma linha do arquivo. Para isso, considere o arquivo `letras.txt`, cujo conteúdo é apenas:

```
abcddefg
```

Repare que o quinto caractere (`d`) deveria ser `e`. Nós podemos utilizar um descritor para leitura e escrita para corrigir o arquivo:

```
# O conteúdo original do arquivo...
:~$ cat letras.txt
abcddfg

# Criando o descritor para leitura e escrita...
:~$ exec 3<> letras.txt 

# Utilizando o 'read' para ler os 4 primeiros caracteres...
:~$ read -n 4 <&3

# Escrevendo na posição atual do ponteiro interno...
:~$ printf 'e' >&3

# Fechando o descritor de arquivos...
:~$ exec 3>&-

# Conferindo, o segundo 'd' foi trocado por 'e'...
~/tmp $ cat letras.txt
abcdefg
```

> A opção `-n` serve para indicar quantos caracteres o comando `read` deverá ler. 

Pode não parecer tão útil, mas considere o que poderia ser feito com arquivos onde os dados são dispostos em colunas de texto com larguras fixas de caracteres, por exemplo. Se planejado com cuidado e atenção, o procedimento pode ser bem mais rápido e flexível do que outras opções envolvendo programas como `awk` e `sed`.

## 9.8 - Não confunda os operadores

Neste ponto, é bem provável que você esteja confuso sobre os operadores utilizados para ler e escrever em arquivos através de descritores de arquivos, especialmente porque eles se parecem muito com os operadores de redirecionamento.

Para evitar esse tipo de confusão, você precisa ter em mente que os operadores de descritores de arquivos são links para os arquivos associados a eles, ou seja, podem ser utilizados como argumentos de comandos e programas que exijam nomes de arquivos, por exemplo:

```
:~$ cat <&4
```

Ou então:

```
:~$ echo 'uma linha de texto' >&5
```

Já nos redirecionamentos, nós determinamos que arquivo será associado a um fluxo de dados de entrada ou saída (geralmente os fluxos padrão). Portanto, não há um argumento sendo passado para um programa ou comando, e sim uma informação para que o shell saiba para onde enviar a saída do comando ou de onde virá o fluxo de dados a ser passado para o comando, por exemplo:

```
echo 'uma linha de texto' > arquivo.txt
```

Aqui, o comando `echo` será executado e, posteriormente, os dados que ele enviar para a saída padrão serão redirecionados para `arquivo.txt` em vez de serem exibidos no terminal.

Quando dissemos que a sintaxe do redirecionamento de saída previa um descritor de arquivos opcional (`[n]>` ou `[n]>>`), foi porque o inteiro opcional `n` é utilizado para a criação de descritores de arquivos em conjunto com o comando `exec`, como vimos:

```
:~$ exec 4>> arquivo.txt
:~$ exec 5> letras.txt
```

O mesmo acontece com o redirecionamento de entrada, cuja sintaxe também prevê a indicação de um inteiro opcional (`[n]<`), ou seja, está opção será utilizada na criação de um descritor de arquivos para leitura.

```
:~$ exec 3< arquivo.txt
```

### Por que precisamos do comando 'exec'

Com o que sabemos sobre operadores, analise o que aconteceria se executássemos o comando abaixo:

```
:~$ 4 > teste.txt
```

Exatamente! O Bash tentaria executar o comando `4`, que não existe, e nós teríamos uma mensagem de erro:

```
:~$ 4 > teste.txt
bash: 4: comando não encontrado
```

Por outro lado, um inteiro junto a um operador de redirecionamento de saída é uma sintaxe válida, e o exemplo abaixo não causaria erros:

```
:~$ 4> teste.txt
:~$
```

O único efeito desse comando será a criação de um arquivo vazio chamado `teste.txt`. O fato de haver ali o número `4` não faz diferença, porque não existe uma saída a ser redirecionada e tudo que o shell entende que deve fazer é criar o arquivo vazio.

Mas, se houvesse um comando produzindo saídas, por não existir um descritor de arquivos `4` associado aos recursos do terminal nem ao processo do shell, os dados seriam enviados normalmente para a saída padrão e o fluxo associado ao descritor inexistente `4` teria o mesmo efeito de antes: a criação de um arquivo vazio.

Observe:

```
:~$ ls teste.txt
ls: não foi possível acessar 'teste.txt': Arquivo ou diretório inexistente

:~$ echo banana 4> teste.txt
banana

:~$ ls teste.txt
teste.txt
```

Com o comando `exec` a situação é diferente. Sua função é substituir o processo do shell pelo processo do comando que ele receber como argumento. Sem esse comando, o `exec` ainda pode passar pela etapa de processamento de redirecionamentos, que é o que o shell faz com todos os comandos que executa, porém, em vez de apenas criar um arquivo vazio, como já vimos, ele também cria uma ligação entre esse arquivo e o descritor de arquivos informado no processo do shell.

```
:~$ exec 4> teste.txt
```

A partir deste ponto, os operadores de redirecionamento de saída (`>` e `>>`) ainda terão a mesma função e sintaxe de antes, mas nós poderemos utilizar `&4` para informar que o destino do redirecionamento é o descritor de arquivos `4`.

```
:~$ echo banana >&4
:~$ cat teste.txt 
banana
```

Quando fechamos um descritor de arquivos, acontece algo semelhante: nós ainda utilizamos o `exec` e um redirecionamento especificando o número do descritor de arquivos à esquerda, mas informamos que o destino do redirecionamento é o caractere `-`, o que será interpretado como um comando para desfazer a ligação entre o descritor de arquivos e o arquivo associado a ele, independente da direção do fluxo de dados (leitura ou escrita):

```
exec 4>&-
exec 4<&-
```

## 9.9 - Operações avançadas com descritores de arquivos

Ainda é possível realizar diversas operações avançadas com os descritores de arquivos, mas abordar todas essas possibilidades fugiria muito do propósito do nosso curso (principalmente pelo tipo de aplicação que as demonstrações envolveriam). Porém, para que você seja capaz de pesquisar mais a fundo e realizar os seus próprios experimentos, aqui estão algumas pistas sobre essas operações:

```
# Duplicar/copiar

[n]>&m  # Copia o descritor 'm' em 'n' para escrita.
        # Se 'n' não for informado, `stdout` será usada.
        # Se 'm' for um traço (-), 'n' será fechado.

[n]<&m  # Copia o descritor 'm' em 'n' para leitura.
        # Se 'n' não for informado, `stdin` será usada.
        # Se 'm' for um traço (-), 'n' será fechado.

# Mover

[n]>&m- # Move o descritor 'm' para 'n' e fecha o descritor 'm'.
        # Se 'n' não for informado, `stdout` será usado.

[n]<&m- # Move o descritor 'm' para 'n' e fecha o descritor 'm'.
        # Se 'n' não for informado, `stdin` será usado.
        
```

Mas ainda podemos identificar algo que já vimos observando os operadores acima:

```
>&m    # Escrita no arquivo associado ao descritor 'm'.
       # É o operador de 'cópia' utilizando 'stdout'.

<&m    # Leitura do arquivo associado ao descritor 'm'.
       # É o operador de 'cópia' utilizando 'stdin'
       
n>&-   # Fecha o descritor aberto para escrita 'n'.
       # É um operador de 'cópia' com '-'.

n<&-   # Fecha o descritor aberto para leitura 'n'.
       # É um operador de 'cópia' com '-'.
```

Isso quer dizer que, em todas as operações de leitura e escrita em arquivos através de descritores de arquivos que vimos nos tópicos anteriores, os descritores que nós criamos são copiados para `stdin` ou `stdout`, dependendo da direção do fluxo de dados. Ou seja, os arquivos associados aos nossos descritores de arquivos assumirão um papel equivalente ao do teclado (`stdin`) ou da saída do terminal (`stdout`).

## 9.10 - 'Here docs'

Com o operador `<<`, chamado de ***here document*** (ou *here doc*), nós podemos fazer exatamente a mesma coisa que fizemos com o operador de redirecionamento para a entrada (`<`), só que utilizando uma string com várias linhas como fonte de dados -- tudo que precisamos fazer é dizer onde começam e onde terminam as linhas.

Esta é a sintaxe geral de uso de um *here doc*:

```
COMANDO [n]<< DELIMITADOR
linha 1
linha 2
...
linha n
DELIMITADOR
```

O espaço depois do operador `<<` é opcional, assim como o número descritor de arquivos 'n' que, se omitido, será `stdin`.

> Importante! Não pode haver espaços antes do delimitador que faz o fechamento do bloco!

O delimitador inicial pode ser qualquer palavra com ou sem aspas, e isso é o que vai determinar se haverá ou não expansões no texto do documento.

Observe:

```
~ $ cat << AQUI
> {1..5}
> ~
> 'Olá, mundo!'
> "Olá, novamente"
> Menu usuário é: $USER
> $(echo zebra)
> <(:)
> $((2+2))
> AQUI
{1..5}
~
'Olá, mundo!'
"Olá, novamente"
Menu usuário é: blau
zebra
<(:)
4
:~$
```

Sem aspas envolvendo a palavra delimitadora `AQUI`, todos os parâmetros são expandidos (além das substituições de comandos e das expansões aritméticas), mas os outros tipos de expansão não são processados. Isso tem um efeito semelhante ao da citação com aspas duplas, ou seja, apenas o `$` e o acento grave preservam seus significados especiais para o shell.

Porém, se utilizarmos aspas (simples ou duplas, tanto faz) no delimitador:

```
:~$ cat << "AQUI"
> {1..5}
> ~
> 'Olá, mundo!'
> "Olá, novamente"
> Menu usuário é: $USER
> $(echo zebra)
> <(:)
> $((2+2))
> AQUI
> {1..5}
> ~
> 'Olá, mundo!'
> "Olá, novamente"
> Menu usuário é: $USER
> $(echo zebra)
> <(:)
> $((2+2))
:~$
```

Nada é expandido e todos os caracteres das linhas são tratados literalmente. 

Nos scripts, pode ser interessante escrever as linhas de um *here doc* com o recuo de uma tabulação em relação à margem:

```
cat << AQUI
	linha um
	linha dois
AQUI
```

O problema é que essa tabulação será incluída como parte da linha:

```
	linha um
	linha dois
```

Se o recuo for feito com uma tabulação real, o shell pode removê-lo se nós acrescentarmos um traço (`-`) ao operador:

```
# Tabulação real...
cat <<- AQUI
	linha um
	linha dois
AQUI

# Tabulação com 4 espaços...
cat <<- AQUI
	linha um
	linha dois
AQUI
```


O resultado será:

```
linha um
linha dois
    linha um
    linha dois
```


## 9.11 – 'Here strings'

As ***here strings*** são equivalentes a comandos do tipo:

```
echo PALAVRA | COMANDO
```

Onde PALAVRA pode ser uma palavra (sequência de caracteres literais) ou resultante de qualquer expansão, exceto expansões de nomes de arquivos e de chaves (porque podem resultar em mais de uma palavra).

A sintaxe de uma *here string* é:

```
COMANDO <<< PALAVRA
```

Exemplos:

```
:~$ cat <<< *
*

:~$ cat <<< ~
/home/blau

:~$ cat <<< {1..5}
{1..5}

:~$ cat <<< $USER
blau

:~$ cat <<< "Meu usuário é $USER"
Meu usuário é blau

:~$ cat <<< <(:)
/dev/fd/63
```