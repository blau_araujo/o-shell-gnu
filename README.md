# O Shell GNU

Repositório de recursos do curso Shell GNU. Em relação aos demais cursos, este pode ser considerado um pouco mais aprofundado e até avançado demais para um primeiro contato com o shell. Para quem está começando, eu recomendo o [Curso Básico de programação em Bash](https://codeberg.org/blau_araujo/prog-bash-basico) e, em seguida, a [playlist do curso intensivo de programação do bash](https://youtube.com/playlist?list=PLXoSGejyuQGr53w4IzUzbPCqR4HPOHjAI).

![](https://blauaraujo.com/wp-content/uploads/2022/05/cafezinho-01.png)

## Links importantes

* [Índice das aulas](aulas/README.md)
* [Playlist completa no Youtube](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)
* [Vídeos e textos para aprender o shell do GNU/Linux](https://codeberg.org/blau_araujo/para-aprender-shell)
* [Dúvidas sobre os tópicos do curso (issues)](https://codeberg.org/blau_araujo/o-shell-gnu/issues)
* [Discussões sobre os tópicos de todos os nossos cursos (Gitter)](https://gitter.im/blau_araujo/community)


## Formas de apoio

* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* Doações via PIX: pix@blauaraujo.com
* [Versão impressa do Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)